﻿namespace biblioteka_rad
{
    partial class frmIzdavacKnjiga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabKontrola = new System.Windows.Forms.TabControl();
            this.tabStranaIzdavacKnjiga = new System.Windows.Forms.TabPage();
            this.btnNoviZapis = new System.Windows.Forms.Button();
            this.btnDesnoZapis = new System.Windows.Forms.Button();
            this.btnLevoZapis = new System.Windows.Forms.Button();
            this.btnUnesiNovogIzdavaca = new System.Windows.Forms.Button();
            this.cmbOcena = new System.Windows.Forms.ComboBox();
            this.cmbNaziv = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtBrojTel = new System.Windows.Forms.TextBox();
            this.txtGrad = new System.Windows.Forms.TextBox();
            this.txtPostanski = new System.Windows.Forms.TextBox();
            this.txtDrzava = new System.Windows.Forms.TextBox();
            this.tabIzdavacKnjiga = new System.Windows.Forms.TabPage();
            this.btnNoviRekordUpisKnjiga = new System.Windows.Forms.Button();
            this.btnKnjigaRekordDesno = new System.Windows.Forms.Button();
            this.btnKnjigaRekordLevo = new System.Windows.Forms.Button();
            this.txtIndetifikatorPosebneKnjige = new System.Windows.Forms.TextBox();
            this.btnUnesiKnjigeStavke = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMestoKnjige = new System.Windows.Forms.TextBox();
            this.cmbOstecenaNeostecena = new System.Windows.Forms.ComboBox();
            this.cmbSlobodnaZauzeta = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnUnesiAutore = new System.Windows.Forms.Button();
            this.cmbUnosAutoraKoautora = new System.Windows.Forms.ComboBox();
            this.btnUnos = new System.Windows.Forms.Button();
            this.gridKnjige = new System.Windows.Forms.DataGridView();
            this.gridAutori = new System.Windows.Forms.DataGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIzdavacKnjigaTab = new System.Windows.Forms.TextBox();
            this.txtBrojStranaKnjige = new System.Windows.Forms.TextBox();
            this.cmbVezKnjige = new System.Windows.Forms.ComboBox();
            this.cmbTipKnjige = new System.Windows.Forms.ComboBox();
            this.cmbOblastKnjige = new System.Windows.Forms.ComboBox();
            this.cmbNazivKnjige = new System.Windows.Forms.ComboBox();
            this.serviceController1 = new System.ServiceProcess.ServiceController();
            this.tabKontrola.SuspendLayout();
            this.tabStranaIzdavacKnjiga.SuspendLayout();
            this.tabIzdavacKnjiga.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridKnjige)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAutori)).BeginInit();
            this.SuspendLayout();
            // 
            // tabKontrola
            // 
            this.tabKontrola.Controls.Add(this.tabStranaIzdavacKnjiga);
            this.tabKontrola.Controls.Add(this.tabIzdavacKnjiga);
            this.tabKontrola.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabKontrola.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabKontrola.Location = new System.Drawing.Point(0, 0);
            this.tabKontrola.Name = "tabKontrola";
            this.tabKontrola.SelectedIndex = 0;
            this.tabKontrola.Size = new System.Drawing.Size(1107, 595);
            this.tabKontrola.TabIndex = 0;
            this.tabKontrola.SelectedIndexChanged += new System.EventHandler(this.PromenaTaba);
            // 
            // tabStranaIzdavacKnjiga
            // 
            this.tabStranaIzdavacKnjiga.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabStranaIzdavacKnjiga.Controls.Add(this.btnNoviZapis);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.btnDesnoZapis);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.btnLevoZapis);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.btnUnesiNovogIzdavaca);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.cmbOcena);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.cmbNaziv);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.label7);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.label6);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.label4);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.label5);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.label3);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.label2);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.label1);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.txtEmail);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.txtBrojTel);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.txtGrad);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.txtPostanski);
            this.tabStranaIzdavacKnjiga.Controls.Add(this.txtDrzava);
            this.tabStranaIzdavacKnjiga.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabStranaIzdavacKnjiga.Location = new System.Drawing.Point(4, 29);
            this.tabStranaIzdavacKnjiga.Name = "tabStranaIzdavacKnjiga";
            this.tabStranaIzdavacKnjiga.Padding = new System.Windows.Forms.Padding(3);
            this.tabStranaIzdavacKnjiga.Size = new System.Drawing.Size(1099, 562);
            this.tabStranaIzdavacKnjiga.TabIndex = 0;
            this.tabStranaIzdavacKnjiga.Text = "Izdavac";
            // 
            // btnNoviZapis
            // 
            this.btnNoviZapis.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnNoviZapis.BackgroundImage = global::biblioteka_rad.Properties.Resources.skrozDesnoIzdavac;
            this.btnNoviZapis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNoviZapis.Location = new System.Drawing.Point(491, 261);
            this.btnNoviZapis.Name = "btnNoviZapis";
            this.btnNoviZapis.Size = new System.Drawing.Size(96, 48);
            this.btnNoviZapis.TabIndex = 10;
            this.btnNoviZapis.UseVisualStyleBackColor = false;
            this.btnNoviZapis.Click += new System.EventHandler(this.btnNoviZapis_Click);
            // 
            // btnDesnoZapis
            // 
            this.btnDesnoZapis.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnDesnoZapis.BackgroundImage = global::biblioteka_rad.Properties.Resources.desnoIzdavac;
            this.btnDesnoZapis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDesnoZapis.Location = new System.Drawing.Point(389, 261);
            this.btnDesnoZapis.Name = "btnDesnoZapis";
            this.btnDesnoZapis.Size = new System.Drawing.Size(96, 48);
            this.btnDesnoZapis.TabIndex = 9;
            this.btnDesnoZapis.UseVisualStyleBackColor = false;
            this.btnDesnoZapis.Click += new System.EventHandler(this.IzdavacRekordDesno);
            // 
            // btnLevoZapis
            // 
            this.btnLevoZapis.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLevoZapis.BackgroundImage = global::biblioteka_rad.Properties.Resources.levoIzdavac;
            this.btnLevoZapis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLevoZapis.Location = new System.Drawing.Point(287, 261);
            this.btnLevoZapis.Name = "btnLevoZapis";
            this.btnLevoZapis.Size = new System.Drawing.Size(96, 48);
            this.btnLevoZapis.TabIndex = 8;
            this.btnLevoZapis.UseVisualStyleBackColor = false;
            this.btnLevoZapis.Click += new System.EventHandler(this.izdavacRekordLevo);
            // 
            // btnUnesiNovogIzdavaca
            // 
            this.btnUnesiNovogIzdavaca.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUnesiNovogIzdavaca.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnesiNovogIzdavaca.Location = new System.Drawing.Point(41, 261);
            this.btnUnesiNovogIzdavaca.Name = "btnUnesiNovogIzdavaca";
            this.btnUnesiNovogIzdavaca.Size = new System.Drawing.Size(220, 48);
            this.btnUnesiNovogIzdavaca.TabIndex = 7;
            this.btnUnesiNovogIzdavaca.Text = "Unesi novog izdavaca";
            this.btnUnesiNovogIzdavaca.UseVisualStyleBackColor = false;
            this.btnUnesiNovogIzdavaca.Click += new System.EventHandler(this.btnUnesiNovogIzdavaca_Click);
            // 
            // cmbOcena
            // 
            this.cmbOcena.FormattingEnabled = true;
            this.cmbOcena.Location = new System.Drawing.Point(287, 214);
            this.cmbOcena.Name = "cmbOcena";
            this.cmbOcena.Size = new System.Drawing.Size(300, 28);
            this.cmbOcena.TabIndex = 6;
            // 
            // cmbNaziv
            // 
            this.cmbNaziv.FormattingEnabled = true;
            this.cmbNaziv.Location = new System.Drawing.Point(287, 22);
            this.cmbNaziv.Name = "cmbNaziv";
            this.cmbNaziv.Size = new System.Drawing.Size(300, 28);
            this.cmbNaziv.TabIndex = 0;
            this.cmbNaziv.SelectedIndexChanged += new System.EventHandler(this.PormenaKomboBoxaIzdavac);
            this.cmbNaziv.TextUpdate += new System.EventHandler(this.UpdateTextaKomboBoxa);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(37, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ocena";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(37, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 19);
            this.label6.TabIndex = 1;
            this.label6.Text = "E-mail";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(37, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 19);
            this.label4.TabIndex = 1;
            this.label4.Text = "Broj telefona";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(37, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(175, 19);
            this.label5.TabIndex = 1;
            this.label5.Text = "Grad izdavaca";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(37, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Postanski broj";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Drzava izdavaca";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Naziv izdavaca";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(287, 182);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(300, 26);
            this.txtEmail.TabIndex = 5;
            // 
            // txtBrojTel
            // 
            this.txtBrojTel.Location = new System.Drawing.Point(287, 150);
            this.txtBrojTel.Name = "txtBrojTel";
            this.txtBrojTel.Size = new System.Drawing.Size(300, 26);
            this.txtBrojTel.TabIndex = 4;
            // 
            // txtGrad
            // 
            this.txtGrad.Location = new System.Drawing.Point(287, 86);
            this.txtGrad.Name = "txtGrad";
            this.txtGrad.Size = new System.Drawing.Size(300, 26);
            this.txtGrad.TabIndex = 2;
            // 
            // txtPostanski
            // 
            this.txtPostanski.Location = new System.Drawing.Point(287, 118);
            this.txtPostanski.Name = "txtPostanski";
            this.txtPostanski.Size = new System.Drawing.Size(300, 26);
            this.txtPostanski.TabIndex = 3;
            // 
            // txtDrzava
            // 
            this.txtDrzava.Location = new System.Drawing.Point(287, 54);
            this.txtDrzava.Name = "txtDrzava";
            this.txtDrzava.Size = new System.Drawing.Size(300, 26);
            this.txtDrzava.TabIndex = 1;
            // 
            // tabIzdavacKnjiga
            // 
            this.tabIzdavacKnjiga.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabIzdavacKnjiga.Controls.Add(this.btnNoviRekordUpisKnjiga);
            this.tabIzdavacKnjiga.Controls.Add(this.btnKnjigaRekordDesno);
            this.tabIzdavacKnjiga.Controls.Add(this.btnKnjigaRekordLevo);
            this.tabIzdavacKnjiga.Controls.Add(this.txtIndetifikatorPosebneKnjige);
            this.tabIzdavacKnjiga.Controls.Add(this.btnUnesiKnjigeStavke);
            this.tabIzdavacKnjiga.Controls.Add(this.label18);
            this.tabIzdavacKnjiga.Controls.Add(this.label17);
            this.tabIzdavacKnjiga.Controls.Add(this.label16);
            this.tabIzdavacKnjiga.Controls.Add(this.label19);
            this.tabIzdavacKnjiga.Controls.Add(this.label15);
            this.tabIzdavacKnjiga.Controls.Add(this.txtMestoKnjige);
            this.tabIzdavacKnjiga.Controls.Add(this.cmbOstecenaNeostecena);
            this.tabIzdavacKnjiga.Controls.Add(this.cmbSlobodnaZauzeta);
            this.tabIzdavacKnjiga.Controls.Add(this.label14);
            this.tabIzdavacKnjiga.Controls.Add(this.btnUnesiAutore);
            this.tabIzdavacKnjiga.Controls.Add(this.cmbUnosAutoraKoautora);
            this.tabIzdavacKnjiga.Controls.Add(this.btnUnos);
            this.tabIzdavacKnjiga.Controls.Add(this.gridKnjige);
            this.tabIzdavacKnjiga.Controls.Add(this.gridAutori);
            this.tabIzdavacKnjiga.Controls.Add(this.label13);
            this.tabIzdavacKnjiga.Controls.Add(this.label12);
            this.tabIzdavacKnjiga.Controls.Add(this.label11);
            this.tabIzdavacKnjiga.Controls.Add(this.label10);
            this.tabIzdavacKnjiga.Controls.Add(this.label9);
            this.tabIzdavacKnjiga.Controls.Add(this.label8);
            this.tabIzdavacKnjiga.Controls.Add(this.txtIzdavacKnjigaTab);
            this.tabIzdavacKnjiga.Controls.Add(this.txtBrojStranaKnjige);
            this.tabIzdavacKnjiga.Controls.Add(this.cmbVezKnjige);
            this.tabIzdavacKnjiga.Controls.Add(this.cmbTipKnjige);
            this.tabIzdavacKnjiga.Controls.Add(this.cmbOblastKnjige);
            this.tabIzdavacKnjiga.Controls.Add(this.cmbNazivKnjige);
            this.tabIzdavacKnjiga.Location = new System.Drawing.Point(4, 29);
            this.tabIzdavacKnjiga.Name = "tabIzdavacKnjiga";
            this.tabIzdavacKnjiga.Padding = new System.Windows.Forms.Padding(3);
            this.tabIzdavacKnjiga.Size = new System.Drawing.Size(1099, 562);
            this.tabIzdavacKnjiga.TabIndex = 1;
            this.tabIzdavacKnjiga.Text = "Knjiga";
            // 
            // btnNoviRekordUpisKnjiga
            // 
            this.btnNoviRekordUpisKnjiga.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnNoviRekordUpisKnjiga.BackgroundImage = global::biblioteka_rad.Properties.Resources.skrozDesnoIzdavac;
            this.btnNoviRekordUpisKnjiga.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNoviRekordUpisKnjiga.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoviRekordUpisKnjiga.Location = new System.Drawing.Point(314, 239);
            this.btnNoviRekordUpisKnjiga.Name = "btnNoviRekordUpisKnjiga";
            this.btnNoviRekordUpisKnjiga.Size = new System.Drawing.Size(106, 48);
            this.btnNoviRekordUpisKnjiga.TabIndex = 9;
            this.btnNoviRekordUpisKnjiga.UseVisualStyleBackColor = false;
            this.btnNoviRekordUpisKnjiga.Click += new System.EventHandler(this.btnNoviRekordUpisKnjiga_Click);
            // 
            // btnKnjigaRekordDesno
            // 
            this.btnKnjigaRekordDesno.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnKnjigaRekordDesno.BackgroundImage = global::biblioteka_rad.Properties.Resources.desno;
            this.btnKnjigaRekordDesno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnKnjigaRekordDesno.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKnjigaRekordDesno.Location = new System.Drawing.Point(214, 239);
            this.btnKnjigaRekordDesno.Name = "btnKnjigaRekordDesno";
            this.btnKnjigaRekordDesno.Size = new System.Drawing.Size(94, 48);
            this.btnKnjigaRekordDesno.TabIndex = 8;
            this.btnKnjigaRekordDesno.UseVisualStyleBackColor = false;
            this.btnKnjigaRekordDesno.Click += new System.EventHandler(this.btnKnjigaRekordDesno_Click);
            // 
            // btnKnjigaRekordLevo
            // 
            this.btnKnjigaRekordLevo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnKnjigaRekordLevo.BackgroundImage = global::biblioteka_rad.Properties.Resources.levo;
            this.btnKnjigaRekordLevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnKnjigaRekordLevo.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKnjigaRekordLevo.Location = new System.Drawing.Point(114, 239);
            this.btnKnjigaRekordLevo.Name = "btnKnjigaRekordLevo";
            this.btnKnjigaRekordLevo.Size = new System.Drawing.Size(94, 48);
            this.btnKnjigaRekordLevo.TabIndex = 7;
            this.btnKnjigaRekordLevo.UseVisualStyleBackColor = false;
            this.btnKnjigaRekordLevo.Click += new System.EventHandler(this.btnKnjigaRekordLevo_Click);
            // 
            // txtIndetifikatorPosebneKnjige
            // 
            this.txtIndetifikatorPosebneKnjige.Location = new System.Drawing.Point(749, 183);
            this.txtIndetifikatorPosebneKnjige.Name = "txtIndetifikatorPosebneKnjige";
            this.txtIndetifikatorPosebneKnjige.Size = new System.Drawing.Size(342, 26);
            this.txtIndetifikatorPosebneKnjige.TabIndex = 12;
            // 
            // btnUnesiKnjigeStavke
            // 
            this.btnUnesiKnjigeStavke.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUnesiKnjigeStavke.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnesiKnjigeStavke.Location = new System.Drawing.Point(749, 341);
            this.btnUnesiKnjigeStavke.Name = "btnUnesiKnjigeStavke";
            this.btnUnesiKnjigeStavke.Size = new System.Drawing.Size(342, 34);
            this.btnUnesiKnjigeStavke.TabIndex = 16;
            this.btnUnesiKnjigeStavke.Text = "Unesi knjige";
            this.btnUnesiKnjigeStavke.UseVisualStyleBackColor = false;
            this.btnUnesiKnjigeStavke.Click += new System.EventHandler(this.UpisViseKnjiga);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(582, 298);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 19);
            this.label18.TabIndex = 14;
            this.label18.Text = "Mesto";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(582, 257);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 19);
            this.label17.TabIndex = 13;
            this.label17.Text = "Ostecena";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(582, 223);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 19);
            this.label16.TabIndex = 14;
            this.label16.Text = "Slobodna?";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(699, 127);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(239, 19);
            this.label19.TabIndex = 13;
            this.label19.Text = "Uneti knjige posebno";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(582, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(162, 19);
            this.label15.TabIndex = 13;
            this.label15.Text = "Indetifikator";
            // 
            // txtMestoKnjige
            // 
            this.txtMestoKnjige.Location = new System.Drawing.Point(749, 298);
            this.txtMestoKnjige.Name = "txtMestoKnjige";
            this.txtMestoKnjige.Size = new System.Drawing.Size(342, 26);
            this.txtMestoKnjige.TabIndex = 15;
            // 
            // cmbOstecenaNeostecena
            // 
            this.cmbOstecenaNeostecena.FormattingEnabled = true;
            this.cmbOstecenaNeostecena.Location = new System.Drawing.Point(748, 257);
            this.cmbOstecenaNeostecena.Name = "cmbOstecenaNeostecena";
            this.cmbOstecenaNeostecena.Size = new System.Drawing.Size(343, 28);
            this.cmbOstecenaNeostecena.TabIndex = 14;
            // 
            // cmbSlobodnaZauzeta
            // 
            this.cmbSlobodnaZauzeta.FormattingEnabled = true;
            this.cmbSlobodnaZauzeta.Location = new System.Drawing.Point(748, 223);
            this.cmbSlobodnaZauzeta.Name = "cmbSlobodnaZauzeta";
            this.cmbSlobodnaZauzeta.Size = new System.Drawing.Size(343, 28);
            this.cmbSlobodnaZauzeta.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 313);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(272, 19);
            this.label14.TabIndex = 10;
            this.label14.Text = "Uneti autore koautore";
            // 
            // btnUnesiAutore
            // 
            this.btnUnesiAutore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUnesiAutore.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnesiAutore.Location = new System.Drawing.Point(297, 347);
            this.btnUnesiAutore.Name = "btnUnesiAutore";
            this.btnUnesiAutore.Size = new System.Drawing.Size(175, 28);
            this.btnUnesiAutore.TabIndex = 11;
            this.btnUnesiAutore.Text = "Unesi autora";
            this.btnUnesiAutore.UseVisualStyleBackColor = false;
            this.btnUnesiAutore.Click += new System.EventHandler(this.btnUnesiAutore_Click);
            // 
            // cmbUnosAutoraKoautora
            // 
            this.cmbUnosAutoraKoautora.FormattingEnabled = true;
            this.cmbUnosAutoraKoautora.Location = new System.Drawing.Point(12, 347);
            this.cmbUnosAutoraKoautora.Name = "cmbUnosAutoraKoautora";
            this.cmbUnosAutoraKoautora.Size = new System.Drawing.Size(246, 28);
            this.cmbUnosAutoraKoautora.TabIndex = 10;
            // 
            // btnUnos
            // 
            this.btnUnos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUnos.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnos.Location = new System.Drawing.Point(487, 71);
            this.btnUnos.Name = "btnUnos";
            this.btnUnos.Size = new System.Drawing.Size(78, 162);
            this.btnUnos.TabIndex = 6;
            this.btnUnos.Text = "Unos";
            this.btnUnos.UseVisualStyleBackColor = false;
            this.btnUnos.Click += new System.EventHandler(this.UnosKnjigeNovoIme);
            // 
            // gridKnjige
            // 
            this.gridKnjige.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.gridKnjige.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridKnjige.Location = new System.Drawing.Point(586, 381);
            this.gridKnjige.Name = "gridKnjige";
            this.gridKnjige.Size = new System.Drawing.Size(505, 173);
            this.gridKnjige.TabIndex = 18;
            // 
            // gridAutori
            // 
            this.gridAutori.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.gridAutori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAutori.Location = new System.Drawing.Point(12, 381);
            this.gridAutori.Name = "gridAutori";
            this.gridAutori.Size = new System.Drawing.Size(460, 173);
            this.gridAutori.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 205);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 19);
            this.label13.TabIndex = 2;
            this.label13.Text = "Vez";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 174);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 19);
            this.label12.TabIndex = 2;
            this.label12.Text = "Tip ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 19);
            this.label11.TabIndex = 2;
            this.label11.Text = "Broj strana";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 19);
            this.label10.TabIndex = 2;
            this.label10.Text = "Oblast";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 19);
            this.label9.TabIndex = 2;
            this.label9.Text = "Naziv knjige";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 19);
            this.label8.TabIndex = 2;
            this.label8.Text = "Izdavac";
            // 
            // txtIzdavacKnjigaTab
            // 
            this.txtIzdavacKnjigaTab.Location = new System.Drawing.Point(198, 6);
            this.txtIzdavacKnjigaTab.Name = "txtIzdavacKnjigaTab";
            this.txtIzdavacKnjigaTab.Size = new System.Drawing.Size(274, 26);
            this.txtIzdavacKnjigaTab.TabIndex = 0;
            // 
            // txtBrojStranaKnjige
            // 
            this.txtBrojStranaKnjige.Location = new System.Drawing.Point(198, 139);
            this.txtBrojStranaKnjige.Name = "txtBrojStranaKnjige";
            this.txtBrojStranaKnjige.Size = new System.Drawing.Size(274, 26);
            this.txtBrojStranaKnjige.TabIndex = 3;
            // 
            // cmbVezKnjige
            // 
            this.cmbVezKnjige.FormattingEnabled = true;
            this.cmbVezKnjige.Location = new System.Drawing.Point(198, 205);
            this.cmbVezKnjige.Name = "cmbVezKnjige";
            this.cmbVezKnjige.Size = new System.Drawing.Size(274, 28);
            this.cmbVezKnjige.TabIndex = 5;
            // 
            // cmbTipKnjige
            // 
            this.cmbTipKnjige.FormattingEnabled = true;
            this.cmbTipKnjige.Location = new System.Drawing.Point(198, 171);
            this.cmbTipKnjige.Name = "cmbTipKnjige";
            this.cmbTipKnjige.Size = new System.Drawing.Size(274, 28);
            this.cmbTipKnjige.TabIndex = 4;
            // 
            // cmbOblastKnjige
            // 
            this.cmbOblastKnjige.FormattingEnabled = true;
            this.cmbOblastKnjige.Location = new System.Drawing.Point(198, 105);
            this.cmbOblastKnjige.Name = "cmbOblastKnjige";
            this.cmbOblastKnjige.Size = new System.Drawing.Size(274, 28);
            this.cmbOblastKnjige.TabIndex = 2;
            // 
            // cmbNazivKnjige
            // 
            this.cmbNazivKnjige.FormattingEnabled = true;
            this.cmbNazivKnjige.Location = new System.Drawing.Point(198, 71);
            this.cmbNazivKnjige.Name = "cmbNazivKnjige";
            this.cmbNazivKnjige.Size = new System.Drawing.Size(274, 28);
            this.cmbNazivKnjige.TabIndex = 1;
            this.cmbNazivKnjige.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChangePromenaKomboKnjiga);
            // 
            // frmIzdavacKnjiga
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 595);
            this.Controls.Add(this.tabKontrola);
            this.Name = "frmIzdavacKnjiga";
            this.ShowIcon = false;
            this.Text = "Upis novih knjiga";
            this.Load += new System.EventHandler(this.LoadPokretanjeForme);
            this.tabKontrola.ResumeLayout(false);
            this.tabStranaIzdavacKnjiga.ResumeLayout(false);
            this.tabStranaIzdavacKnjiga.PerformLayout();
            this.tabIzdavacKnjiga.ResumeLayout(false);
            this.tabIzdavacKnjiga.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridKnjige)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAutori)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabKontrola;
        private System.Windows.Forms.TabPage tabStranaIzdavacKnjiga;
        private System.Windows.Forms.ComboBox cmbNaziv;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtBrojTel;
        private System.Windows.Forms.TextBox txtGrad;
        private System.Windows.Forms.TextBox txtPostanski;
        private System.Windows.Forms.TextBox txtDrzava;
        private System.Windows.Forms.TabPage tabIzdavacKnjiga;
        private System.Windows.Forms.ComboBox cmbOcena;
        private System.Windows.Forms.Button btnUnesiNovogIzdavaca;
        private System.Windows.Forms.Button btnNoviZapis;
        private System.Windows.Forms.Button btnDesnoZapis;
        private System.Windows.Forms.Button btnLevoZapis;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIzdavacKnjigaTab;
        private System.Windows.Forms.TextBox txtBrojStranaKnjige;
        private System.Windows.Forms.ComboBox cmbVezKnjige;
        private System.Windows.Forms.ComboBox cmbTipKnjige;
        private System.Windows.Forms.ComboBox cmbOblastKnjige;
        private System.Windows.Forms.ComboBox cmbNazivKnjige;
        private System.Windows.Forms.DataGridView gridKnjige;
        private System.Windows.Forms.DataGridView gridAutori;
        private System.Windows.Forms.Button btnUnos;
        private System.Windows.Forms.Button btnUnesiAutore;
        private System.Windows.Forms.ComboBox cmbUnosAutoraKoautora;
        private System.ServiceProcess.ServiceController serviceController1;
        private System.Windows.Forms.Button btnUnesiKnjigeStavke;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtMestoKnjige;
        private System.Windows.Forms.ComboBox cmbOstecenaNeostecena;
        private System.Windows.Forms.ComboBox cmbSlobodnaZauzeta;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtIndetifikatorPosebneKnjige;
        private System.Windows.Forms.Button btnKnjigaRekordDesno;
        private System.Windows.Forms.Button btnKnjigaRekordLevo;
        private System.Windows.Forms.Button btnNoviRekordUpisKnjiga;
    }
}