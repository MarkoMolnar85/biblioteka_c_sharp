﻿namespace biblioteka_rad
{
    partial class frmParametarskiIzvestaj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridParametarskiUpit = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridParametarskiUpit)).BeginInit();
            this.SuspendLayout();
            // 
            // gridParametarskiUpit
            // 
            this.gridParametarskiUpit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridParametarskiUpit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridParametarskiUpit.Location = new System.Drawing.Point(0, 0);
            this.gridParametarskiUpit.Name = "gridParametarskiUpit";
            this.gridParametarskiUpit.Size = new System.Drawing.Size(1354, 574);
            this.gridParametarskiUpit.TabIndex = 0;
            // 
            // frmParametarskiIzvestaj
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 574);
            this.Controls.Add(this.gridParametarskiUpit);
            this.Name = "frmParametarskiIzvestaj";
            this.Text = "Pregled";
            this.Load += new System.EventHandler(this.loadPokretanjeForme);
            ((System.ComponentModel.ISupportInitialize)(this.gridParametarskiUpit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridParametarskiUpit;
    }
}