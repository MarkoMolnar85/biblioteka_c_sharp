﻿namespace biblioteka_rad
{
    partial class frmBibliotekarUnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSifraBibliotekara = new System.Windows.Forms.TextBox();
            this.txtImeBibliotekara = new System.Windows.Forms.TextBox();
            this.txtPrezimeBibliotekara = new System.Windows.Forms.TextBox();
            this.txtBrojFonaBibliotekara = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUnosNovogBibliotekara = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtSifraBibliotekara
            // 
            this.txtSifraBibliotekara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSifraBibliotekara.Location = new System.Drawing.Point(171, 12);
            this.txtSifraBibliotekara.Name = "txtSifraBibliotekara";
            this.txtSifraBibliotekara.Size = new System.Drawing.Size(201, 26);
            this.txtSifraBibliotekara.TabIndex = 0;
            // 
            // txtImeBibliotekara
            // 
            this.txtImeBibliotekara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImeBibliotekara.Location = new System.Drawing.Point(171, 44);
            this.txtImeBibliotekara.Name = "txtImeBibliotekara";
            this.txtImeBibliotekara.Size = new System.Drawing.Size(201, 26);
            this.txtImeBibliotekara.TabIndex = 1;
            // 
            // txtPrezimeBibliotekara
            // 
            this.txtPrezimeBibliotekara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrezimeBibliotekara.Location = new System.Drawing.Point(171, 76);
            this.txtPrezimeBibliotekara.Name = "txtPrezimeBibliotekara";
            this.txtPrezimeBibliotekara.Size = new System.Drawing.Size(201, 26);
            this.txtPrezimeBibliotekara.TabIndex = 2;
            // 
            // txtBrojFonaBibliotekara
            // 
            this.txtBrojFonaBibliotekara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrojFonaBibliotekara.Location = new System.Drawing.Point(171, 108);
            this.txtBrojFonaBibliotekara.Name = "txtBrojFonaBibliotekara";
            this.txtBrojFonaBibliotekara.Size = new System.Drawing.Size(201, 26);
            this.txtBrojFonaBibliotekara.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sifra bibliotekara";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Prezime";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "BrojTelefona";
            // 
            // btnUnosNovogBibliotekara
            // 
            this.btnUnosNovogBibliotekara.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUnosNovogBibliotekara.Font = new System.Drawing.Font("Book Antiqua", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnosNovogBibliotekara.ForeColor = System.Drawing.Color.Black;
            this.btnUnosNovogBibliotekara.Location = new System.Drawing.Point(16, 160);
            this.btnUnosNovogBibliotekara.Name = "btnUnosNovogBibliotekara";
            this.btnUnosNovogBibliotekara.Size = new System.Drawing.Size(356, 47);
            this.btnUnosNovogBibliotekara.TabIndex = 4;
            this.btnUnosNovogBibliotekara.Text = "Unos novog bibliotekara";
            this.btnUnosNovogBibliotekara.UseVisualStyleBackColor = false;
            this.btnUnosNovogBibliotekara.Click += new System.EventHandler(this.btnUnosNovogBibliotekara_Click);
            // 
            // frmBibliotekarUnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(394, 232);
            this.Controls.Add(this.btnUnosNovogBibliotekara);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBrojFonaBibliotekara);
            this.Controls.Add(this.txtPrezimeBibliotekara);
            this.Controls.Add(this.txtImeBibliotekara);
            this.Controls.Add(this.txtSifraBibliotekara);
            this.Name = "frmBibliotekarUnos";
            this.ShowIcon = false;
            this.Text = "Bibliotekar unos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSifraBibliotekara;
        private System.Windows.Forms.TextBox txtImeBibliotekara;
        private System.Windows.Forms.TextBox txtPrezimeBibliotekara;
        private System.Windows.Forms.TextBox txtBrojFonaBibliotekara;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnUnosNovogBibliotekara;
    }
}