﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmAutor : Form
    {
        public frmAutor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                AutorKlasa a1 = new AutorKlasa(txtImePrezime, txtGodina, txtDrzava, txtGrad);
                if (a1.PopuNjenaPolja())
                {
                    if (a1.ProveraGodinaOK())
                    {
                        a1.KonekcijaUnosAutora();
                        txtGrad.Clear(); txtImePrezime.Clear(); txtDrzava.Clear(); txtGodina.Clear();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
