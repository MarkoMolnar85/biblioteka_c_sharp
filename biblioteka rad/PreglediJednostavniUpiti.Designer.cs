﻿namespace biblioteka_rad
{
    partial class frmPreglediJednostavniUpiti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridJednostavniUpiti = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridJednostavniUpiti)).BeginInit();
            this.SuspendLayout();
            // 
            // gridJednostavniUpiti
            // 
            this.gridJednostavniUpiti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridJednostavniUpiti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridJednostavniUpiti.Location = new System.Drawing.Point(0, 0);
            this.gridJednostavniUpiti.Name = "gridJednostavniUpiti";
            this.gridJednostavniUpiti.Size = new System.Drawing.Size(1423, 540);
            this.gridJednostavniUpiti.TabIndex = 0;
            // 
            // frmPreglediJednostavniUpiti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1423, 540);
            this.Controls.Add(this.gridJednostavniUpiti);
            this.Name = "frmPreglediJednostavniUpiti";
            this.Text = "Pregled";
            this.Load += new System.EventHandler(this.LoadPokretanjeFormeJednostavniUpiti);
            ((System.ComponentModel.ISupportInitialize)(this.gridJednostavniUpiti)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridJednostavniUpiti;
    }
}