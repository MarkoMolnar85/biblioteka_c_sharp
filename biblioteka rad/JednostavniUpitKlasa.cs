﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;
using System.Drawing;
namespace biblioteka_rad
{
    class JednostavniUpitKlasa : UpitiKlasaAbstrakt
    {
        public JednostavniUpitKlasa(string traziSe, DataGridView grid, Form forma)
        {
            this.traziSe = traziSe;
            this.grid = grid;
            this.forma = forma;
        }

        public void IzbaciPregled()
        {
            switch (this.traziSe)
            {
                case "svi studenti": SviStudenti();
                    break;
                case "svi upisani autori": SviUpisaniAutori();
                    break;
                case "sve knjige": SveKnjige();
                    break;
                case "svi primerci knjiga": SviPrimerciKnjiga();
                    break;
                case "svi slobodni primerci knjiga": SviSlobodniPrimerciKnjiga();
                    break;
                case "svi zauzeti primerci knjiga": SviZauzetiPrimerciKnjiga();
                    break;
                case "svi osteceni primerci knjiga": SviOsteceniPrimerciKnjiga();
                    break;
                case "spisak ikada izdatih knjiga": SpisakIkadaIzdatihKnjiga();
                    break;
                case "prosecna ocena za svaku knjigu": ProsecnaOcenaZaSvakuKnjigu();
                    break;
                default:
                    break;
            }
        }
        protected override void VisinaForme(int rb)
        {
            base.VisinaForme(rb);
        }
        protected override void SirinaForme(int n)
        {
            base.SirinaForme(n);
        }
        private void SviStudenti()
        {     
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Broj indeksa");
            dt.Columns.Add("Ime studenta");
            dt.Columns.Add("Prezime studenta");
            dt.Columns.Add("pol");
            dt.Columns.Add("JMBG");
            dt.Columns.Add("Bracni statis");
            dt.Columns.Add("nacin finansirana");
            dt.Columns.Add("Godina rodjenja");
            dt.Columns.Add("Drzava");
            dt.Columns.Add("Grad");
            dt.Columns.Add("Telefon");
            dt.Columns.Add("Pouzdan");
            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komanda = new MySqlCommand())
                {
                    komanda.Connection = konekcija;
                    komanda.CommandText = "select * from student order by prezimeStudenta;";
                    MySqlDataReader citac = komanda.ExecuteReader();
                    int rb = 1;
                    while (citac.Read())
                    {
                        dt.Rows.Add(rb.ToString(), citac.GetString(1), citac.GetString(2),
                            citac.GetString(3), citac.GetString(4),
                            citac.GetString(5), citac.GetString(6), citac.GetString(7), citac.GetString(8),
                             citac.GetString(9), citac.GetString(10), citac.GetString(11), citac.GetString(12));
                        rb++;
                    }
                    citac.Close();
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                    VisinaForme(rb);
                    SirinaForme(dt.Columns.Count);
                }
            }
        }//kraj metode svi studenti
        private void SviUpisaniAutori()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Ime i prezime autora");
            dt.Columns.Add("Godina rodjenja");
            dt.Columns.Add("Drzava");
            dt.Columns.Add("Grad");
            dt.Columns.Add("Broj knjiga ovog autora kod nas");

            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaKljuceviAutora = new MySqlCommand())
                {
                    komandaKljuceviAutora.Connection = konekcija;
                    komandaKljuceviAutora.CommandText = "select autor_id from autor order by imePrezimeAutora;";
                    MySqlDataReader citac = komandaKljuceviAutora.ExecuteReader();
                    List<string> ListaKljuceva = new List<string>();
                    while (citac.Read())
                    {
                        ListaKljuceva.Add(citac.GetString(0));
                    }
                    citac.Close();
                    List<int> ListaBrojevaIzvrsenja = new List<int>();
                    for (int i = 0; i < ListaKljuceva.Count; i++)
                    {
                        using(MySqlCommand komandaBrojKljucevaDeteTabela = new MySqlCommand())
                        {
                            komandaBrojKljucevaDeteTabela.Connection = konekcija;
                            komandaBrojKljucevaDeteTabela.CommandText = "select * from autori where autor_autor_id = @kljuc;";
                            komandaBrojKljucevaDeteTabela.Parameters.AddWithValue("@kljuc", ListaKljuceva[i]);
                            citac = komandaBrojKljucevaDeteTabela.ExecuteReader();
                            int brojacIzvrsejna = 0;
                            while (citac.Read())
                            {
                                brojacIzvrsejna++;
                            }
                            citac.Close();
                            ListaBrojevaIzvrsenja.Add(brojacIzvrsejna);
                            brojacIzvrsejna = 0;
                        }
                    }
                    using (MySqlCommand komandaSviAutori = new MySqlCommand())
                    {
                        komandaSviAutori.Connection = konekcija;
                        komandaSviAutori.CommandText = "select * from autor order by imePrezimeAutora;";
                        citac = komandaSviAutori.ExecuteReader();
                        int brojac = 0;
                        while (citac.Read())
                        {
                            dt.Rows.Add((brojac+1),citac.GetString(1), citac.GetString(2), 
                                citac.GetString(3), citac.GetString(4), ListaBrojevaIzvrsenja[brojac]);
                            brojac++;
                        }
                        citac.Close();
                        BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                        VisinaForme(brojac); SirinaForme(dt.Columns.Count);
                    }
                }
            }
        }//kraj svi upisani autori

        private void SveKnjige()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Naziv knjige");
            dt.Columns.Add("Oblast");
            dt.Columns.Add("Broj strana");
            dt.Columns.Add("Tip");
            dt.Columns.Add("Vez");
            dt.Columns.Add("Izdavac");
            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaSoljniKljuceviIzdavaca = new MySqlCommand())
                {
                    komandaSoljniKljuceviIzdavaca.Connection = konekcija;
                    komandaSoljniKljuceviIzdavaca.CommandText = "select izdavac_izdavac_id from knjiga order by nazivKnjige;";
                    MySqlDataReader citac = komandaSoljniKljuceviIzdavaca.ExecuteReader();
                    List<string> ListaSpoljnihKljucevaIzdavaca = new List<string>();
                    while (citac.Read())
                    {
                        ListaSpoljnihKljucevaIzdavaca.Add(citac.GetString(0));
                    }
                    citac.Close();
                    for (int i = 0; i < ListaSpoljnihKljucevaIzdavaca.Count; i++)
                    {
                        using (MySqlCommand komandaDajIzdavace = new MySqlCommand())
                        {
                            komandaDajIzdavace.Connection = konekcija;
                            komandaDajIzdavace.CommandText = "select nazivIzdavaca from izdavac where izdavac_id = @kljucIzdavaca;";
                            komandaDajIzdavace.Parameters.AddWithValue("@kljucIzdavaca", ListaSpoljnihKljucevaIzdavaca[i]);
                            citac = komandaDajIzdavace.ExecuteReader();
                            while (citac.Read())
                            {
                                ListaSpoljnihKljucevaIzdavaca[i] = citac.GetString(0);
                            }
                            citac.Close();
                        }
                    }
                    using (MySqlCommand komandaSveKnjige = new MySqlCommand())
                    {
                        komandaSveKnjige.Connection = konekcija;
                        komandaSveKnjige.CommandText = "select * from knjiga order by nazivKnjige;";
                        int br = 0;
                        citac = komandaSveKnjige.ExecuteReader();
                        while (citac.Read())
                        {
                            dt.Rows.Add((br +1), citac.GetString(1), citac.GetString(2), citac.GetString(3), citac.GetString(4),
                                citac.GetString(5), ListaSpoljnihKljucevaIzdavaca[br]);
                            br++;
                        }
                        citac.Close();
                        BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                        VisinaForme(br); SirinaForme(dt.Columns.Count);
                    }
                }
            }
        }//kraj metode sve knjige

        private void SviPrimerciKnjiga() 
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BR");
            dt.Columns.Add("Naziv primerka");
            dt.Columns.Add("Slobodna");
            dt.Columns.Add("Ostecena?");
            dt.Columns.Add("Mesto knjige");
            dt.Columns.Add("Knjiga naziv");

            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaPrimerciKniga = new MySqlCommand())
                {
                    komandaPrimerciKniga.Connection = konekcija;
                    komandaPrimerciKniga.CommandText = "select indetifikator, slobodnaZauzeta, ostecenaNeostecena, mestoKnjige, " +
                        " nazivKnjige from knjige, knjiga where knjiga_knjiga_id = knjiga_id;";
                    MySqlDataReader citac = komandaPrimerciKniga.ExecuteReader();
                    int br = 0;
                    while (citac.Read())
                    {
                        dt.Rows.Add((br+1), citac.GetString(0), citac.GetString(1), citac.GetString(2), citac.GetString(3), citac.GetString(4));
                        br++;
                    }
                    citac.Close();
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                    VisinaForme(br); SirinaForme(dt.Columns.Count);
                }
            }
        }//kraj svi primerzi knjiga

        private void SviSlobodniPrimerciKnjiga()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BR");
            dt.Columns.Add("Naziv primerka");
            dt.Columns.Add("Slobodna");
            dt.Columns.Add("Ostecena?");
            dt.Columns.Add("Mesto knjige");
            dt.Columns.Add("Knjiga naziv");

            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaPrimerciKniga = new MySqlCommand())
                {
                    komandaPrimerciKniga.Connection = konekcija;
                    komandaPrimerciKniga.CommandText = "select indetifikator, slobodnaZauzeta, ostecenaNeostecena, mestoKnjige, " +
                        " nazivKnjige from knjige, knjiga where knjiga_knjiga_id = knjiga_id && slobodnaZauzeta = 'Slobodna';";
                    MySqlDataReader citac = komandaPrimerciKniga.ExecuteReader();
                    int br = 0;
                    while (citac.Read())
                    {
                        dt.Rows.Add((br + 1), citac.GetString(0), citac.GetString(1), citac.GetString(2), citac.GetString(3), citac.GetString(4));
                        br++;
                    }
                    citac.Close();
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                    VisinaForme(br); SirinaForme(dt.Columns.Count);
                }
            }
        }//kraj metode svi slobodni primerci knjiga
        private void SviZauzetiPrimerciKnjiga()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BR");
            dt.Columns.Add("Naziv primerka");
            dt.Columns.Add("Slobodna");
            dt.Columns.Add("Ostecena?");
            dt.Columns.Add("Mesto knjige");
            dt.Columns.Add("Knjiga naziv");

            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaPrimerciKniga = new MySqlCommand())
                {
                    komandaPrimerciKniga.Connection = konekcija;
                    komandaPrimerciKniga.CommandText = "select indetifikator, slobodnaZauzeta, ostecenaNeostecena, mestoKnjige, " +
                        " nazivKnjige from knjige, knjiga where knjiga_knjiga_id = knjiga_id && slobodnaZauzeta = 'Zauzeta';";
                    MySqlDataReader citac = komandaPrimerciKniga.ExecuteReader();
                    int br = 0;
                    while (citac.Read())
                    {
                        dt.Rows.Add((br + 1), citac.GetString(0), citac.GetString(1), citac.GetString(2), citac.GetString(3), citac.GetString(4));
                        br++;
                    }
                    citac.Close();
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                    VisinaForme(br); SirinaForme(dt.Columns.Count);
                }
            }
        }//kraj metode svi zauzeti primerci knjiga
        private void SviOsteceniPrimerciKnjiga()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BR");
            dt.Columns.Add("Naziv primerka");
            dt.Columns.Add("Slobodna");
            dt.Columns.Add("Ostecena?");
            dt.Columns.Add("Mesto knjige");
            dt.Columns.Add("Knjiga naziv");

            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaPrimerciKniga = new MySqlCommand())
                {
                    komandaPrimerciKniga.Connection = konekcija;
                    komandaPrimerciKniga.CommandText = "select indetifikator, slobodnaZauzeta, ostecenaNeostecena, mestoKnjige, " +
                        " nazivKnjige from knjige, knjiga where knjiga_knjiga_id = knjiga_id && ostecenaNeostecena !='Neostecena';";
                    MySqlDataReader citac = komandaPrimerciKniga.ExecuteReader();
                    int br = 0;
                    while (citac.Read())
                    {
                        dt.Rows.Add((br + 1), citac.GetString(0), citac.GetString(1), citac.GetString(2), citac.GetString(3), citac.GetString(4));
                        br++;
                    }
                    citac.Close();
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                    //grid.AutoResizeColumns();
                    VisinaForme(br); SirinaForme(dt.Columns.Count);
                    
                }
            }
        }//kraj metode svi osteceni primerci knjiga

        private void SpisakIkadaIzdatihKnjiga()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Sifra bibliotekara");
            dt.Columns.Add("Ime bibliotekara");
            dt.Columns.Add("Prezime bibliotekara");
            dt.Columns.Add("Broj indeksa");
            dt.Columns.Add("Ime studenta");
            dt.Columns.Add("Prezime studenta");
            dt.Columns.Add("Primerak knjige");
            dt.Columns.Add("Ocena");
            dt.Columns.Add("Datuma izdato");
            dt.Columns.Add("Predvidjeno vracanje");
            dt.Columns.Add("Datuma vraceno");
            dt.Columns.Add("Ostecena?");

            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komanda = new MySqlCommand())
                {
                    komanda.Connection = konekcija;
                    string komandaStr = "select datumaUzeto, datumaVraceno, izdatoVracenocol, " + 
                    " ocenaKnjige, izdatovraceno.ostecenaNeostecena, indetifikator, " +
                    " sifraRadnika, imeRadnika, prezimeRadnika, " +
                    " brojIndeksa, imeStudenta, prezimeStudenta from izdatovraceno, knjige, bibliotekar, student " +
                    " where student_id = student_student_id && knjige_id = knjige_knjige_id && " +
                    " bibliotekar_id=bibliotekar_bibliotekar_id;";
                    komanda.CommandText = komandaStr;
                    MySqlDataReader citac = komanda.ExecuteReader();
                    int br = 0;
                    while (citac.Read())
                    {
                        string sifraBibliotekara = citac.GetString(6);
                        string imeBibliotekara = citac.GetString(7);
                        string prezimeBibliotekara = citac.GetString(8);
                        string brojIndeksa = citac.GetString(9);
                        string imeStudenta = citac.GetString(10);
                        string prezimeStudenta = citac.GetString(11);
                        string primerakKnjige = citac.GetString(5);

                        string ocenaKnjige = "";
                        if (!citac.IsDBNull(3))
                        {
                            ocenaKnjige = citac.GetString(3);
                        }
                        
                        string datumaIzdato = citac.GetString(0);
                        string predvidjenoVracanje = citac.GetString(2);

                        string datumaVraceno = "";
                        if(!citac.IsDBNull(1))
                        {
                            datumaVraceno = citac.GetString(1);
                        }
                        
                        string ostecena = "";

                        if (!citac.IsDBNull(4))
                        {
                            ostecena = citac.GetString(4);
                        }

                        br++;
                        dt.Rows.Add((br), sifraBibliotekara, imeBibliotekara, prezimeBibliotekara, brojIndeksa,
                            imeStudenta, prezimeStudenta, primerakKnjige, ocenaKnjige, datumaIzdato,
                            predvidjenoVracanje, datumaVraceno, ostecena);
                    }
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                    VisinaForme(br); SirinaForme(dt.Rows.Count);
                }
            }
        }//kraj metode spisak ikada izdatih knjiga

        private void ProsecnaOcenaZaSvakuKnjigu()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Naziv knjige");
            dt.Columns.Add("Oblast");
            dt.Columns.Add("Prosecna ocena");
            dt.Columns.Add("Broj strana");
            dt.Columns.Add("Izdavac");

            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using(MySqlCommand komandaKnjiga = new MySqlCommand())
                {
                    komandaKnjiga.Connection = konekcija;
                    komandaKnjiga.CommandText = "select knjiga_id from knjiga;";
                    List<string> listaNaslova = new List<string>();
                    MySqlDataReader citac = komandaKnjiga.ExecuteReader();
                    while (citac.Read())
                    {
                        listaNaslova.Add(citac.GetString(0));
                    }
                    citac.Close();
                    List<List<string>> listaPrimerakaVelika = new List<List<string>>();
                    for (int i = 0; i < listaNaslova.Count; i++)
                    {
                        List<string> listaPrimerakaMala = new List<string>();
                        using (MySqlCommand komandaPrimerci = new MySqlCommand())
                        {
                            komandaPrimerci.Connection = konekcija;
                            komandaPrimerci.CommandText = "select knjige_id from knjige where knjiga_knjiga_id = @knjigaKljuc;";
                            komandaPrimerci.Parameters.AddWithValue("@knjigaKljuc", listaNaslova[i]);
                            citac = komandaPrimerci.ExecuteReader();
                            while (citac.Read())
                            {
                                listaPrimerakaMala.Add(citac.GetString(0));
                            }
                            citac.Close();
                            listaPrimerakaVelika.Add(listaPrimerakaMala);
                            
                        }
                    }///////////
                    List<List<List<string>>> VelikaLista = new List<List<List<string>>>();
                    for (int i = 0; i < listaPrimerakaVelika.Count; i++)
                    {
                        List<string> listaPrimerakaMala = listaPrimerakaVelika[i];
                        List<List<string>> listaPrimerakaSaOcenama = new List<List<string>>();
                        for (int j = 0; j < listaPrimerakaMala.Count; j++)
                        {
                            using (MySqlCommand komandaSveOcene = new MySqlCommand())
                            {
                                komandaSveOcene.Connection = konekcija;
                                komandaSveOcene.CommandText = "select ocenaKnjige from izdatovraceno where knjige_knjige_id = @kljucKnjige;";
                                komandaSveOcene.Parameters.AddWithValue("@kljucKnjige", listaPrimerakaMala[j]);
                                citac = komandaSveOcene.ExecuteReader();
                                List<string> listaOcenaMala = new List<string>();

                                while (citac.Read())
                                {
                                    if (!citac.IsDBNull(0))
                                    {
                                        listaOcenaMala.Add(citac.GetString(0));
                                    }
                                }
                                citac.Close();
                                listaPrimerakaSaOcenama.Add(listaOcenaMala);
                            }
                        }
                        VelikaLista.Add(listaPrimerakaSaOcenama);
                    }//kraj petlje
                    List<string> listaProseka = new List<string>();
                    for (int i = 0; i < VelikaLista.Count; i++)
                    {
                        List<List<string>> primerci = VelikaLista[i];
                        double sum = 0;
                        int brojacZaSvakiNaslov = 0;
                        for (int j = 0; j < primerci.Count; j++)
                        {
                            List<string> Ocene = primerci[j];
                            for (int k = 0; k < Ocene.Count; k++)
                            {
                                object obj = Ocene[k];
                                if (obj != null)
                                {
                                    brojacZaSvakiNaslov++;
                                    sum += double.Parse(Ocene[k]);
                                }
                            }
                        }
                        listaProseka.Add((sum / brojacZaSvakiNaslov).ToString());
                        brojacZaSvakiNaslov = 0; sum = 0;
                    }//kraj petlje
                    for (int i = 0; i < listaProseka.Count; i++)
                    {
                        if (double.IsNaN(double.Parse(listaProseka[i])))
                        {
                            listaProseka[i] = "Neocenjen";
                        }
                    }
                    using (MySqlCommand komandaOceneOstaloZaTabelu = new MySqlCommand())
                    {
                        komandaOceneOstaloZaTabelu.Connection = konekcija;
                        komandaOceneOstaloZaTabelu.CommandText = "select nazivKnjige, oblast, brojStrana, nazivIzdavaca from knjiga, izdavac " +
                            " where izdavac_id = izdavac_izdavac_id;";
                        citac = komandaOceneOstaloZaTabelu.ExecuteReader();
                        int br = 0;
                        while (citac.Read())
                        {
                            dt.Rows.Add((br + 1), citac.GetString(0), citac.GetString(1), listaProseka[br], citac.GetString(2), citac.GetString(3));
                            br++;
                        }
                        citac.Close();
                        BindingSource bs = new BindingSource(); bs.DataSource = dt; grid.DataSource = bs;
                        VisinaForme(br); SirinaForme(dt.Rows.Count);
                    }
                }
            }

            
        }//kraj metode za prosecnu ocenu

    }
}
