﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;
namespace biblioteka_rad
{
    class IzdavacKlasa
    {
        public static int brojacZaListu; 
        public string Naziv { get; set; }
        string Drzava { get; set; }
        string Grad { get; set; }
        string PostanskiBroj { get; set; }
        string BrojTelefona { get; set; }
        string Email { get; set; }
        int Ocena { get; set; }
        bool praznoPolje;

        public IzdavacKlasa(string naziv, string drzava, string grad, string postanskiBroj, string brojTelefona,
            string email, int ocena, string ulazZaListu)
        {
            this.Naziv = naziv; this.Drzava = drzava; this.Grad = grad; this.PostanskiBroj = postanskiBroj;
            this.BrojTelefona = brojTelefona; this.Email = email; this.Ocena = ocena;
        }
        public IzdavacKlasa(ComboBox naziv, TextBox drzava, TextBox grad, TextBox postanskiBroj, TextBox brojTelefona,
            TextBox email, ComboBox ocena)
        {
            if (String.IsNullOrEmpty(drzava.Text) ||
                String.IsNullOrEmpty(grad.Text) || String.IsNullOrEmpty(postanskiBroj.Text) ||
                String.IsNullOrEmpty(brojTelefona.Text) || String.IsNullOrEmpty(email.Text) )
            {
                praznoPolje = true;
                MessageBox.Show("Potrebno je popuniti sva polja");
            }
            else if (String.IsNullOrEmpty(naziv.Text)) { praznoPolje = true; }
            else
            {
                bool OkOcenaU = false;
                int ocenaU = 0; 
                praznoPolje = false;
                this.Naziv = naziv.Text; this.Drzava = drzava.Text; this.Grad = grad.Text; this.PostanskiBroj = postanskiBroj.Text;
                this.BrojTelefona = brojTelefona.Text; this.Email = email.Text;
                OkOcenaU = int.TryParse(ocena.Text, out ocenaU);
                if (OkOcenaU) { this.Ocena = ocenaU; }

            }
        }
        public bool PopunjenaPoljaOK()
        {
            if (praznoPolje == true) { return false; } else { return true; }
        }
        public static void FiksniKomboBoxOcena(ComboBox kombo)
        {
            kombo.Text = "3";
            kombo.Items.Add("1"); kombo.Items.Add("2");kombo.Items.Add("3");kombo.Items.Add("4");kombo.Items.Add("5");
        }
        public bool ProveraOceneOK(ComboBox kombo)
        {
            string kom = kombo.Text;
            if (kom == "1" || kom == "2" || kom == "3" || kom == "4" || kom == "5")
            {
                return true;
            }
            else
            {
                MessageBox.Show("Potrebno je uneto ocenu od 1-5");
                return false;
            }
        }
        public void KonekcijaUnosIzdavaca(List<IzdavacKlasa>listaIzdavaca)
        {
            using (MySqlConnection konekcija = new MySqlConnection("server=localhost;uid=root;password=;database=biblioteka;"))
            {
                konekcija.Open();
                string unosIzdavacaStr = "insert into izdavac (nazivIzdavaca, drzavaIzdavaca, gradIzdavaca, " +
                    "postanskiBrojIzdavaca, brojTelefonaIzdavaca, emailIzdavaca, ocenaIzdavaca) " +
                    "values(@naziv, @drzava, @grad, @postanski, @telefon, @email, @ocena);";
                using (MySqlCommand komandaUnos = new MySqlCommand(unosIzdavacaStr, konekcija))
                {
                    komandaUnos.Parameters.AddWithValue("@naziv", this.Naziv);
                    komandaUnos.Parameters.AddWithValue("@drzava", this.Drzava);
                    komandaUnos.Parameters.AddWithValue("@grad", this.Grad);
                    komandaUnos.Parameters.AddWithValue("@postanski", this.PostanskiBroj);
                    komandaUnos.Parameters.AddWithValue("@telefon", this.BrojTelefona);
                    komandaUnos.Parameters.AddWithValue("@email", this.Email);
                    komandaUnos.Parameters.AddWithValue("@ocena", this.Ocena);
                    komandaUnos.ExecuteNonQuery();
                }
            }//kraj using
            listaIzdavaca.Add(new IzdavacKlasa(this.Naziv, this.Drzava, this.Grad, this.PostanskiBroj, this.BrojTelefona,
                this.Email, this.Ocena, "ulazZaListu"));
        }
        public static void PopunjavanjeListe(List<IzdavacKlasa>listaIzdavaca)
        {
            
            using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=; database=biblioteka;"))
            {
                string konStr = "select * from izdavac order by nazivIzdavaca;";
                using (MySqlCommand komanda = new MySqlCommand(konStr, konekcija))
                {
                    DataTable tabela = new DataTable();
                    MySqlDataAdapter adapter = new MySqlDataAdapter(komanda);
                    adapter.Fill(tabela);
                    for (int i = 0; i < tabela.Rows.Count; i++)
                    {
                        string naziv = tabela.Rows[i]["nazivIzdavaca"].ToString();
                        string drzava = tabela.Rows[i]["drzavaIzdavaca"].ToString();
                        string grad = tabela.Rows[i]["gradIzdavaca"].ToString();
                        string postanskiBroj = tabela.Rows[i]["postanskiBrojIzdavaca"].ToString();
                        string brojTelefona = tabela.Rows[i]["brojTelefonaIzdavaca"].ToString();
                        string mail = tabela.Rows[i]["emailIzdavaca"].ToString();
                        int ocena = Convert.ToInt16(tabela.Rows[i]["ocenaIzdavaca"].ToString());
                        IzdavacKlasa izd = new IzdavacKlasa(naziv, drzava, grad, postanskiBroj, brojTelefona, mail, ocena, "ulazZaListu");
                        listaIzdavaca.Add(izd);
                        
                    }
                }
            }//kraj prvog usinga
        }//kraj metode
        public static void Rekordlevo(ComboBox naziv, TextBox drzava, TextBox grad, TextBox postanskiBroj, TextBox brojTelefona,
            TextBox email, ComboBox ocena, List<IzdavacKlasa>listaIzdavaca)
        {
            --IzdavacKlasa.brojacZaListu;
            if (IzdavacKlasa.brojacZaListu < 0)
            {
                IzdavacKlasa.brojacZaListu = listaIzdavaca.Count - 1;
            }
            int index = IzdavacKlasa.brojacZaListu;
            naziv.Text = listaIzdavaca[index].Naziv; drzava.Text = listaIzdavaca[index].Drzava;

            grad.Text = listaIzdavaca[index].Grad; postanskiBroj.Text = listaIzdavaca[index].PostanskiBroj;
            brojTelefona.Text = listaIzdavaca[index].BrojTelefona; email.Text = listaIzdavaca[index].Email;
            ocena.Text = listaIzdavaca[index].Ocena.ToString();

        }
        public static void RekorDesno(ComboBox naziv, TextBox drzava, TextBox grad, TextBox postanskiBroj, TextBox brojTelefona,
            TextBox email, ComboBox ocena, List<IzdavacKlasa> listaIzdavaca)
        {
            ++IzdavacKlasa.brojacZaListu;
            if (IzdavacKlasa.brojacZaListu >= listaIzdavaca.Count)
            {
                IzdavacKlasa.brojacZaListu = 0;
            }
            int index = IzdavacKlasa.brojacZaListu;
            naziv.Text = listaIzdavaca[index].Naziv; drzava.Text = listaIzdavaca[index].Drzava;

            grad.Text = listaIzdavaca[index].Grad; postanskiBroj.Text = listaIzdavaca[index].PostanskiBroj;
            brojTelefona.Text = listaIzdavaca[index].BrojTelefona; email.Text = listaIzdavaca[index].Email;
            ocena.Text = listaIzdavaca[index].Ocena.ToString();

        }
        public static void CiscenjeZaUpisNovog(ComboBox naziv, TextBox drzava,
            TextBox grad, TextBox postanskiBroj, TextBox brojTelefona,
            TextBox email, ComboBox ocena)
        {
            naziv.Text = ""; drzava.Clear(); grad.Clear(); postanskiBroj.Clear(); brojTelefona.Clear();
            email.Clear(); ocena.Text = "";
        }
        public static void PromenaTextaNaKomboBoxu(ComboBox komboBox, List<IzdavacKlasa>ListaIzdavaca)
        {
            string kombo ="";
            komboBox.Items.Clear();
            komboBox.SelectionStart = komboBox.Text.Length;
            komboBox.SelectionLength = 0;
            if(String.IsNullOrEmpty(komboBox.Text))
            {
                kombo = "";            
            }
            else
            {
                kombo = komboBox.Text;
            }
            int brPoklapanja = 0;
            for (int i = 0; i < ListaIzdavaca.Count; i++)
            {
                int duzinaIzListe = ListaIzdavaca[i].Naziv.Length;
                for (int j = 0; j < kombo.Length && j < duzinaIzListe; j++)
                {
                    if (kombo[j] == ListaIzdavaca[i].Naziv[j])
                    {
                        brPoklapanja++;
                    }
                }
                if(brPoklapanja == kombo.Length)
                komboBox.Items.Add(ListaIzdavaca[i].Naziv);
                brPoklapanja = 0;
            }

        }//kraj metode
        public static void PopunjavanjeNaPromenu(ComboBox naziv, TextBox drzava, TextBox grad, TextBox postanskiBroj, TextBox brojTelefona,
            TextBox email, ComboBox ocena, List<IzdavacKlasa>listaIzdavaca)
        {
            int rBroj = 0;
            foreach (var v in listaIzdavaca)
            {
                if (v.Naziv == naziv.Text)
                {
                    IzdavacKlasa.brojacZaListu = rBroj;
                }
                rBroj++;
            }
            naziv.Text = listaIzdavaca[IzdavacKlasa.brojacZaListu].Naziv;
            drzava.Text = listaIzdavaca[IzdavacKlasa.brojacZaListu].Drzava;
            grad.Text = listaIzdavaca[IzdavacKlasa.brojacZaListu].Grad;
            postanskiBroj.Text = listaIzdavaca[IzdavacKlasa.brojacZaListu].PostanskiBroj;
            brojTelefona.Text = listaIzdavaca[IzdavacKlasa.brojacZaListu].BrojTelefona;
            email.Text = listaIzdavaca[IzdavacKlasa.brojacZaListu].Email;
            ocena.Text = listaIzdavaca[IzdavacKlasa.brojacZaListu].Ocena.ToString();

        }

    }
}
