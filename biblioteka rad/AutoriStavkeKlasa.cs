﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace biblioteka_rad
{
    class AutoriStavkeKlasa
    {
        string imeKnjige;

        public string ImeKnjige
        {
            get { return imeKnjige; }
            set { imeKnjige = value; }
        }
        string imeAutora;

        public string ImeAutora
        {
            get { return imeAutora; }
            set { imeAutora = value; }
        }
        bool praznaPolja;
        public AutoriStavkeKlasa(string nazivKnjige, string imeAutora, string str)
        {
            this.imeKnjige = nazivKnjige; this.imeAutora = imeAutora;
        }
        public AutoriStavkeKlasa(ComboBox nazivKnjige, ComboBox imeAutora)
        {
            if (String.IsNullOrEmpty(nazivKnjige.Text) || String.IsNullOrEmpty(imeAutora.Text))
            {
                MessageBox.Show("Autor treba biti izabran");
                praznaPolja = true;
            }
            else
            {
                this.imeKnjige = nazivKnjige.Text; this.imeAutora = imeAutora.Text;
                praznaPolja = false;
            }
        }
        public bool AutorStavkaPoljaUnetaOK()
        {
            if(praznaPolja)
            {return false;}
            else
            {return true;}
        }
        public void KonekcijaUnosAutoraStavke()
        {
            string konekcijaStr = "server=localhost; uid=root; password=; database=biblioteka;";
            using (MySqlConnection konekcija = new MySqlConnection(konekcijaStr))
            {
                konekcija.Open();
                string nadjiAutorForgenStr = "select autor_id from autor where imePrezimeAutora = @imeAutora;";
                using (MySqlCommand komandaNadjiAutorKey = new MySqlCommand(nadjiAutorForgenStr, konekcija))
                {
                    komandaNadjiAutorKey.Parameters.AddWithValue("@imeAutora", this.imeAutora);
                    MySqlDataReader citac = komandaNadjiAutorKey.ExecuteReader();
                    string autorForgenJe = "";
                    while (citac.Read())
                    {
                        autorForgenJe = citac.GetString(0);
                    }
                   
                    citac.Close();
                    string nadjiKnjiguKey = "select knjiga_id from knjiga where nazivKnjige = @nazivKnjige;";
                    using(MySqlCommand komandaNadjiKnjiguForgen = new MySqlCommand(nadjiKnjiguKey, konekcija))
                    {
                        komandaNadjiKnjiguForgen.Parameters.AddWithValue("@nazivKnjige", this.imeKnjige);
                        citac = komandaNadjiKnjiguForgen.ExecuteReader();
                        string knjigaForgenJe = "";
                        while (citac.Read())
                        {
                            knjigaForgenJe = citac.GetString(0);
                        }
                        citac.Close();
                        using (MySqlCommand komandaUnos = new MySqlCommand())
                        {
                            komandaUnos.Connection = konekcija;
                            komandaUnos.CommandText = "insert into autori (autor_autor_id, knjiga_knjiga_id) values(@autorID, @knjigaID);";
                            komandaUnos.Parameters.AddWithValue("@autorID", autorForgenJe);
                            komandaUnos.Parameters.AddWithValue("@knjigaID", knjigaForgenJe);
                            komandaUnos.ExecuteNonQuery();

                        }
                        

                    }
                }
            }
        }//kraj funkcije unos autora
    }
}
