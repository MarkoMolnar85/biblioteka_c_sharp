﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;
using System.Drawing;
namespace biblioteka_rad
{
    abstract class UpitiKlasaAbstrakt
    {
        protected string traziSe;
        protected DataGridView grid;
        protected Form forma;

        protected virtual void VisinaForme(int rb)
        {
            int razlikaFormaGrid = this.forma.Height - this.grid.Height;
            if (razlikaFormaGrid < 0) { razlikaFormaGrid = 0; }
            int visinaReda = grid.Rows[0].Height * rb;
            if (visinaReda + razlikaFormaGrid + grid.Columns[0].HeaderCell.Size.Height < this.forma.Height)
            {
                this.forma.Height = visinaReda + razlikaFormaGrid + grid.Columns[0].HeaderCell.Size.Height;
            }
        }
        protected virtual void SirinaForme(int n)
        {
            grid.AutoResizeColumns();

            int sirinaKolona = 0;
            for (int i = 0; i < grid.Columns.Count; i++)
            {
                sirinaKolona += grid.Columns[i].Width;
            }

            if (grid.Width - (grid.Width - sirinaKolona) + grid.Rows[0].HeaderCell.Size.Width < this.forma.Width)
            {
                forma.Width = grid.Width - (grid.Width - sirinaKolona) + grid.Rows[0].HeaderCell.Size.Width + 35;
            }
        }
    }
}
