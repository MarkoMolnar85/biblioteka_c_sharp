﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
namespace biblioteka_rad
{
    public partial class frmStudent : Form
    {
        MySqlConnection konekcija;
        List<StudentKlasa> ListaStudenata;
        string putanjaSlike = "";
        public frmStudent()
        {
            InitializeComponent();
        }
        void KomboBoxoviFiksniStudent()
        {
            //ispod za unos
            cmbBracnStatus.Items.Add("Slobodan");
            cmbBracnStatus.Items.Add("Slobodna");
            cmbBracnStatus.Items.Add("Ozenjen");
            cmbBracnStatus.Items.Add("Udata");
            cmbBracnStatus.Items.Add("Razveden");
            cmbBracnStatus.Items.Add("Razvedena");
            cmbBracnStatus.Items.Add("Udovac");
            cmbBracnStatus.Items.Add("Udovica");

            cmbPol.Items.Add("Musko"); cmbPol.Items.Add("Zensko");
            cmbFinansiranje.Items.Add("Buzet"); cmbFinansiranje.Items.Add("Samofinansiranje");
            cmbPouzdanost.Items.Add("Pouzdan"); cmbPouzdanost.Items.Add("Srednje"); cmbPouzdanost.Items.Add("Nepouzdan"); 
            //ispod za pregled
            cmbBracniStatusPre.Items.Add("Slobodan");
            cmbBracniStatusPre.Items.Add("Slobodna");
            cmbBracniStatusPre.Items.Add("Ozenjen");
            cmbBracniStatusPre.Items.Add("Udata");
            cmbBracniStatusPre.Items.Add("Razveden");
            cmbBracniStatusPre.Items.Add("Razvedena");
            cmbBracniStatusPre.Items.Add("Udovac");
            cmbBracniStatusPre.Items.Add("Udovica");
            cmbPolPre.Items.Add("Musko"); cmbPolPre.Items.Add("Zensko");
            cmbFinansiranjePre.Items.Add("Buzet"); cmbFinansiranjePre.Items.Add("Samofinansiranje");
            cmbPouzdanostPre.Items.Add("Pouzdan"); cmbPouzdanostPre.Items.Add("Srednje"); cmbPouzdanostPre.Items.Add("Nepouzdan"); 
        }
        void DefoltniZaUpis()
        {
            cmbFinansiranje.Text = "Buzet";
            txtDrzava.Text = "Srbija";
            txtGrad.Text = "Uzice";
            cmbPouzdanost.Text = "Pouzdan";
            
        }
        private void OtvaranjeFormeEvent(object sender, EventArgs e)
        {
            try
            {
                int brStudenata = 0;
                StudentKlasa.RedniProj = 0;
                KomboBoxoviFiksniStudent();
                DefoltniZaUpis();
                ListaStudenata = new List<StudentKlasa>();
                string konekcioniString = "SERVER=localhost;UID=root;password=;DATABASE=biblioteka;";
                using (MySqlConnection konekcija = new MySqlConnection(konekcioniString))
                {
                    konekcija.Open();
                    using (MySqlCommand komandaUnesiSve = new MySqlCommand())
                    {
                        komandaUnesiSve.Connection = konekcija;
                        string upit = "select * from student order by prezimeStudenta;";
                        komandaUnesiSve.CommandText = upit;
                        DataTable tabelaStudenata = new DataTable();
                        MySqlDataAdapter adapter = new MySqlDataAdapter(komandaUnesiSve);
                        adapter.Fill(tabelaStudenata);
                        for (int i = 0; i < tabelaStudenata.Rows.Count; i++)
                        {
                            StudentKlasa studentUlistu = new StudentKlasa("studentUlistu",
                                tabelaStudenata.Rows[i]["brojIndeksa"].ToString(), tabelaStudenata.Rows[i]["imeStudenta"].ToString(),
                                tabelaStudenata.Rows[i]["prezimeStudenta"].ToString(), tabelaStudenata.Rows[i]["polStudenta"].ToString(),
                                tabelaStudenata.Rows[i]["jmbgStudenta"].ToString(), tabelaStudenata.Rows[i]["bracniStatusStudenta"].ToString(),
                                tabelaStudenata.Rows[i]["nacinFinansiranja"].ToString(), tabelaStudenata.Rows[i]["godinaRodjenjaStudenta"].ToString(),
                                tabelaStudenata.Rows[i]["drzavaStudenta"].ToString(), tabelaStudenata.Rows[i]["gradStudenta"].ToString(),
                                tabelaStudenata.Rows[i]["brojTelefonaStudenta"].ToString(), tabelaStudenata.Rows[i]["nivoPoverenjaStudenta"].ToString());
                            if (tabelaStudenata.Rows[i]["slika"] != DBNull.Value)
                            {
                                studentUlistu.SlikaStudenta = Image.FromStream(new MemoryStream((byte[])tabelaStudenata.Rows[i]["slika"]));
                            }
                            else
                            {
                                studentUlistu.SlikaStudenta = null;
                            }

                            ListaStudenata.Add(studentUlistu);
                            brStudenata++;
                        }
                    }
                }
                if (brStudenata > 0)
                {
                    unosPodatakaTabPregleda();
                    OplemenjivanjeKomboBoxa();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
               
               
        }

        private void BiranjePola(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPol.Text) && String.IsNullOrEmpty(cmbBracnStatus.Text))
            {
                if (cmbPol.Text == "Musko")
                    cmbBracnStatus.Text = "Slobodan";
                else
                    cmbBracnStatus.Text = "Slobodna";
            }
        }

        private void UnosNovogStudenta(object sender, EventArgs e)
        {
            try
            {
                StudentKlasa s1 = new StudentKlasa(txtBrojIndeksa, txtIme, txtPrezime, cmbPol,
                    txtJmbg, cmbBracnStatus, cmbFinansiranje, txtDatumRodjenja, txtDrzava, txtGrad,
                    txtTelefon, cmbPouzdanost);
                if (s1.NijePopunjeno() != "popunjeno")
                {
                    MessageBox.Show(s1.NijePopunjeno());
                }
                else
                {
                    if (s1.korektanIndeks() && s1.KorektanPol() && s1.JMBGkorektan() && s1.bracnoStanjeKorektno() &&
                        s1.finansiranjeKorektno() && s1.podobanKorektno())
                    {
                        string konekcioniString = "SERVER=localhost;UID=root;password=;DATABASE=biblioteka;";
                        using (konekcija = new MySqlConnection(konekcioniString))
                        {
                            using (MySqlCommand komandaUnosStudenta = new MySqlCommand())
                            {
                                konekcija.Open();
                                komandaUnosStudenta.Connection = konekcija;

                                byte[] biti = null;
                                if (imgSlikaStudenta.Image != null)
                                {
                                    FileStream sTok = new FileStream(putanjaSlike, FileMode.Open, FileAccess.Read);
                                    BinaryReader bRider = new BinaryReader(sTok);
                                    biti = bRider.ReadBytes((int)sTok.Length);
                                }

                                komandaUnosStudenta.CommandText = "insert into student(brojIndeksa, imeStudenta, prezimeStudenta," +
                                    " polStudenta, jmbgStudenta, bracniStatusStudenta, nacinFinansiranja, godinaRodjenjaStudenta," +
                                    "drzavaStudenta, gradStudenta, brojTelefonaStudenta, nivoPoverenjaStudenta, slika)" +
                                    " values(@brIndeksa, @imeStudenta, @prezimeStudenta, @polStudenta, @jmbgStudenta, " +
                                "@bracniStatusS, @nacinFinansiranjaS, @godinaRodjenjaS, @drzavaS, @gradS, @brFonaS, @nivoPoverenjaS, @slikaStudentaPar);";
                                komandaUnosStudenta.Parameters.AddWithValue("@brIndeksa", s1.BrIndeksa);
                                komandaUnosStudenta.Parameters.AddWithValue("@imeStudenta", s1.Ime);
                                komandaUnosStudenta.Parameters.AddWithValue("@prezimeStudenta", s1.Prezime);
                                komandaUnosStudenta.Parameters.AddWithValue("@polStudenta", s1.Pol);
                                komandaUnosStudenta.Parameters.AddWithValue("@jmbgStudenta", s1.Jmbg);
                                komandaUnosStudenta.Parameters.AddWithValue("@bracniStatusS", s1.BracniStatus);
                                komandaUnosStudenta.Parameters.AddWithValue("@nacinFinansiranjaS", s1.Finansiranje);
                                komandaUnosStudenta.Parameters.AddWithValue("@godinaRodjenjaS", s1.Godina);
                                komandaUnosStudenta.Parameters.AddWithValue("@drzavaS", s1.Drzava);
                                komandaUnosStudenta.Parameters.AddWithValue("@gradS", s1.Grad);
                                komandaUnosStudenta.Parameters.AddWithValue("@brFonaS", s1.Telefon);
                                komandaUnosStudenta.Parameters.AddWithValue("@nivoPoverenjaS", s1.Poverenje);
                                if (imgSlikaStudenta.Image == null)
                                {
                                    komandaUnosStudenta.Parameters.Add(new MySqlParameter("@slikaStudentaPar", DBNull.Value));
                                }
                                else
                                {
                                    komandaUnosStudenta.Parameters.Add(new MySqlParameter("@slikaStudentaPar", biti));
                                }
                                komandaUnosStudenta.ExecuteNonQuery();
                            }
                        }
                        if (imgSlikaStudenta.Image == null)
                        {
                            s1.SlikaStudenta = null;
                        }
                        else
                        {
                            s1.SlikaStudenta = imgSlikaStudenta.Image;
                        }
                        ListaStudenata.Add(s1);
                        txtBrojIndeksa.Clear(); txtIme.Clear(); txtPrezime.Clear(); cmbPol.Text = ""; txtJmbg.Clear();
                        cmbBracnStatus.Text = ""; cmbFinansiranje.Text = ""; txtDatumRodjenja.Clear(); txtDrzava.Clear(); txtGrad.Clear();
                        cmbPouzdanost.Text = ""; imgSlikaStudenta.Image = null;

                    }
                    else
                    {
                        string staNijeIspravnoUneto = "";
                        if (!s1.korektanIndeks())
                            staNijeIspravnoUneto += StudentKlasa.NeIspravanBrojInkdeksaStr();
                        if (!s1.KorektanPol())
                            staNijeIspravnoUneto += StudentKlasa.NeIspravnoUnetPol();
                        if(!s1.JMBGkorektan())
                            staNijeIspravnoUneto += StudentKlasa.NeispravanJMBG();
                        if(!s1.bracnoStanjeKorektno())
                            staNijeIspravnoUneto += StudentKlasa.NeispravnoBracnoStanje();
                        if(!s1.finansiranjeKorektno())
                            staNijeIspravnoUneto += StudentKlasa.NeispravnoFinansiranje();
                        if(!s1.podobanKorektno())
                            staNijeIspravnoUneto += StudentKlasa.NeispravnoPodoban();
                        MessageBox.Show(staNijeIspravnoUneto);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
             
        }
        void unosPodatakaTabPregleda()
        {
            cmbBrojIndeksaPre.Text = ListaStudenata[StudentKlasa.RedniProj].BrIndeksa;
            txtImePre.Text = ListaStudenata[StudentKlasa.RedniProj].Ime;
            txtPrezimePre.Text = ListaStudenata[StudentKlasa.RedniProj].Prezime;
            cmbPolPre.Text = ListaStudenata[StudentKlasa.RedniProj].Pol;
            txtJmbgPre.Text = ListaStudenata[StudentKlasa.RedniProj].Jmbg;
            cmbBracniStatusPre.Text = ListaStudenata[StudentKlasa.RedniProj].BracniStatus;
            cmbFinansiranjePre.Text = ListaStudenata[StudentKlasa.RedniProj].Finansiranje;
            txtGodinaRodjenjaPre.Text = ListaStudenata[StudentKlasa.RedniProj].Godina;
            txtDrzavaPre.Text = ListaStudenata[StudentKlasa.RedniProj].Drzava;
            txtGradPre.Text = ListaStudenata[StudentKlasa.RedniProj].Grad;
            txtTelefonPre.Text = ListaStudenata[StudentKlasa.RedniProj].Telefon;
            cmbPouzdanostPre.Text = ListaStudenata[StudentKlasa.RedniProj].Poverenje;
            imgSlikaPregled.Image = ListaStudenata[StudentKlasa.RedniProj].SlikaStudenta;
            imgSlikaPregled.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void RekordDesno(object sender, EventArgs e)
        {
            StudentKlasa.RedniProj++;
            if (StudentKlasa.RedniProj >= ListaStudenata.Count)
            {
                StudentKlasa.RedniProj = 0;
            }
            
            unosPodatakaTabPregleda();
        }

        private void RekordLevo(object sender, EventArgs e)
        {
            StudentKlasa.RedniProj--;
            if (StudentKlasa.RedniProj <= 0)
            {
                StudentKlasa.RedniProj = ListaStudenata.Count-1;
            }
            unosPodatakaTabPregleda();
        }
        private void OplemenjivanjeKomboBoxa()
        {
            foreach (var v in ListaStudenata)
            {
                cmbBrojIndeksaPre.Items.Add(v.BrIndeksa.ToString());
            }
        }

        private void PromeKomboBoxa(object sender, EventArgs e)
        {
            int i = 0;
            foreach (var v in ListaStudenata)
            {
                if (v.BrIndeksa == cmbBrojIndeksaPre.Text)
                {
                    StudentKlasa.RedniProj = i;
                }
                i++;
            }
            unosPodatakaTabPregleda();
        }

        private void PromenaTextaSkracenjeListe(object sender, EventArgs e)
        {
            try
            {
                cmbBrojIndeksaPre.Items.Clear();
                cmbBrojIndeksaPre.SelectionStart = cmbBrojIndeksaPre.Text.Length;
                cmbBrojIndeksaPre.SelectionLength = 0;
                string komboText= string.Copy(cmbBrojIndeksaPre.Text);
                int komboDuzina = komboText.Length;
                int listaDuzina = ListaStudenata.Count;
                int brojac = 0;
                foreach (var v in ListaStudenata)
                {
                    string izListe = string.Copy(v.BrIndeksa);
                    for (int i = 0; i < komboDuzina && i < izListe.Length; i++)
                    {
                        if (komboText[i] == izListe[i])
                        {
                            brojac++;
                        }
                    }
                    if (brojac == komboText.Length)
                    {
                        cmbBrojIndeksaPre.Items.Add(izListe);
                    }
                    brojac = 0;
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void IzmeniPodatkeStudenta(object sender, EventArgs e)
        {
            try
            {
                string konekcioniString = "SERVER=localhost;UID=root;password=;DATABASE=biblioteka;";
                using (MySqlConnection konekcija = new MySqlConnection(konekcioniString))
                {
                    konekcija.Open();
                    using (MySqlCommand postojanjeIndeksa = new MySqlCommand())
                    {
                        postojanjeIndeksa.Connection = konekcija;
                        if (String.IsNullOrEmpty(cmbBrojIndeksaPre.Text))
                        {
                            MessageBox.Show("Broj indeksa mora biti unet");
                        }
                        else
                        {
                            postojanjeIndeksa.CommandText = "select brojIndeksa from student where brojIndeksa = @brIndeksa";
                            postojanjeIndeksa.Parameters.AddWithValue("@brIndeksa", cmbBrojIndeksaPre.Text);
                            MySqlDataReader citac = postojanjeIndeksa.ExecuteReader();
                            int brojacRedova = 0;
                            while (citac.Read())
                            {
                                brojacRedova++;
                            }
                            citac.Close();
                            if (brojacRedova == 0)
                            {
                                MessageBox.Show("Ne postoji student sa ovim brojem indeksa");
                            }
                            else
                            {
                                string izmeniPodatkeStudentaUpit = "update student set imeStudenta = @imeStudenta, prezimeStudenta =@prezimeStudenta, " +
                                    " polStudenta = @polStudenta, jmbgStudenta = @jmbgStudenta, bracniStatusStudenta = @bracniStatusS, " +
                                    " nacinFinansiranja = @nacinFinansiranjaS, godinaRodjenjaStudenta = @godinaRodjenjaS, " +
                                    "drzavaStudenta = @drzavaS, gradStudenta = @gradS, brojTelefonaStudenta = @brFonaS, nivoPoverenjaStudenta = @nivoPoverenjaS " +
                                    "where brojIndeksa=@brIndeksa ";
                                using (MySqlCommand komandaPromeneUpdate = new MySqlCommand(izmeniPodatkeStudentaUpit, konekcija))
                                {
                                    ListaStudenata[StudentKlasa.RedniProj].BrIndeksa = cmbBrojIndeksaPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Ime = txtImePre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Prezime = txtPrezimePre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Pol = cmbPolPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Jmbg = txtJmbgPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].BracniStatus = cmbBracniStatusPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Finansiranje = cmbFinansiranjePre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Godina = txtGodinaRodjenjaPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Drzava = txtDrzavaPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Grad = txtGradPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Telefon = txtTelefonPre.Text;
                                    ListaStudenata[StudentKlasa.RedniProj].Poverenje = cmbPouzdanostPre.Text;


                                    komandaPromeneUpdate.Parameters.AddWithValue("@brIndeksa", ListaStudenata[StudentKlasa.RedniProj].BrIndeksa);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@imeStudenta", ListaStudenata[StudentKlasa.RedniProj].Ime);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@prezimeStudenta", ListaStudenata[StudentKlasa.RedniProj].Prezime);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@polStudenta", ListaStudenata[StudentKlasa.RedniProj].Pol);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@jmbgStudenta", ListaStudenata[StudentKlasa.RedniProj].Jmbg);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@bracniStatusS", ListaStudenata[StudentKlasa.RedniProj].BracniStatus);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@nacinFinansiranjaS", ListaStudenata[StudentKlasa.RedniProj].Finansiranje);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@godinaRodjenjaS", ListaStudenata[StudentKlasa.RedniProj].Godina);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@drzavaS", ListaStudenata[StudentKlasa.RedniProj].Drzava);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@gradS", ListaStudenata[StudentKlasa.RedniProj].Grad);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@brFonaS", ListaStudenata[StudentKlasa.RedniProj].Telefon);
                                    komandaPromeneUpdate.Parameters.AddWithValue("@nivoPoverenjaS", ListaStudenata[StudentKlasa.RedniProj].Poverenje);
                                    komandaPromeneUpdate.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        //kraj funkcije
        private void btnPostaviSliku_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dijalog = new OpenFileDialog())
            {
                dijalog.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png|ALL Files(*.*)|*.*";
                if (dijalog.ShowDialog() == DialogResult.OK)
                {
                    string putanja = dijalog.FileName.ToString();
                    Image img = Image.FromFile(putanja);
                    imgSlikaStudenta.Image = img;
                    imgSlikaStudenta.SizeMode = PictureBoxSizeMode.StretchImage;
                    putanjaSlike = putanja;
                }
            }
        }
    }
}
