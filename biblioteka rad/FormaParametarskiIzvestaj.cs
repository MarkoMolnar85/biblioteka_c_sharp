﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmParametarskiIzvestaj : Form
    {
        string izbor;
        string parametar;
        public frmParametarskiIzvestaj(string izbor, string parametar)
        {
            InitializeComponent();
            this.izbor = izbor;
            this.parametar = parametar;
        }

        private void loadPokretanjeForme(object sender, EventArgs e)
        {
            try
            {
                ParametarskiIzvestaj pi = new ParametarskiIzvestaj(gridParametarskiUpit, this, this.izbor, this.parametar);
                pi.IzvrsenjeUpita();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
