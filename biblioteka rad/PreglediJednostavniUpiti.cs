﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmPreglediJednostavniUpiti : Form
    {
        string izbor;
        public frmPreglediJednostavniUpiti(string izbor)
        {
            InitializeComponent();
            this.izbor = izbor;
        }

        private void LoadPokretanjeFormeJednostavniUpiti(object sender, EventArgs e)
        {
            try
            {
                JednostavniUpitKlasa ju = new JednostavniUpitKlasa(izbor, gridJednostavniUpiti, this);
                ju.IzbaciPregled();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); 
            }
        }
    }

}
