﻿namespace biblioteka_rad
{
    partial class frmPitanjeZaParametarskiUpit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPoruka = new System.Windows.Forms.Label();
            this.txtParametar = new System.Windows.Forms.TextBox();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.chcOstaviOtvoreno = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblPoruka
            // 
            this.lblPoruka.AutoSize = true;
            this.lblPoruka.Font = new System.Drawing.Font("Perpetua Titling MT", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoruka.Location = new System.Drawing.Point(9, 13);
            this.lblPoruka.Name = "lblPoruka";
            this.lblPoruka.Size = new System.Drawing.Size(282, 13);
            this.lblPoruka.TabIndex = 0;
            this.lblPoruka.Text = "Uneti bilo koju informaciju o studentu";
            // 
            // txtParametar
            // 
            this.txtParametar.Location = new System.Drawing.Point(12, 29);
            this.txtParametar.Name = "txtParametar";
            this.txtParametar.Size = new System.Drawing.Size(342, 20);
            this.txtParametar.TabIndex = 0;
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.Location = new System.Drawing.Point(190, 55);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(164, 37);
            this.btnPrikazi.TabIndex = 2;
            this.btnPrikazi.Text = "Prikazi";
            this.btnPrikazi.UseVisualStyleBackColor = true;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // chcOstaviOtvoreno
            // 
            this.chcOstaviOtvoreno.AutoSize = true;
            this.chcOstaviOtvoreno.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chcOstaviOtvoreno.Location = new System.Drawing.Point(12, 55);
            this.chcOstaviOtvoreno.Name = "chcOstaviOtvoreno";
            this.chcOstaviOtvoreno.Size = new System.Drawing.Size(110, 18);
            this.chcOstaviOtvoreno.TabIndex = 1;
            this.chcOstaviOtvoreno.Text = "Ostavi otvoreno";
            this.chcOstaviOtvoreno.UseVisualStyleBackColor = true;
            // 
            // frmPitanjeZaParametarskiUpit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(385, 104);
            this.Controls.Add(this.chcOstaviOtvoreno);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.txtParametar);
            this.Controls.Add(this.lblPoruka);
            this.Name = "frmPitanjeZaParametarskiUpit";
            this.Text = "Pretraga";
            this.Load += new System.EventHandler(this.loadPokretanjeFormePitalica);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPoruka;
        private System.Windows.Forms.TextBox txtParametar;
        private System.Windows.Forms.Button btnPrikazi;
        private System.Windows.Forms.CheckBox chcOstaviOtvoreno;
    }
}