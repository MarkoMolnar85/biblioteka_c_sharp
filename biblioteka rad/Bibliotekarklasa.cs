﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace biblioteka_rad
{
    class Bibliotekarklasa
    {
        string sifraRadnika;
        string ime;
        string prezime;
        string brojTelefona;
        bool praznoPolje;
        public Bibliotekarklasa(TextBox sifra, TextBox im, TextBox pre, TextBox brojF)
        {
            if (String.IsNullOrEmpty(sifra.Text) || String.IsNullOrEmpty(im.Text) ||
                String.IsNullOrEmpty(pre.Text) || String.IsNullOrEmpty(brojF.Text))
            {
                praznoPolje = true;
                MessageBox.Show("Potrebno je popuniti sva polja");
            }
            else
            {
                praznoPolje = false;
                sifraRadnika = sifra.Text; ime = im.Text; prezime = pre.Text; brojTelefona = brojF.Text;
            }
        }
        public bool PoljeJePrazno()
        {
            return praznoPolje;
        }
        public void KonekcijaUnosNovogBibliotekara()
        {
            using(MySqlConnection konekcija = new MySqlConnection("server=localhost;uid=root;password=;database=biblioteka;"))
            {
                konekcija.Open();
                UnosPodataka(konekcija);
            }
        }
        private void UnosPodataka(MySqlConnection konekcija)
        {
            string unosBibliotekara = "insert into bibliotekar (sifraRadnika, imeRadnika, prezimeRadnika, brojTelefonaRadnika) " +
                "values (@sifraR, @ime, @prezime, @brojFona);";
            using (MySqlCommand komanda = new MySqlCommand(unosBibliotekara, konekcija))
            {
                komanda.Parameters.AddWithValue("@sifraR", this.sifraRadnika);
                komanda.Parameters.AddWithValue("@ime", this.ime);
                komanda.Parameters.AddWithValue("@prezime", this.prezime);
                komanda.Parameters.AddWithValue("@brojFona", this.brojTelefona);

                komanda.ExecuteNonQuery();
            }
        }

    }
}
