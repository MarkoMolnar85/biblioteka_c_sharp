﻿namespace biblioteka_rad
{
    partial class frmStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabKontrolaStudent = new System.Windows.Forms.TabControl();
            this.tabStranaStudent = new System.Windows.Forms.TabPage();
            this.btnUnos = new System.Windows.Forms.Button();
            this.txtBrojIndeksa = new System.Windows.Forms.TextBox();
            this.cmbPouzdanost = new System.Windows.Forms.ComboBox();
            this.cmbFinansiranje = new System.Windows.Forms.ComboBox();
            this.cmbBracnStatus = new System.Windows.Forms.ComboBox();
            this.cmbPol = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDatumRodjenja = new System.Windows.Forms.TextBox();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.txtGrad = new System.Windows.Forms.TextBox();
            this.txtJmbg = new System.Windows.Forms.TextBox();
            this.txtDrzava = new System.Windows.Forms.TextBox();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cmbBrojIndeksaPre = new System.Windows.Forms.ComboBox();
            this.btnIzmena = new System.Windows.Forms.Button();
            this.cmbPouzdanostPre = new System.Windows.Forms.ComboBox();
            this.cmbFinansiranjePre = new System.Windows.Forms.ComboBox();
            this.cmbBracniStatusPre = new System.Windows.Forms.ComboBox();
            this.cmbPolPre = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtGodinaRodjenjaPre = new System.Windows.Forms.TextBox();
            this.txtTelefonPre = new System.Windows.Forms.TextBox();
            this.txtPrezimePre = new System.Windows.Forms.TextBox();
            this.txtGradPre = new System.Windows.Forms.TextBox();
            this.txtJmbgPre = new System.Windows.Forms.TextBox();
            this.txtDrzavaPre = new System.Windows.Forms.TextBox();
            this.txtImePre = new System.Windows.Forms.TextBox();
            this.btnPostaviSliku = new System.Windows.Forms.Button();
            this.imgSlikaStudenta = new System.Windows.Forms.PictureBox();
            this.btnDesnoZapis = new System.Windows.Forms.Button();
            this.btnLevoZapis = new System.Windows.Forms.Button();
            this.imgSlikaPregled = new System.Windows.Forms.PictureBox();
            this.tabKontrolaStudent.SuspendLayout();
            this.tabStranaStudent.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaStudenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaPregled)).BeginInit();
            this.SuspendLayout();
            // 
            // tabKontrolaStudent
            // 
            this.tabKontrolaStudent.Controls.Add(this.tabStranaStudent);
            this.tabKontrolaStudent.Controls.Add(this.tabPage2);
            this.tabKontrolaStudent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabKontrolaStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabKontrolaStudent.Location = new System.Drawing.Point(0, 0);
            this.tabKontrolaStudent.Name = "tabKontrolaStudent";
            this.tabKontrolaStudent.SelectedIndex = 0;
            this.tabKontrolaStudent.Size = new System.Drawing.Size(1237, 570);
            this.tabKontrolaStudent.TabIndex = 0;
            // 
            // tabStranaStudent
            // 
            this.tabStranaStudent.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabStranaStudent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tabStranaStudent.Controls.Add(this.btnPostaviSliku);
            this.tabStranaStudent.Controls.Add(this.imgSlikaStudenta);
            this.tabStranaStudent.Controls.Add(this.btnUnos);
            this.tabStranaStudent.Controls.Add(this.txtBrojIndeksa);
            this.tabStranaStudent.Controls.Add(this.cmbPouzdanost);
            this.tabStranaStudent.Controls.Add(this.cmbFinansiranje);
            this.tabStranaStudent.Controls.Add(this.cmbBracnStatus);
            this.tabStranaStudent.Controls.Add(this.cmbPol);
            this.tabStranaStudent.Controls.Add(this.label8);
            this.tabStranaStudent.Controls.Add(this.label13);
            this.tabStranaStudent.Controls.Add(this.label4);
            this.tabStranaStudent.Controls.Add(this.label7);
            this.tabStranaStudent.Controls.Add(this.label6);
            this.tabStranaStudent.Controls.Add(this.label11);
            this.tabStranaStudent.Controls.Add(this.label2);
            this.tabStranaStudent.Controls.Add(this.label5);
            this.tabStranaStudent.Controls.Add(this.label10);
            this.tabStranaStudent.Controls.Add(this.label3);
            this.tabStranaStudent.Controls.Add(this.label9);
            this.tabStranaStudent.Controls.Add(this.label1);
            this.tabStranaStudent.Controls.Add(this.txtDatumRodjenja);
            this.tabStranaStudent.Controls.Add(this.txtTelefon);
            this.tabStranaStudent.Controls.Add(this.txtPrezime);
            this.tabStranaStudent.Controls.Add(this.txtGrad);
            this.tabStranaStudent.Controls.Add(this.txtJmbg);
            this.tabStranaStudent.Controls.Add(this.txtDrzava);
            this.tabStranaStudent.Controls.Add(this.txtIme);
            this.tabStranaStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabStranaStudent.Location = new System.Drawing.Point(4, 29);
            this.tabStranaStudent.Name = "tabStranaStudent";
            this.tabStranaStudent.Padding = new System.Windows.Forms.Padding(3);
            this.tabStranaStudent.Size = new System.Drawing.Size(1229, 537);
            this.tabStranaStudent.TabIndex = 0;
            this.tabStranaStudent.Text = "Unos studenta";
            // 
            // btnUnos
            // 
            this.btnUnos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUnos.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnos.Location = new System.Drawing.Point(554, 239);
            this.btnUnos.Name = "btnUnos";
            this.btnUnos.Size = new System.Drawing.Size(309, 36);
            this.btnUnos.TabIndex = 13;
            this.btnUnos.Text = "Unos";
            this.btnUnos.UseVisualStyleBackColor = false;
            this.btnUnos.Click += new System.EventHandler(this.UnosNovogStudenta);
            // 
            // txtBrojIndeksa
            // 
            this.txtBrojIndeksa.Location = new System.Drawing.Point(179, 16);
            this.txtBrojIndeksa.Name = "txtBrojIndeksa";
            this.txtBrojIndeksa.Size = new System.Drawing.Size(193, 26);
            this.txtBrojIndeksa.TabIndex = 0;
            // 
            // cmbPouzdanost
            // 
            this.cmbPouzdanost.FormattingEnabled = true;
            this.cmbPouzdanost.Location = new System.Drawing.Point(670, 138);
            this.cmbPouzdanost.Name = "cmbPouzdanost";
            this.cmbPouzdanost.Size = new System.Drawing.Size(193, 28);
            this.cmbPouzdanost.TabIndex = 11;
            // 
            // cmbFinansiranje
            // 
            this.cmbFinansiranje.FormattingEnabled = true;
            this.cmbFinansiranje.Location = new System.Drawing.Point(179, 215);
            this.cmbFinansiranje.Name = "cmbFinansiranje";
            this.cmbFinansiranje.Size = new System.Drawing.Size(193, 28);
            this.cmbFinansiranje.TabIndex = 6;
            // 
            // cmbBracnStatus
            // 
            this.cmbBracnStatus.FormattingEnabled = true;
            this.cmbBracnStatus.Location = new System.Drawing.Point(179, 181);
            this.cmbBracnStatus.Name = "cmbBracnStatus";
            this.cmbBracnStatus.Size = new System.Drawing.Size(193, 28);
            this.cmbBracnStatus.TabIndex = 5;
            // 
            // cmbPol
            // 
            this.cmbPol.FormattingEnabled = true;
            this.cmbPol.Location = new System.Drawing.Point(179, 115);
            this.cmbPol.Name = "cmbPol";
            this.cmbPol.Size = new System.Drawing.Size(193, 28);
            this.cmbPol.TabIndex = 3;
            this.cmbPol.SelectedIndexChanged += new System.EventHandler(this.BiranjePola);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 21);
            this.label8.TabIndex = 1;
            this.label8.Text = "Bracni status";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(550, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 21);
            this.label13.TabIndex = 1;
            this.label13.Text = "Grad";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ime";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 21);
            this.label7.TabIndex = 1;
            this.label7.Text = "Godina rodjenja";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 21);
            this.label6.TabIndex = 1;
            this.label6.Text = "JMBG";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(550, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 21);
            this.label11.TabIndex = 1;
            this.label11.Text = "Pouzdanost";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pol";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Finansiranje";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(550, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 21);
            this.label10.TabIndex = 1;
            this.label10.Text = "Drzava";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 21);
            this.label3.TabIndex = 1;
            this.label3.Text = "Broj indeksa";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(550, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Telefon";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Prezime";
            // 
            // txtDatumRodjenja
            // 
            this.txtDatumRodjenja.Location = new System.Drawing.Point(179, 249);
            this.txtDatumRodjenja.Name = "txtDatumRodjenja";
            this.txtDatumRodjenja.Size = new System.Drawing.Size(193, 26);
            this.txtDatumRodjenja.TabIndex = 7;
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(670, 99);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(193, 26);
            this.txtTelefon.TabIndex = 10;
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(179, 83);
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(193, 26);
            this.txtPrezime.TabIndex = 2;
            // 
            // txtGrad
            // 
            this.txtGrad.Location = new System.Drawing.Point(670, 58);
            this.txtGrad.Name = "txtGrad";
            this.txtGrad.Size = new System.Drawing.Size(193, 26);
            this.txtGrad.TabIndex = 9;
            // 
            // txtJmbg
            // 
            this.txtJmbg.Location = new System.Drawing.Point(179, 149);
            this.txtJmbg.Name = "txtJmbg";
            this.txtJmbg.Size = new System.Drawing.Size(193, 26);
            this.txtJmbg.TabIndex = 4;
            // 
            // txtDrzava
            // 
            this.txtDrzava.Location = new System.Drawing.Point(670, 16);
            this.txtDrzava.Name = "txtDrzava";
            this.txtDrzava.Size = new System.Drawing.Size(193, 26);
            this.txtDrzava.TabIndex = 8;
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(179, 51);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(193, 26);
            this.txtIme.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage2.Controls.Add(this.imgSlikaPregled);
            this.tabPage2.Controls.Add(this.cmbBrojIndeksaPre);
            this.tabPage2.Controls.Add(this.btnIzmena);
            this.tabPage2.Controls.Add(this.cmbPouzdanostPre);
            this.tabPage2.Controls.Add(this.cmbFinansiranjePre);
            this.tabPage2.Controls.Add(this.cmbBracniStatusPre);
            this.tabPage2.Controls.Add(this.cmbPolPre);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.txtGodinaRodjenjaPre);
            this.tabPage2.Controls.Add(this.txtTelefonPre);
            this.tabPage2.Controls.Add(this.txtPrezimePre);
            this.tabPage2.Controls.Add(this.txtGradPre);
            this.tabPage2.Controls.Add(this.txtJmbgPre);
            this.tabPage2.Controls.Add(this.txtDrzavaPre);
            this.tabPage2.Controls.Add(this.txtImePre);
            this.tabPage2.Controls.Add(this.btnDesnoZapis);
            this.tabPage2.Controls.Add(this.btnLevoZapis);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1229, 537);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pregled studenata";
            // 
            // cmbBrojIndeksaPre
            // 
            this.cmbBrojIndeksaPre.FormattingEnabled = true;
            this.cmbBrojIndeksaPre.Location = new System.Drawing.Point(165, 14);
            this.cmbBrojIndeksaPre.Name = "cmbBrojIndeksaPre";
            this.cmbBrojIndeksaPre.Size = new System.Drawing.Size(195, 28);
            this.cmbBrojIndeksaPre.TabIndex = 0;
            this.cmbBrojIndeksaPre.SelectedIndexChanged += new System.EventHandler(this.PromeKomboBoxa);
            this.cmbBrojIndeksaPre.TextUpdate += new System.EventHandler(this.PromenaTextaSkracenjeListe);
            // 
            // btnIzmena
            // 
            this.btnIzmena.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnIzmena.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmena.Location = new System.Drawing.Point(558, 232);
            this.btnIzmena.Name = "btnIzmena";
            this.btnIzmena.Size = new System.Drawing.Size(301, 36);
            this.btnIzmena.TabIndex = 12;
            this.btnIzmena.Text = "Izmeni";
            this.btnIzmena.UseVisualStyleBackColor = false;
            this.btnIzmena.Click += new System.EventHandler(this.IzmeniPodatkeStudenta);
            // 
            // cmbPouzdanostPre
            // 
            this.cmbPouzdanostPre.FormattingEnabled = true;
            this.cmbPouzdanostPre.Location = new System.Drawing.Point(664, 140);
            this.cmbPouzdanostPre.Name = "cmbPouzdanostPre";
            this.cmbPouzdanostPre.Size = new System.Drawing.Size(193, 28);
            this.cmbPouzdanostPre.TabIndex = 11;
            // 
            // cmbFinansiranjePre
            // 
            this.cmbFinansiranjePre.FormattingEnabled = true;
            this.cmbFinansiranjePre.Location = new System.Drawing.Point(165, 212);
            this.cmbFinansiranjePre.Name = "cmbFinansiranjePre";
            this.cmbFinansiranjePre.Size = new System.Drawing.Size(195, 28);
            this.cmbFinansiranjePre.TabIndex = 6;
            // 
            // cmbBracniStatusPre
            // 
            this.cmbBracniStatusPre.FormattingEnabled = true;
            this.cmbBracniStatusPre.Location = new System.Drawing.Point(165, 178);
            this.cmbBracniStatusPre.Name = "cmbBracniStatusPre";
            this.cmbBracniStatusPre.Size = new System.Drawing.Size(195, 28);
            this.cmbBracniStatusPre.TabIndex = 5;
            // 
            // cmbPolPre
            // 
            this.cmbPolPre.FormattingEnabled = true;
            this.cmbPolPre.Location = new System.Drawing.Point(165, 112);
            this.cmbPolPre.Name = "cmbPolPre";
            this.cmbPolPre.Size = new System.Drawing.Size(195, 28);
            this.cmbPolPre.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(28, 183);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 21);
            this.label12.TabIndex = 25;
            this.label12.Text = "Bracni status";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(554, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 21);
            this.label14.TabIndex = 24;
            this.label14.Text = "Grad";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(28, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 21);
            this.label15.TabIndex = 23;
            this.label15.Text = "Ime";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(27, 248);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 21);
            this.label16.TabIndex = 26;
            this.label16.Text = "Godina rodjenja";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(28, 148);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 21);
            this.label17.TabIndex = 21;
            this.label17.Text = "JMBG";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(554, 140);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 21);
            this.label18.TabIndex = 20;
            this.label18.Text = "Pouzdanost";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(28, 117);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 21);
            this.label19.TabIndex = 14;
            this.label19.Text = "Pol";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(28, 214);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(103, 21);
            this.label20.TabIndex = 19;
            this.label20.Text = "Finansiranje";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(554, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 21);
            this.label21.TabIndex = 18;
            this.label21.Text = "Drzava";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(27, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(104, 21);
            this.label22.TabIndex = 17;
            this.label22.Text = "Broj indeksa";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(554, 101);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(66, 21);
            this.label23.TabIndex = 16;
            this.label23.Text = "Telefon";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Nirmala UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(28, 85);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(72, 21);
            this.label24.TabIndex = 15;
            this.label24.Text = "Prezime";
            // 
            // txtGodinaRodjenjaPre
            // 
            this.txtGodinaRodjenjaPre.Location = new System.Drawing.Point(165, 246);
            this.txtGodinaRodjenjaPre.Name = "txtGodinaRodjenjaPre";
            this.txtGodinaRodjenjaPre.Size = new System.Drawing.Size(195, 26);
            this.txtGodinaRodjenjaPre.TabIndex = 7;
            // 
            // txtTelefonPre
            // 
            this.txtTelefonPre.Location = new System.Drawing.Point(664, 101);
            this.txtTelefonPre.Name = "txtTelefonPre";
            this.txtTelefonPre.Size = new System.Drawing.Size(193, 26);
            this.txtTelefonPre.TabIndex = 10;
            // 
            // txtPrezimePre
            // 
            this.txtPrezimePre.Location = new System.Drawing.Point(165, 80);
            this.txtPrezimePre.Name = "txtPrezimePre";
            this.txtPrezimePre.Size = new System.Drawing.Size(195, 26);
            this.txtPrezimePre.TabIndex = 2;
            // 
            // txtGradPre
            // 
            this.txtGradPre.Location = new System.Drawing.Point(664, 60);
            this.txtGradPre.Name = "txtGradPre";
            this.txtGradPre.Size = new System.Drawing.Size(193, 26);
            this.txtGradPre.TabIndex = 9;
            // 
            // txtJmbgPre
            // 
            this.txtJmbgPre.Location = new System.Drawing.Point(165, 146);
            this.txtJmbgPre.Name = "txtJmbgPre";
            this.txtJmbgPre.Size = new System.Drawing.Size(195, 26);
            this.txtJmbgPre.TabIndex = 4;
            // 
            // txtDrzavaPre
            // 
            this.txtDrzavaPre.Location = new System.Drawing.Point(664, 18);
            this.txtDrzavaPre.Name = "txtDrzavaPre";
            this.txtDrzavaPre.Size = new System.Drawing.Size(193, 26);
            this.txtDrzavaPre.TabIndex = 8;
            // 
            // txtImePre
            // 
            this.txtImePre.Location = new System.Drawing.Point(165, 48);
            this.txtImePre.Name = "txtImePre";
            this.txtImePre.Size = new System.Drawing.Size(195, 26);
            this.txtImePre.TabIndex = 1;
            this.txtImePre.Tag = "1";
            // 
            // btnPostaviSliku
            // 
            this.btnPostaviSliku.Location = new System.Drawing.Point(978, 242);
            this.btnPostaviSliku.Name = "btnPostaviSliku";
            this.btnPostaviSliku.Size = new System.Drawing.Size(207, 36);
            this.btnPostaviSliku.TabIndex = 12;
            this.btnPostaviSliku.Text = "Postavi sliku";
            this.btnPostaviSliku.UseVisualStyleBackColor = true;
            this.btnPostaviSliku.Click += new System.EventHandler(this.btnPostaviSliku_Click);
            // 
            // imgSlikaStudenta
            // 
            this.imgSlikaStudenta.BackgroundImage = global::biblioteka_rad.Properties.Resources.nema_slike;
            this.imgSlikaStudenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgSlikaStudenta.Location = new System.Drawing.Point(978, 16);
            this.imgSlikaStudenta.Name = "imgSlikaStudenta";
            this.imgSlikaStudenta.Size = new System.Drawing.Size(207, 215);
            this.imgSlikaStudenta.TabIndex = 13;
            this.imgSlikaStudenta.TabStop = false;
            // 
            // btnDesnoZapis
            // 
            this.btnDesnoZapis.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnDesnoZapis.BackgroundImage = global::biblioteka_rad.Properties.Resources.desno;
            this.btnDesnoZapis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDesnoZapis.Location = new System.Drawing.Point(202, 292);
            this.btnDesnoZapis.Name = "btnDesnoZapis";
            this.btnDesnoZapis.Size = new System.Drawing.Size(102, 45);
            this.btnDesnoZapis.TabIndex = 14;
            this.btnDesnoZapis.UseVisualStyleBackColor = false;
            this.btnDesnoZapis.Click += new System.EventHandler(this.RekordDesno);
            // 
            // btnLevoZapis
            // 
            this.btnLevoZapis.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLevoZapis.BackgroundImage = global::biblioteka_rad.Properties.Resources.levo;
            this.btnLevoZapis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLevoZapis.Location = new System.Drawing.Point(94, 292);
            this.btnLevoZapis.Name = "btnLevoZapis";
            this.btnLevoZapis.Size = new System.Drawing.Size(102, 45);
            this.btnLevoZapis.TabIndex = 13;
            this.btnLevoZapis.UseVisualStyleBackColor = false;
            this.btnLevoZapis.Click += new System.EventHandler(this.RekordLevo);
            // 
            // imgSlikaPregled
            // 
            this.imgSlikaPregled.BackgroundImage = global::biblioteka_rad.Properties.Resources.nema_slike;
            this.imgSlikaPregled.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgSlikaPregled.Location = new System.Drawing.Point(929, 14);
            this.imgSlikaPregled.Name = "imgSlikaPregled";
            this.imgSlikaPregled.Size = new System.Drawing.Size(223, 221);
            this.imgSlikaPregled.TabIndex = 27;
            this.imgSlikaPregled.TabStop = false;
            // 
            // frmStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(1237, 570);
            this.Controls.Add(this.tabKontrolaStudent);
            this.Name = "frmStudent";
            this.ShowIcon = false;
            this.Text = "Student";
            this.Load += new System.EventHandler(this.OtvaranjeFormeEvent);
            this.tabKontrolaStudent.ResumeLayout(false);
            this.tabStranaStudent.ResumeLayout(false);
            this.tabStranaStudent.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaStudenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaPregled)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabKontrolaStudent;
        private System.Windows.Forms.TabPage tabStranaStudent;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDatumRodjenja;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.TextBox txtGrad;
        private System.Windows.Forms.TextBox txtJmbg;
        private System.Windows.Forms.TextBox txtDrzava;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.ComboBox cmbPouzdanost;
        private System.Windows.Forms.ComboBox cmbFinansiranje;
        private System.Windows.Forms.ComboBox cmbBracnStatus;
        private System.Windows.Forms.ComboBox cmbPol;
        private System.Windows.Forms.TextBox txtBrojIndeksa;
        private System.Windows.Forms.Button btnUnos;
        private System.Windows.Forms.Button btnIzmena;
        private System.Windows.Forms.ComboBox cmbPouzdanostPre;
        private System.Windows.Forms.ComboBox cmbFinansiranjePre;
        private System.Windows.Forms.ComboBox cmbBracniStatusPre;
        private System.Windows.Forms.ComboBox cmbPolPre;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtGodinaRodjenjaPre;
        private System.Windows.Forms.TextBox txtTelefonPre;
        private System.Windows.Forms.TextBox txtPrezimePre;
        private System.Windows.Forms.TextBox txtGradPre;
        private System.Windows.Forms.TextBox txtJmbgPre;
        private System.Windows.Forms.TextBox txtDrzavaPre;
        private System.Windows.Forms.TextBox txtImePre;
        private System.Windows.Forms.Button btnLevoZapis;
        private System.Windows.Forms.Button btnDesnoZapis;
        private System.Windows.Forms.ComboBox cmbBrojIndeksaPre;
        private System.Windows.Forms.Button btnPostaviSliku;
        private System.Windows.Forms.PictureBox imgSlikaStudenta;
        private System.Windows.Forms.PictureBox imgSlikaPregled;
    }
}

