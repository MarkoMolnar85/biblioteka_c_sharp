﻿namespace biblioteka_rad
{
    partial class frmIzdatoVracenoForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.KontrolaTabIzdatoVrateci = new System.Windows.Forms.TabControl();
            this.tabStanaIzdato = new System.Windows.Forms.TabPage();
            this.imgSlikaStudentaIzdajem = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPoverenjeIzdajem = new System.Windows.Forms.TextBox();
            this.txtBrojTelefonaIdajem = new System.Windows.Forms.TextBox();
            this.txtGradIzdajem = new System.Windows.Forms.TextBox();
            this.txtDrzavaIzdajem = new System.Windows.Forms.TextBox();
            this.txtGodinaRodjenjaIzdajem = new System.Windows.Forms.TextBox();
            this.txtnacinFinansiranjaIzdajem = new System.Windows.Forms.TextBox();
            this.txtBracniStatusIzdajem = new System.Windows.Forms.TextBox();
            this.txtJmbgizdajem = new System.Windows.Forms.TextBox();
            this.txtpolIzdajem = new System.Windows.Forms.TextBox();
            this.txtPrezimeIzdajem = new System.Windows.Forms.TextBox();
            this.txtImeIzdajem = new System.Windows.Forms.TextBox();
            this.btnNoviUnosIzdajem = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbBibliotekarIzdajem = new System.Windows.Forms.ComboBox();
            this.btnIzdajem = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPredvidjenoVracanjeIzdajem = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtKolikoDanaIzdajem = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDatumIzdavanjaIzdajem = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbBrojIndeksaIzdajem = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbPrimerakIzdajem = new System.Windows.Forms.ComboBox();
            this.cmbKnjigaIzdajem = new System.Windows.Forms.ComboBox();
            this.tabStranaVraceno = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbOcenaVracam = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbOstecenaVracam = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbPoverenjeVracam = new System.Windows.Forms.ComboBox();
            this.txtDatumaVracenoVracam = new System.Windows.Forms.TextBox();
            this.cmbPrimerakVracam = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbBrojIndeksaVracam = new System.Windows.Forms.ComboBox();
            this.imgSlikaVraceno = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtPoverenjeVraceno = new System.Windows.Forms.TextBox();
            this.txtBrojTelefonaVraceno = new System.Windows.Forms.TextBox();
            this.txtGradVraceno = new System.Windows.Forms.TextBox();
            this.txtDrzavaVraceno = new System.Windows.Forms.TextBox();
            this.txtGodinaVraceno = new System.Windows.Forms.TextBox();
            this.txtnacinFinansiranjaVraceno = new System.Windows.Forms.TextBox();
            this.txtBracniStatusVraceno = new System.Windows.Forms.TextBox();
            this.txtJmbgVraceno = new System.Windows.Forms.TextBox();
            this.txtPolVraceon = new System.Windows.Forms.TextBox();
            this.txtPrezimeVraceno = new System.Windows.Forms.TextBox();
            this.txtImeVraceno = new System.Windows.Forms.TextBox();
            this.KontrolaTabIzdatoVrateci.SuspendLayout();
            this.tabStanaIzdato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaStudentaIzdajem)).BeginInit();
            this.tabStranaVraceno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaVraceno)).BeginInit();
            this.SuspendLayout();
            // 
            // KontrolaTabIzdatoVrateci
            // 
            this.KontrolaTabIzdatoVrateci.Controls.Add(this.tabStanaIzdato);
            this.KontrolaTabIzdatoVrateci.Controls.Add(this.tabStranaVraceno);
            this.KontrolaTabIzdatoVrateci.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KontrolaTabIzdatoVrateci.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KontrolaTabIzdatoVrateci.Location = new System.Drawing.Point(0, 0);
            this.KontrolaTabIzdatoVrateci.Name = "KontrolaTabIzdatoVrateci";
            this.KontrolaTabIzdatoVrateci.SelectedIndex = 0;
            this.KontrolaTabIzdatoVrateci.Size = new System.Drawing.Size(1462, 474);
            this.KontrolaTabIzdatoVrateci.TabIndex = 0;
            this.KontrolaTabIzdatoVrateci.Click += new System.EventHandler(this.KliknutTabIzdajemVracam);
            // 
            // tabStanaIzdato
            // 
            this.tabStanaIzdato.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabStanaIzdato.Controls.Add(this.imgSlikaStudentaIzdajem);
            this.tabStanaIzdato.Controls.Add(this.label24);
            this.tabStanaIzdato.Controls.Add(this.label23);
            this.tabStanaIzdato.Controls.Add(this.label22);
            this.tabStanaIzdato.Controls.Add(this.label21);
            this.tabStanaIzdato.Controls.Add(this.label20);
            this.tabStanaIzdato.Controls.Add(this.label19);
            this.tabStanaIzdato.Controls.Add(this.label18);
            this.tabStanaIzdato.Controls.Add(this.label17);
            this.tabStanaIzdato.Controls.Add(this.label16);
            this.tabStanaIzdato.Controls.Add(this.label15);
            this.tabStanaIzdato.Controls.Add(this.label13);
            this.tabStanaIzdato.Controls.Add(this.txtPoverenjeIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtBrojTelefonaIdajem);
            this.tabStanaIzdato.Controls.Add(this.txtGradIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtDrzavaIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtGodinaRodjenjaIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtnacinFinansiranjaIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtBracniStatusIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtJmbgizdajem);
            this.tabStanaIzdato.Controls.Add(this.txtpolIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtPrezimeIzdajem);
            this.tabStanaIzdato.Controls.Add(this.txtImeIzdajem);
            this.tabStanaIzdato.Controls.Add(this.btnNoviUnosIzdajem);
            this.tabStanaIzdato.Controls.Add(this.label12);
            this.tabStanaIzdato.Controls.Add(this.cmbBibliotekarIzdajem);
            this.tabStanaIzdato.Controls.Add(this.btnIzdajem);
            this.tabStanaIzdato.Controls.Add(this.label6);
            this.tabStanaIzdato.Controls.Add(this.txtPredvidjenoVracanjeIzdajem);
            this.tabStanaIzdato.Controls.Add(this.label5);
            this.tabStanaIzdato.Controls.Add(this.txtKolikoDanaIzdajem);
            this.tabStanaIzdato.Controls.Add(this.label4);
            this.tabStanaIzdato.Controls.Add(this.txtDatumIzdavanjaIzdajem);
            this.tabStanaIzdato.Controls.Add(this.label3);
            this.tabStanaIzdato.Controls.Add(this.cmbBrojIndeksaIzdajem);
            this.tabStanaIzdato.Controls.Add(this.label2);
            this.tabStanaIzdato.Controls.Add(this.label1);
            this.tabStanaIzdato.Controls.Add(this.cmbPrimerakIzdajem);
            this.tabStanaIzdato.Controls.Add(this.cmbKnjigaIzdajem);
            this.tabStanaIzdato.Location = new System.Drawing.Point(4, 29);
            this.tabStanaIzdato.Name = "tabStanaIzdato";
            this.tabStanaIzdato.Padding = new System.Windows.Forms.Padding(3);
            this.tabStanaIzdato.Size = new System.Drawing.Size(1454, 441);
            this.tabStanaIzdato.TabIndex = 0;
            this.tabStanaIzdato.Text = "Izdajem";
            // 
            // imgSlikaStudentaIzdajem
            // 
            this.imgSlikaStudentaIzdajem.BackgroundImage = global::biblioteka_rad.Properties.Resources.nema_slike;
            this.imgSlikaStudentaIzdajem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgSlikaStudentaIzdajem.Location = new System.Drawing.Point(1226, 133);
            this.imgSlikaStudentaIzdajem.Name = "imgSlikaStudentaIzdajem";
            this.imgSlikaStudentaIzdajem.Size = new System.Drawing.Size(170, 163);
            this.imgSlikaStudentaIzdajem.TabIndex = 17;
            this.imgSlikaStudentaIzdajem.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(1108, 97);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 20);
            this.label24.TabIndex = 16;
            this.label24.Text = "Poverenje";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1108, 63);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(112, 20);
            this.label23.TabIndex = 16;
            this.label23.Text = "Broj telefona";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1108, 29);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 20);
            this.label22.TabIndex = 16;
            this.label22.Text = "Grad";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(728, 273);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 20);
            this.label21.TabIndex = 16;
            this.label21.Text = "Drzava";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(725, 241);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(136, 20);
            this.label20.TabIndex = 16;
            this.label20.Text = "Godina rodjejna";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(725, 202);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(152, 20);
            this.label19.TabIndex = 16;
            this.label19.Text = "Nacin finansiranja";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(725, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 20);
            this.label18.TabIndex = 16;
            this.label18.Text = "Bracni status";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(728, 133);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 20);
            this.label17.TabIndex = 16;
            this.label17.Text = "JMBG";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(728, 97);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 20);
            this.label16.TabIndex = 16;
            this.label16.Text = "Pol";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(728, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 20);
            this.label15.TabIndex = 16;
            this.label15.Text = "Prezime";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(728, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 20);
            this.label13.TabIndex = 16;
            this.label13.Text = "Ime";
            // 
            // txtPoverenjeIzdajem
            // 
            this.txtPoverenjeIzdajem.Location = new System.Drawing.Point(1226, 91);
            this.txtPoverenjeIzdajem.Name = "txtPoverenjeIzdajem";
            this.txtPoverenjeIzdajem.Size = new System.Drawing.Size(170, 26);
            this.txtPoverenjeIzdajem.TabIndex = 15;
            // 
            // txtBrojTelefonaIdajem
            // 
            this.txtBrojTelefonaIdajem.Location = new System.Drawing.Point(1226, 58);
            this.txtBrojTelefonaIdajem.Name = "txtBrojTelefonaIdajem";
            this.txtBrojTelefonaIdajem.Size = new System.Drawing.Size(170, 26);
            this.txtBrojTelefonaIdajem.TabIndex = 15;
            // 
            // txtGradIzdajem
            // 
            this.txtGradIzdajem.Location = new System.Drawing.Point(1226, 23);
            this.txtGradIzdajem.Name = "txtGradIzdajem";
            this.txtGradIzdajem.Size = new System.Drawing.Size(170, 26);
            this.txtGradIzdajem.TabIndex = 15;
            // 
            // txtDrzavaIzdajem
            // 
            this.txtDrzavaIzdajem.Location = new System.Drawing.Point(884, 270);
            this.txtDrzavaIzdajem.Name = "txtDrzavaIzdajem";
            this.txtDrzavaIzdajem.Size = new System.Drawing.Size(210, 26);
            this.txtDrzavaIzdajem.TabIndex = 15;
            // 
            // txtGodinaRodjenjaIzdajem
            // 
            this.txtGodinaRodjenjaIzdajem.Location = new System.Drawing.Point(884, 238);
            this.txtGodinaRodjenjaIzdajem.Name = "txtGodinaRodjenjaIzdajem";
            this.txtGodinaRodjenjaIzdajem.Size = new System.Drawing.Size(210, 26);
            this.txtGodinaRodjenjaIzdajem.TabIndex = 15;
            // 
            // txtnacinFinansiranjaIzdajem
            // 
            this.txtnacinFinansiranjaIzdajem.Location = new System.Drawing.Point(884, 202);
            this.txtnacinFinansiranjaIzdajem.Name = "txtnacinFinansiranjaIzdajem";
            this.txtnacinFinansiranjaIzdajem.Size = new System.Drawing.Size(210, 26);
            this.txtnacinFinansiranjaIzdajem.TabIndex = 15;
            // 
            // txtBracniStatusIzdajem
            // 
            this.txtBracniStatusIzdajem.Location = new System.Drawing.Point(884, 167);
            this.txtBracniStatusIzdajem.Name = "txtBracniStatusIzdajem";
            this.txtBracniStatusIzdajem.Size = new System.Drawing.Size(210, 26);
            this.txtBracniStatusIzdajem.TabIndex = 15;
            // 
            // txtJmbgizdajem
            // 
            this.txtJmbgizdajem.Location = new System.Drawing.Point(884, 133);
            this.txtJmbgizdajem.Name = "txtJmbgizdajem";
            this.txtJmbgizdajem.Size = new System.Drawing.Size(210, 26);
            this.txtJmbgizdajem.TabIndex = 15;
            // 
            // txtpolIzdajem
            // 
            this.txtpolIzdajem.Location = new System.Drawing.Point(884, 99);
            this.txtpolIzdajem.Name = "txtpolIzdajem";
            this.txtpolIzdajem.Size = new System.Drawing.Size(210, 26);
            this.txtpolIzdajem.TabIndex = 15;
            // 
            // txtPrezimeIzdajem
            // 
            this.txtPrezimeIzdajem.Location = new System.Drawing.Point(884, 65);
            this.txtPrezimeIzdajem.Name = "txtPrezimeIzdajem";
            this.txtPrezimeIzdajem.Size = new System.Drawing.Size(210, 26);
            this.txtPrezimeIzdajem.TabIndex = 15;
            // 
            // txtImeIzdajem
            // 
            this.txtImeIzdajem.Location = new System.Drawing.Point(884, 29);
            this.txtImeIzdajem.Name = "txtImeIzdajem";
            this.txtImeIzdajem.Size = new System.Drawing.Size(210, 26);
            this.txtImeIzdajem.TabIndex = 15;
            // 
            // btnNoviUnosIzdajem
            // 
            this.btnNoviUnosIzdajem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnNoviUnosIzdajem.Font = new System.Drawing.Font("Nirmala UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoviUnosIzdajem.Location = new System.Drawing.Point(130, 286);
            this.btnNoviUnosIzdajem.Name = "btnNoviUnosIzdajem";
            this.btnNoviUnosIzdajem.Size = new System.Drawing.Size(142, 47);
            this.btnNoviUnosIzdajem.TabIndex = 7;
            this.btnNoviUnosIzdajem.Text = "Novi unos";
            this.btnNoviUnosIzdajem.UseVisualStyleBackColor = false;
            this.btnNoviUnosIzdajem.Click += new System.EventHandler(this.btnNoviUnosIzdavanjeKnjige);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Castellar", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(67, 38);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 19);
            this.label12.TabIndex = 14;
            this.label12.Text = "Bibliotekar";
            // 
            // cmbBibliotekarIzdajem
            // 
            this.cmbBibliotekarIzdajem.FormattingEnabled = true;
            this.cmbBibliotekarIzdajem.Location = new System.Drawing.Point(320, 29);
            this.cmbBibliotekarIzdajem.Name = "cmbBibliotekarIzdajem";
            this.cmbBibliotekarIzdajem.Size = new System.Drawing.Size(329, 28);
            this.cmbBibliotekarIzdajem.TabIndex = 0;
            // 
            // btnIzdajem
            // 
            this.btnIzdajem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnIzdajem.Font = new System.Drawing.Font("Nirmala UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzdajem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnIzdajem.Location = new System.Drawing.Point(280, 286);
            this.btnIzdajem.Name = "btnIzdajem";
            this.btnIzdajem.Size = new System.Drawing.Size(329, 47);
            this.btnIzdajem.TabIndex = 6;
            this.btnIzdajem.Text = "Izdaj knjigu";
            this.btnIzdajem.UseVisualStyleBackColor = false;
            this.btnIzdajem.Click += new System.EventHandler(this.IzdajKnjiguEVENT);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Castellar", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(67, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(247, 19);
            this.label6.TabIndex = 11;
            this.label6.Text = "Predvidjeno da vrati";
            // 
            // txtPredvidjenoVracanjeIzdajem
            // 
            this.txtPredvidjenoVracanjeIzdajem.Location = new System.Drawing.Point(320, 231);
            this.txtPredvidjenoVracanjeIzdajem.Name = "txtPredvidjenoVracanjeIzdajem";
            this.txtPredvidjenoVracanjeIzdajem.Size = new System.Drawing.Size(329, 26);
            this.txtPredvidjenoVracanjeIzdajem.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Castellar", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(68, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "Na koliko dana";
            // 
            // txtKolikoDanaIzdajem
            // 
            this.txtKolikoDanaIzdajem.Location = new System.Drawing.Point(320, 199);
            this.txtKolikoDanaIzdajem.Name = "txtKolikoDanaIzdajem";
            this.txtKolikoDanaIzdajem.Size = new System.Drawing.Size(329, 26);
            this.txtKolikoDanaIzdajem.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Castellar", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(67, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 19);
            this.label4.TabIndex = 7;
            this.label4.Text = "Datuma izdata";
            // 
            // txtDatumIzdavanjaIzdajem
            // 
            this.txtDatumIzdavanjaIzdajem.Location = new System.Drawing.Point(320, 167);
            this.txtDatumIzdavanjaIzdajem.Name = "txtDatumIzdavanjaIzdajem";
            this.txtDatumIzdavanjaIzdajem.Size = new System.Drawing.Size(329, 26);
            this.txtDatumIzdavanjaIzdajem.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Castellar", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(67, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Broj Indeksa";
            // 
            // cmbBrojIndeksaIzdajem
            // 
            this.cmbBrojIndeksaIzdajem.FormattingEnabled = true;
            this.cmbBrojIndeksaIzdajem.Location = new System.Drawing.Point(320, 65);
            this.cmbBrojIndeksaIzdajem.Name = "cmbBrojIndeksaIzdajem";
            this.cmbBrojIndeksaIzdajem.Size = new System.Drawing.Size(329, 28);
            this.cmbBrojIndeksaIzdajem.TabIndex = 1;
            this.cmbBrojIndeksaIzdajem.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChangeIzdajemIndex);
            this.cmbBrojIndeksaIzdajem.TextUpdate += new System.EventHandler(this.ModifikacijaTexaKomboBoxaBrIndeksaIzdajem);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Castellar", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(67, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Primerak";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Castellar", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(67, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Knjiga";
            // 
            // cmbPrimerakIzdajem
            // 
            this.cmbPrimerakIzdajem.FormattingEnabled = true;
            this.cmbPrimerakIzdajem.Location = new System.Drawing.Point(320, 133);
            this.cmbPrimerakIzdajem.Name = "cmbPrimerakIzdajem";
            this.cmbPrimerakIzdajem.Size = new System.Drawing.Size(329, 28);
            this.cmbPrimerakIzdajem.TabIndex = 3;
            this.cmbPrimerakIzdajem.TextUpdate += new System.EventHandler(this.UpdateModifikacijaPrimerakaKnjiga);
            // 
            // cmbKnjigaIzdajem
            // 
            this.cmbKnjigaIzdajem.FormattingEnabled = true;
            this.cmbKnjigaIzdajem.Location = new System.Drawing.Point(320, 99);
            this.cmbKnjigaIzdajem.Name = "cmbKnjigaIzdajem";
            this.cmbKnjigaIzdajem.Size = new System.Drawing.Size(329, 28);
            this.cmbKnjigaIzdajem.TabIndex = 2;
            this.cmbKnjigaIzdajem.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChangePromenaKomboKnjiga);
            this.cmbKnjigaIzdajem.TextUpdate += new System.EventHandler(this.UpdateTextaKnjiga);
            // 
            // tabStranaVraceno
            // 
            this.tabStranaVraceno.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabStranaVraceno.Controls.Add(this.imgSlikaVraceno);
            this.tabStranaVraceno.Controls.Add(this.label25);
            this.tabStranaVraceno.Controls.Add(this.label26);
            this.tabStranaVraceno.Controls.Add(this.label27);
            this.tabStranaVraceno.Controls.Add(this.label28);
            this.tabStranaVraceno.Controls.Add(this.label29);
            this.tabStranaVraceno.Controls.Add(this.label30);
            this.tabStranaVraceno.Controls.Add(this.label31);
            this.tabStranaVraceno.Controls.Add(this.label32);
            this.tabStranaVraceno.Controls.Add(this.label33);
            this.tabStranaVraceno.Controls.Add(this.label34);
            this.tabStranaVraceno.Controls.Add(this.label35);
            this.tabStranaVraceno.Controls.Add(this.txtPoverenjeVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtBrojTelefonaVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtGradVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtDrzavaVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtGodinaVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtnacinFinansiranjaVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtBracniStatusVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtJmbgVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtPolVraceon);
            this.tabStranaVraceno.Controls.Add(this.txtPrezimeVraceno);
            this.tabStranaVraceno.Controls.Add(this.txtImeVraceno);
            this.tabStranaVraceno.Controls.Add(this.button1);
            this.tabStranaVraceno.Controls.Add(this.label14);
            this.tabStranaVraceno.Controls.Add(this.cmbOcenaVracam);
            this.tabStranaVraceno.Controls.Add(this.button2);
            this.tabStranaVraceno.Controls.Add(this.label11);
            this.tabStranaVraceno.Controls.Add(this.cmbOstecenaVracam);
            this.tabStranaVraceno.Controls.Add(this.label10);
            this.tabStranaVraceno.Controls.Add(this.cmbPoverenjeVracam);
            this.tabStranaVraceno.Controls.Add(this.txtDatumaVracenoVracam);
            this.tabStranaVraceno.Controls.Add(this.cmbPrimerakVracam);
            this.tabStranaVraceno.Controls.Add(this.label9);
            this.tabStranaVraceno.Controls.Add(this.label8);
            this.tabStranaVraceno.Controls.Add(this.label7);
            this.tabStranaVraceno.Controls.Add(this.cmbBrojIndeksaVracam);
            this.tabStranaVraceno.Location = new System.Drawing.Point(4, 29);
            this.tabStranaVraceno.Name = "tabStranaVraceno";
            this.tabStranaVraceno.Padding = new System.Windows.Forms.Padding(3);
            this.tabStranaVraceno.Size = new System.Drawing.Size(1454, 441);
            this.tabStranaVraceno.TabIndex = 1;
            this.tabStranaVraceno.Text = "Vraceno";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button1.Font = new System.Drawing.Font("Nirmala UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(116, 253);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 46);
            this.button1.TabIndex = 7;
            this.button1.Text = "Novi unos";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btnNoviUnosVracenoEVENT);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(68, 208);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 19);
            this.label14.TabIndex = 12;
            this.label14.Text = "Ocena knjige";
            // 
            // cmbOcenaVracam
            // 
            this.cmbOcenaVracam.FormattingEnabled = true;
            this.cmbOcenaVracam.Location = new System.Drawing.Point(280, 199);
            this.cmbOcenaVracam.Name = "cmbOcenaVracam";
            this.cmbOcenaVracam.Size = new System.Drawing.Size(323, 28);
            this.cmbOcenaVracam.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button2.Font = new System.Drawing.Font("Nirmala UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(257, 253);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(323, 46);
            this.button2.TabIndex = 6;
            this.button2.Text = "Vrati knjigu";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.btnVracanjeKnjigeEVENT);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(68, 174);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 19);
            this.label11.TabIndex = 7;
            this.label11.Text = "Ostecena?";
            // 
            // cmbOstecenaVracam
            // 
            this.cmbOstecenaVracam.FormattingEnabled = true;
            this.cmbOstecenaVracam.Location = new System.Drawing.Point(280, 165);
            this.cmbOstecenaVracam.Name = "cmbOstecenaVracam";
            this.cmbOstecenaVracam.Size = new System.Drawing.Size(323, 28);
            this.cmbOstecenaVracam.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(68, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 19);
            this.label10.TabIndex = 5;
            this.label10.Text = "Poverenje";
            // 
            // cmbPoverenjeVracam
            // 
            this.cmbPoverenjeVracam.FormattingEnabled = true;
            this.cmbPoverenjeVracam.Location = new System.Drawing.Point(280, 131);
            this.cmbPoverenjeVracam.Name = "cmbPoverenjeVracam";
            this.cmbPoverenjeVracam.Size = new System.Drawing.Size(323, 28);
            this.cmbPoverenjeVracam.TabIndex = 3;
            // 
            // txtDatumaVracenoVracam
            // 
            this.txtDatumaVracenoVracam.Location = new System.Drawing.Point(280, 99);
            this.txtDatumaVracenoVracam.Name = "txtDatumaVracenoVracam";
            this.txtDatumaVracenoVracam.Size = new System.Drawing.Size(323, 26);
            this.txtDatumaVracenoVracam.TabIndex = 2;
            // 
            // cmbPrimerakVracam
            // 
            this.cmbPrimerakVracam.FormattingEnabled = true;
            this.cmbPrimerakVracam.Location = new System.Drawing.Point(280, 65);
            this.cmbPrimerakVracam.Name = "cmbPrimerakVracam";
            this.cmbPrimerakVracam.Size = new System.Drawing.Size(323, 28);
            this.cmbPrimerakVracam.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(68, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(199, 19);
            this.label9.TabIndex = 1;
            this.label9.Text = "Datuma vraceno";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(68, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 19);
            this.label8.TabIndex = 1;
            this.label8.Text = "Primerak";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Castellar", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(68, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "Broj Indeksa";
            // 
            // cmbBrojIndeksaVracam
            // 
            this.cmbBrojIndeksaVracam.FormattingEnabled = true;
            this.cmbBrojIndeksaVracam.Location = new System.Drawing.Point(280, 31);
            this.cmbBrojIndeksaVracam.Name = "cmbBrojIndeksaVracam";
            this.cmbBrojIndeksaVracam.Size = new System.Drawing.Size(323, 28);
            this.cmbBrojIndeksaVracam.TabIndex = 0;
            this.cmbBrojIndeksaVracam.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChangePromenaBrIndexavraceno);
            // 
            // imgSlikaVraceno
            // 
            this.imgSlikaVraceno.BackgroundImage = global::biblioteka_rad.Properties.Resources.nema_slike;
            this.imgSlikaVraceno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgSlikaVraceno.Location = new System.Drawing.Point(1169, 137);
            this.imgSlikaVraceno.Name = "imgSlikaVraceno";
            this.imgSlikaVraceno.Size = new System.Drawing.Size(170, 163);
            this.imgSlikaVraceno.TabIndex = 40;
            this.imgSlikaVraceno.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1056, 101);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 20);
            this.label25.TabIndex = 33;
            this.label25.Text = "Poverenje";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1056, 67);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(112, 20);
            this.label26.TabIndex = 34;
            this.label26.Text = "Broj telefona";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(1056, 33);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 20);
            this.label27.TabIndex = 32;
            this.label27.Text = "Grad";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(674, 277);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(65, 20);
            this.label28.TabIndex = 30;
            this.label28.Text = "Drzava";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(671, 245);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(136, 20);
            this.label29.TabIndex = 31;
            this.label29.Text = "Godina rodjejna";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(671, 206);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(152, 20);
            this.label30.TabIndex = 38;
            this.label30.Text = "Nacin finansiranja";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(671, 171);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(115, 20);
            this.label31.TabIndex = 39;
            this.label31.Text = "Bracni status";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(674, 137);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(58, 20);
            this.label32.TabIndex = 37;
            this.label32.Text = "JMBG";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(674, 101);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(34, 20);
            this.label33.TabIndex = 35;
            this.label33.Text = "Pol";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(674, 69);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(73, 20);
            this.label34.TabIndex = 36;
            this.label34.Text = "Prezime";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(674, 33);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(39, 20);
            this.label35.TabIndex = 29;
            this.label35.Text = "Ime";
            // 
            // txtPoverenjeVraceno
            // 
            this.txtPoverenjeVraceno.Location = new System.Drawing.Point(1169, 95);
            this.txtPoverenjeVraceno.Name = "txtPoverenjeVraceno";
            this.txtPoverenjeVraceno.Size = new System.Drawing.Size(170, 26);
            this.txtPoverenjeVraceno.TabIndex = 21;
            // 
            // txtBrojTelefonaVraceno
            // 
            this.txtBrojTelefonaVraceno.Location = new System.Drawing.Point(1169, 62);
            this.txtBrojTelefonaVraceno.Name = "txtBrojTelefonaVraceno";
            this.txtBrojTelefonaVraceno.Size = new System.Drawing.Size(170, 26);
            this.txtBrojTelefonaVraceno.TabIndex = 22;
            // 
            // txtGradVraceno
            // 
            this.txtGradVraceno.Location = new System.Drawing.Point(1169, 27);
            this.txtGradVraceno.Name = "txtGradVraceno";
            this.txtGradVraceno.Size = new System.Drawing.Size(170, 26);
            this.txtGradVraceno.TabIndex = 20;
            // 
            // txtDrzavaVraceno
            // 
            this.txtDrzavaVraceno.Location = new System.Drawing.Point(829, 274);
            this.txtDrzavaVraceno.Name = "txtDrzavaVraceno";
            this.txtDrzavaVraceno.Size = new System.Drawing.Size(210, 26);
            this.txtDrzavaVraceno.TabIndex = 18;
            // 
            // txtGodinaVraceno
            // 
            this.txtGodinaVraceno.Location = new System.Drawing.Point(829, 242);
            this.txtGodinaVraceno.Name = "txtGodinaVraceno";
            this.txtGodinaVraceno.Size = new System.Drawing.Size(210, 26);
            this.txtGodinaVraceno.TabIndex = 19;
            // 
            // txtnacinFinansiranjaVraceno
            // 
            this.txtnacinFinansiranjaVraceno.Location = new System.Drawing.Point(829, 206);
            this.txtnacinFinansiranjaVraceno.Name = "txtnacinFinansiranjaVraceno";
            this.txtnacinFinansiranjaVraceno.Size = new System.Drawing.Size(210, 26);
            this.txtnacinFinansiranjaVraceno.TabIndex = 23;
            // 
            // txtBracniStatusVraceno
            // 
            this.txtBracniStatusVraceno.Location = new System.Drawing.Point(829, 171);
            this.txtBracniStatusVraceno.Name = "txtBracniStatusVraceno";
            this.txtBracniStatusVraceno.Size = new System.Drawing.Size(210, 26);
            this.txtBracniStatusVraceno.TabIndex = 27;
            // 
            // txtJmbgVraceno
            // 
            this.txtJmbgVraceno.Location = new System.Drawing.Point(829, 137);
            this.txtJmbgVraceno.Name = "txtJmbgVraceno";
            this.txtJmbgVraceno.Size = new System.Drawing.Size(210, 26);
            this.txtJmbgVraceno.TabIndex = 28;
            // 
            // txtPolVraceon
            // 
            this.txtPolVraceon.Location = new System.Drawing.Point(829, 103);
            this.txtPolVraceon.Name = "txtPolVraceon";
            this.txtPolVraceon.Size = new System.Drawing.Size(210, 26);
            this.txtPolVraceon.TabIndex = 26;
            // 
            // txtPrezimeVraceno
            // 
            this.txtPrezimeVraceno.Location = new System.Drawing.Point(829, 69);
            this.txtPrezimeVraceno.Name = "txtPrezimeVraceno";
            this.txtPrezimeVraceno.Size = new System.Drawing.Size(210, 26);
            this.txtPrezimeVraceno.TabIndex = 24;
            // 
            // txtImeVraceno
            // 
            this.txtImeVraceno.Location = new System.Drawing.Point(829, 33);
            this.txtImeVraceno.Name = "txtImeVraceno";
            this.txtImeVraceno.Size = new System.Drawing.Size(210, 26);
            this.txtImeVraceno.TabIndex = 25;
            // 
            // frmIzdatoVracenoForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1462, 474);
            this.Controls.Add(this.KontrolaTabIzdatoVrateci);
            this.Name = "frmIzdatoVracenoForma";
            this.Text = "IzdatoVracenoForma";
            this.Load += new System.EventHandler(this.PokretanjeFormePocetak);
            this.KontrolaTabIzdatoVrateci.ResumeLayout(false);
            this.tabStanaIzdato.ResumeLayout(false);
            this.tabStanaIzdato.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaStudentaIzdajem)).EndInit();
            this.tabStranaVraceno.ResumeLayout(false);
            this.tabStranaVraceno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlikaVraceno)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl KontrolaTabIzdatoVrateci;
        private System.Windows.Forms.TabPage tabStanaIzdato;
        private System.Windows.Forms.Button btnIzdajem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPredvidjenoVracanjeIzdajem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtKolikoDanaIzdajem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDatumIzdavanjaIzdajem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbBrojIndeksaIzdajem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbPrimerakIzdajem;
        private System.Windows.Forms.ComboBox cmbKnjigaIzdajem;
        private System.Windows.Forms.TabPage tabStranaVraceno;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbOstecenaVracam;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbPoverenjeVracam;
        private System.Windows.Forms.TextBox txtDatumaVracenoVracam;
        private System.Windows.Forms.ComboBox cmbPrimerakVracam;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbBrojIndeksaVracam;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbBibliotekarIzdajem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbOcenaVracam;
        private System.Windows.Forms.Button btnNoviUnosIzdajem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDrzavaIzdajem;
        private System.Windows.Forms.TextBox txtGodinaRodjenjaIzdajem;
        private System.Windows.Forms.TextBox txtnacinFinansiranjaIzdajem;
        private System.Windows.Forms.TextBox txtBracniStatusIzdajem;
        private System.Windows.Forms.TextBox txtJmbgizdajem;
        private System.Windows.Forms.TextBox txtpolIzdajem;
        private System.Windows.Forms.TextBox txtPrezimeIzdajem;
        private System.Windows.Forms.TextBox txtImeIzdajem;
        private System.Windows.Forms.PictureBox imgSlikaStudentaIzdajem;
        private System.Windows.Forms.TextBox txtPoverenjeIzdajem;
        private System.Windows.Forms.TextBox txtBrojTelefonaIdajem;
        private System.Windows.Forms.TextBox txtGradIzdajem;
        private System.Windows.Forms.PictureBox imgSlikaVraceno;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtPoverenjeVraceno;
        private System.Windows.Forms.TextBox txtBrojTelefonaVraceno;
        private System.Windows.Forms.TextBox txtGradVraceno;
        private System.Windows.Forms.TextBox txtDrzavaVraceno;
        private System.Windows.Forms.TextBox txtGodinaVraceno;
        private System.Windows.Forms.TextBox txtnacinFinansiranjaVraceno;
        private System.Windows.Forms.TextBox txtBracniStatusVraceno;
        private System.Windows.Forms.TextBox txtJmbgVraceno;
        private System.Windows.Forms.TextBox txtPolVraceon;
        private System.Windows.Forms.TextBox txtPrezimeVraceno;
        private System.Windows.Forms.TextBox txtImeVraceno;
    }
}