﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
namespace biblioteka_rad
{
    class StudentKlasa
    {
        public Image SlikaStudenta { get; set; }
        public static int RedniProj { get; set; }
        string DalPopunjeno;
        string brIndeksa;
        public string BrIndeksa
        {
            get { return brIndeksa; }
            set { brIndeksa = value; }
        }
        string ime;

        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }
        string prezime;

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }
        string pol;

        public string Pol
        {
            get { return pol; }
            set { pol = value; }
        }
        string jmbg;

        public string Jmbg
        {
            get { return jmbg; }
            set { jmbg = value; }
        }
        string bracniStatus;

        public string BracniStatus
        {
            get { return bracniStatus; }
            set { bracniStatus = value; }
        }
        string finansiranje;

        public string Finansiranje
        {
            get { return finansiranje; }
            set { finansiranje = value; }
        }
        string godina;

        public string Godina
        {
            get { return godina; }
            set { godina = value; }
        }
        string drzava;

        public string Drzava
        {
            get { return drzava; }
            set { drzava = value; }
        }
        string grad;

        public string Grad
        {
            get { return grad; }
            set { grad = value; }
        }
        string telefon;

        public string Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }
        string poverenje;

        public string Poverenje
        {
            get { return poverenje; }
            set { poverenje = value; }
        }
        public StudentKlasa(string odabirKonstruktora, string kbrIndeksa, string kime, string kprezime, string kpol, string kjmbg,
            string kbracni, string kfinansiranje, string kgodina, string kdrzava, string kgrad, string ktel, string kpoverenje) //konstruktor 13 za unos u listu
        {
            brIndeksa = kbrIndeksa;
            ime = kime; prezime = kprezime; pol = kpol; jmbg = kjmbg; bracniStatus = kbracni;
            finansiranje = kfinansiranje;
            godina = kgodina; drzava = kdrzava; grad = kgrad; telefon = ktel; poverenje = kpoverenje;
        }
        public StudentKlasa(TextBox kbrIndeksa, TextBox kime, TextBox kprezime, ComboBox kpol, TextBox kjmbg,
            ComboBox kbracni, ComboBox kfinansiranje, TextBox kgodina, TextBox kdrzava, TextBox kgrad, TextBox ktel, ComboBox kpoverenje) //konstruktor 12 zxa unos ustudenta u bazu
        {
            if (String.IsNullOrEmpty(kbrIndeksa.Text))
            {
                DalPopunjeno = "Potrebno je uneti broj indeksa u formatu(IN56/13)";
            }
            else if (String.IsNullOrEmpty(kime.Text))
            {
                DalPopunjeno="Potrebno je uneti ime studenta";
            }
            else if (String.IsNullOrEmpty(kprezime.Text))
            { DalPopunjeno= "Potrebno je uneti prezime studenta"; }
            else if (String.IsNullOrEmpty(kpol.Text))
            { DalPopunjeno="Potrebno je uneti pol studenta"; }
            else if (String.IsNullOrEmpty(kjmbg.Text))
            { DalPopunjeno ="Potrebno je uneti jedinstveni maticni broj gradjana studenta"; }
            else if (String.IsNullOrEmpty(kbracni.Text))
            { DalPopunjeno="Potrebno je uneti bracni status studenta"; }
            else if (String.IsNullOrEmpty(kfinansiranje.Text))
            { DalPopunjeno="Potrebno je uneti nacin finansiranja studenta"; }
            else if (String.IsNullOrEmpty(kgodina.Text))
            { DalPopunjeno="Potrebno je uneti godinu rodjenja studenta"; }
            else if (String.IsNullOrEmpty(kdrzava.Text))
            { DalPopunjeno="Potrebno je uneti drzavu porekla studenta"; }
            else if (String.IsNullOrEmpty(kgrad.Text))
            { DalPopunjeno="Potrebno je uneti grad u kom zivi student"; }
            else if (String.IsNullOrEmpty(ktel.Text))
            { DalPopunjeno="Potrebno je uneti telefon studenta"; }
            else if (String.IsNullOrEmpty(kpoverenje.Text))
            { DalPopunjeno="Potrebno je uneti nivo poverenja studenta (odnos prema knjigama)"; }
            else
            { 
                DalPopunjeno="popunjeno";
                brIndeksa = kbrIndeksa.Text;
                ime = kime.Text; prezime = kprezime.Text; pol = kpol.Text; jmbg = kjmbg.Text; bracniStatus = kbracni.Text;
                finansiranje = kfinansiranje.Text;
                godina = kgodina.Text; drzava = kdrzava.Text; grad = kgrad.Text; telefon = ktel.Text; poverenje = kpoverenje.Text;
                
                korektanIndeks(); KorektanPol(); JMBGkorektan(); podobanKorektno();
                finansiranjeKorektno(); bracnoStanjeKorektno();

            }


        }
        public string NijePopunjeno()
        {
            return DalPopunjeno;
        }
        public bool korektanIndeks()
        {

            if (this.brIndeksa[0] == 'G' && this.brIndeksa[1] == 'R')
            {
                return true;
            }
            else if (this.brIndeksa[0] == 'I' && brIndeksa[1] == 'N')
            {
                return true;
            }
            else if (brIndeksa[0] == 'P' && brIndeksa[1] == 'M')
            {
                return true;
            }
            else if (brIndeksa[0] == 'R' && brIndeksa[1] == 'R')
            {
                return true;
            }
            else if (brIndeksa[0] == 'T' && brIndeksa[1] == 'H')
            {
                return true;
            }
            else if (brIndeksa[0] == 'M' && brIndeksa[1] == 'A')
            {
                return true;
            }
            else if (brIndeksa[0] == 'T' && brIndeksa[1] == 'T')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool KorektanPol()
        {
            if (this.pol == "Musko" || pol == "musko" || pol == "Zensko" || pol == "zensko")
            {
                return true;
            }
            else
                return false;
        }
        public bool JMBGkorektan()
        {
            if (this.jmbg.Length == 13)
            {
                return true;
            }
            else
                return false;
        }
        public bool bracnoStanjeKorektno()
        {
            if (this.bracniStatus == "Slobodan" || bracniStatus == "Slobodna" || bracniStatus 
                == "Ozenjen" || bracniStatus == "Udata" || bracniStatus == "Razveden"
                || bracniStatus == "Razvedena" || bracniStatus == "Udovac" || bracniStatus == "Udovica")
            {
                return true;
            }
            else
                return false;
        }
        public bool finansiranjeKorektno()
        {
            if (this.finansiranje == "Buzet" || finansiranje == "Samofinansiranje")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool podobanKorektno()
        {
            if (this.poverenje == "Pouzdan" || poverenje == "Srednje" || poverenje == "Nepouzdan")
                return true;
            else
                return false;
        }

        public static string NeIspravanBrojInkdeksaStr()
        {
            return "Broj indeksa uneti u formatu IN56/13 Obavezna velika prva dva slova\n";
        }
        public static string NeIspravnoUnetPol()
        {
            return "Pol uneti isto kao opcije u padajucoj listi\n";
        }
        public static string NeispravanJMBG()
        {
            return "JMBG treba da ime 13 cifara\n";
        }
        public static string NeispravnoBracnoStanje()
        {
            return "bracno stanje treba da bude upisano kao opcije u padajucoj listi\n";
        }
        public static string NeispravnoFinansiranje()
        {
            return "Nacin finansiranje treba da bude upisano kao opcije u padajucoj listi\n";
        }
        public static string NeispravnoPodoban()
        {
            return "Poverenje u studenta treba da bude upisano kao opcije u padajucoj listi\n";
        }
    
    }
}
