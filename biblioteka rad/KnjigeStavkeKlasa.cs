﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace biblioteka_rad
{
    class KnjigeStavkeKlasa
    {
        string indetifikator;

        public string Indetifikator
        {
            get { return indetifikator; }
            set { indetifikator = value; }
        }
        string slobodnaZauzeta;

        public string SlobodnaZauzeta
        {
            get { return slobodnaZauzeta; }
            set { slobodnaZauzeta = value; }
        }
        string ostecenaNeostecena;

        public string OstecenaNeostecena
        {
            get { return ostecenaNeostecena; }
            set { ostecenaNeostecena = value; }
        }
        string mestoKnjige;

        public string MestoKnjige
        {
            get { return mestoKnjige; }
            set { mestoKnjige = value; }
        }
        string imeKnjigeNaslov;

        public string ImeKnjigeNaslov
        {
            get { return imeKnjigeNaslov; }
            set { imeKnjigeNaslov = value; }
        }
        bool praznaPolja;
        public KnjigeStavkeKlasa(string indetifikatorKnjige, string slobodnaZauzeta, string ostecena, string mesto, string nazivKjnigeNaslov, string str)
        {
            this.indetifikator = indetifikatorKnjige; this.slobodnaZauzeta = slobodnaZauzeta; this.ostecenaNeostecena = ostecena;
            this.mestoKnjige = mesto; this.imeKnjigeNaslov = nazivKjnigeNaslov;
        }
        public KnjigeStavkeKlasa(TextBox indetifikatorKnjige, ComboBox slobodnaZauzeta, ComboBox ostecena, TextBox mesto, ComboBox nazivKjnigeNaslov)
        {
            if (String.IsNullOrEmpty(indetifikatorKnjige.Text) || String.IsNullOrEmpty(slobodnaZauzeta.Text) ||
                String.IsNullOrEmpty(ostecena.Text) || String.IsNullOrEmpty(mesto.Text))
            {
                MessageBox.Show("Polja knjige treba da budu popunjena");
                praznaPolja = true;
            }
            else if (String.IsNullOrEmpty(nazivKjnigeNaslov.Text))
            {
                MessageBox.Show("Naslov knjige takodje mora biti unet");
            }
            else
            {
                praznaPolja = false;
                this.indetifikator = indetifikatorKnjige.Text;
                this.slobodnaZauzeta = slobodnaZauzeta.Text;
                this.ostecenaNeostecena = ostecena.Text;
                this.mestoKnjige = mesto.Text;
                this.imeKnjigeNaslov = nazivKjnigeNaslov.Text;
            }
        }
        public bool KnjigeStavkaUnetaPoljaOK()
        {
            if (praznaPolja)
            { return false; }
            else
            { return true; }
        }
        public void KonekcijaUpis()
        {
            string konekcijaStr = "server=localhost; uid=root; password=; database=biblioteka;";
            using (MySqlConnection konekcija = new MySqlConnection(konekcijaStr))
            {
                konekcija.Open();
                string traziKnjigaIdStr = "select knjiga_id from knjiga where nazivKnjige=@nazivKnjige;";
                using (MySqlCommand komandaTraziKnjigaId = new MySqlCommand(traziKnjigaIdStr, konekcija))
                {
                    komandaTraziKnjigaId.Parameters.AddWithValue("@nazivKnjige", this.imeKnjigeNaslov);
                    string forgenStr = "";
                    MySqlDataReader citac = komandaTraziKnjigaId.ExecuteReader();
                    while (citac.Read())
                    {
                        forgenStr = citac.GetString(0);
                    }
                    citac.Close();
                    string unosStr = "insert into knjige (knjige.indetifikator, knjige.slobodnaZauzeta,knjige.ostecenaNeostecena, knjige.mestoKnjige, knjiga_knjiga_id) " +
                        " values (@indeti, @slobodnaZauzeta, @ostecenaNe, @mesto, " +
                        " @spoljni) ; ";
                    using (MySqlCommand komandaUnosa = new MySqlCommand(unosStr, konekcija))
                    {
                        komandaUnosa.Parameters.AddWithValue("@indeti", this.indetifikator);
                        komandaUnosa.Parameters.AddWithValue("@slobodnaZauzeta", this.slobodnaZauzeta);
                        komandaUnosa.Parameters.AddWithValue("@ostecenaNe", this.ostecenaNeostecena);
                        komandaUnosa.Parameters.AddWithValue("@mesto", this.mestoKnjige);
                        komandaUnosa.Parameters.AddWithValue("@spoljni", forgenStr);
                        komandaUnosa.ExecuteNonQuery();
                    }
                }
            }
        }//kraj metode
        
        
    }
}
