﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmBibliotekarUnos : Form
    {
        public frmBibliotekarUnos()
        {
            InitializeComponent();
        }

        private void btnUnosNovogBibliotekara_Click(object sender, EventArgs e)
        {
            try
            {
                frmJesiSiguranForma jesiSiguran = new frmJesiSiguranForma("Da li ste sigruni da zelite \nda unesete novog bibliotekara",
                    "Jesam", "Nisam");
                jesiSiguran.ShowDialog();
                if (frmJesiSiguranForma.KluknutoDaDugmeJesiSiguran == true)
                {
                    Bibliotekarklasa b1 = new Bibliotekarklasa(txtSifraBibliotekara, txtImeBibliotekara, txtPrezimeBibliotekara, txtBrojFonaBibliotekara);
                    if (!b1.PoljeJePrazno())
                    {
                        b1.KonekcijaUnosNovogBibliotekara();
                        txtSifraBibliotekara.Clear(); txtImeBibliotekara.Clear(); txtPrezimeBibliotekara.Clear(); txtBrojFonaBibliotekara.Clear();
                        MessageBox.Show("Novi bibliotekar je unet");
                    }
                }
 
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
