﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmJesiSiguranForma : Form
    {
        public frmJesiSiguranForma(string pitanje, string dugme1, string dugme2)
        {
            InitializeComponent();
            lblPitanje.Text = pitanje;
            btn1.Text = dugme1;
            btn2.Text = dugme2;
            KluknutoDaDugmeJesiSiguran = false;
        }
        public static bool KluknutoDaDugmeJesiSiguran { get; set; }
        private void btn1_Click(object sender, EventArgs e)
        {
            KluknutoDaDugmeJesiSiguran = true;
            this.Close();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            KluknutoDaDugmeJesiSiguran = false;
            this.Close();
        }
    }
}
