﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using System.Drawing;
namespace biblioteka_rad
{
    class IzdatoKlasa
    {
        string bibliotekar;
        string brojIndeksa;
        string knjiga;
        string primerak;
        DateTime datumaIzdata;
        int kolikoDana;
        DateTime predvidjeniPovratak;

        public DateTime PredvidjeniPovratak
        {
            get { return predvidjeniPovratak; }
            set { predvidjeniPovratak = value; }
        }
        public bool daniOk { get; private set; }
        bool popunjenaPolja;
        public DateTime RazbiDatum(string datumaIzdato)
        {
            DateTime vreme = DateTime.Now;

            string[] danMesecGodina = new string[3];
            int j = 0;
            string datumaIzdatoStr = datumaIzdato;
            for (int i = 0; i < datumaIzdatoStr.Length && j < danMesecGodina.Length; i++)
            {
                if (datumaIzdatoStr[i] != ' ')
                {
                    if (datumaIzdatoStr[i] != '/')
                    {
                        danMesecGodina[j] += datumaIzdatoStr[i].ToString();
                    }
                }
                if (datumaIzdatoStr[i] == '/')
                {
                    j++;
                }
            }
            danMesecGodina[2] += '/'; danMesecGodina[1] += '/';
            string spojen = danMesecGodina[2] + danMesecGodina[1] + danMesecGodina[0];
            vreme = new DateTime(); vreme = Convert.ToDateTime(spojen);

            return vreme;
        }
        public static string LepoNapisiDatumZaTextBoxSamoAkoSeVadiIzGotovogDatuma(DateTime datum)
        {
            string LepoNapisanDatum = datum.ToShortDateString();
            string[] danMesecGodina = new string[3];
            int j = 0;
            for (int i = 0; i < LepoNapisanDatum.Length && i < 10; i++)
            {
                if (LepoNapisanDatum[i] != ' ')
                {
                    if (LepoNapisanDatum[i] != '/')
                    {
                        danMesecGodina[j] += LepoNapisanDatum[i].ToString();
                    }
                }
                if (LepoNapisanDatum[i] == '/') { j++; }
            }
            LepoNapisanDatum = danMesecGodina[1] + "/" + danMesecGodina[0] + "/" + danMesecGodina[2];
            return LepoNapisanDatum;
        }
        public IzdatoKlasa(ComboBox bibliotekar, ComboBox brIndeksa, ComboBox knjiga, ComboBox primerak, TextBox datumaIzdata,
            TextBox naKolikoDana)
        {
            if (String.IsNullOrEmpty(bibliotekar.Text) || String.IsNullOrEmpty(brIndeksa.Text) || String.IsNullOrEmpty(primerak.Text) ||
            String.IsNullOrEmpty(datumaIzdata.Text) || String.IsNullOrEmpty(naKolikoDana.Text))
            {
                popunjenaPolja = false;
                MessageBox.Show("Polja trebaju biti popunjena");
            }
            else
            {
                daniOk = false;
                daniOk = int.TryParse(naKolikoDana.Text, out this.kolikoDana);
                if (daniOk)
                {
                    this.bibliotekar = bibliotekar.Text; this.brojIndeksa = brIndeksa.Text; this.knjiga = knjiga.Text; this.primerak = primerak.Text;
                    
                    this.datumaIzdata = Convert.ToDateTime(RazbiDatum(datumaIzdata.Text));

                    this.predvidjeniPovratak = Convert.ToDateTime(RazbiDatum(datumaIzdata.Text));
                    this.predvidjeniPovratak = this.predvidjeniPovratak.AddDays(this.kolikoDana);

                    popunjenaPolja = true;
                }
                else
                {
                    MessageBox.Show("Broj dana nije ispravno unet");
                }
            }
        }//kraj konstruktora
        public bool PopunjenaPoljaizdajemOK()
        {
            return popunjenaPolja;
        }
        public void KonekcijaUpisIzdavanja()
        {
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();

                string kljucIndetifikatoraStr = ""; int brojKljucevaIndetifikatora = 0;
                string kljucBibliotekar = ""; int brojKljucevaBibliotekara = 0;
                string kljucStudentStr = ""; int brojKljucevaStudent = 0;

                string komandaTraziKnjigeIStr = "select knjige_id from knjige where indetifikator = @indetigikator;";
                using (MySqlCommand komandaTraziKnjigeID = new MySqlCommand(komandaTraziKnjigeIStr, konekcija))
                {
                    komandaTraziKnjigeID.Parameters.AddWithValue("@indetigikator", this.primerak);
                    kljucIndetifikatoraStr = ""; brojKljucevaIndetifikatora = 0;
                    MySqlDataReader citac = komandaTraziKnjigeID.ExecuteReader();
                    while (citac.Read()) { kljucIndetifikatoraStr = citac.GetString(0); brojKljucevaIndetifikatora++; } citac.Close();
                }
                string komandaTraziKljucBibliotekaraStr = "select bibliotekar_id from bibliotekar where sifraRadnika = @sifraRadnika;";
                using (MySqlCommand komandaTraziBibliotekarID = new MySqlCommand(komandaTraziKljucBibliotekaraStr, konekcija))
                {
                    komandaTraziBibliotekarID.Parameters.AddWithValue("@sifraRadnika", this.bibliotekar);
                    MySqlDataReader citac = komandaTraziBibliotekarID.ExecuteReader();
                    kljucBibliotekar = ""; brojKljucevaBibliotekara = 0;
                    while (citac.Read()) { kljucBibliotekar = citac.GetString(0); brojKljucevaBibliotekara++; } citac.Close();
                }
                string komandaTraziKljucStudentaIdStr = "select student_id from student where brojIndeksa = @brojIndeksa;";
                using (MySqlCommand komandaTraziStudentID = new MySqlCommand(komandaTraziKljucStudentaIdStr, konekcija))
                {
                    komandaTraziStudentID.Parameters.AddWithValue("@brojIndeksa", this.brojIndeksa);
                    MySqlDataReader citac = komandaTraziStudentID.ExecuteReader();
                    kljucStudentStr = ""; brojKljucevaStudent = 0;
                    while (citac.Read()) { kljucStudentStr = citac.GetString(0); brojKljucevaStudent++; } citac.Close();
                }
                if (brojKljucevaBibliotekara <= 0)
                {
                    MessageBox.Show("Ovaj bibliotekar ne postoji");
                }
                else if (brojKljucevaIndetifikatora <= 0)
                {
                    MessageBox.Show("Ovaj primerak knjige ne postoji");
                }
                else if (brojKljucevaStudent < 0)
                {
                    MessageBox.Show("Ovaj student ne postoji");
                }
                else
                {
                    
                    using (MySqlCommand komandaUnos = new MySqlCommand())
                    {
                        komandaUnos.Connection = konekcija;
                        komandaUnos.CommandText = "insert into izdatovraceno (bibliotekar_bibliotekar_id, student_student_id, knjige_knjige_id, " +
                           " datumaUzeto, kolikoDana, izdatoVracenocol) values (@kljucBibliotekar, @kljucStudent, @kljucKnjigePrimerci, @datumUzeto, @dana, " + 
                           "@predvidjeniPovratak);";
                        komandaUnos.Parameters.AddWithValue("@kljucBibliotekar", kljucBibliotekar);
                        komandaUnos.Parameters.AddWithValue("@kljucStudent", kljucStudentStr);
                        komandaUnos.Parameters.AddWithValue("@kljucKnjigePrimerci", kljucIndetifikatoraStr);
                        komandaUnos.Parameters.AddWithValue("@datumUzeto", this.datumaIzdata);
                        komandaUnos.Parameters.AddWithValue("@dana", this.kolikoDana);
                        komandaUnos.Parameters.AddWithValue("@predvidjeniPovratak", this.predvidjeniPovratak);
                        komandaUnos.ExecuteNonQuery();
                    }
                    using (MySqlCommand komandaUpdatePrimerakDaJeZauzet = new MySqlCommand())
                    {
                        komandaUpdatePrimerakDaJeZauzet.Connection = konekcija;
                        komandaUpdatePrimerakDaJeZauzet.CommandText = "update knjige set slobodnaZauzeta ='Zauzeta' where knjige_id = @kljucKnjige;";
                        komandaUpdatePrimerakDaJeZauzet.Parameters.AddWithValue("@kljucKnjige", kljucIndetifikatoraStr);
                        komandaUpdatePrimerakDaJeZauzet.ExecuteNonQuery();
                    }
                }
                
                
            }//kraj usinga
        }//kraj metode konekcija

        public static void BogacenjeKomboBoxaBibliotekar(ComboBox bibliotekarKombo)
        {
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaSviBibliotekari = new MySqlCommand())
                {
                    bibliotekarKombo.Items.Clear();
                    komandaSviBibliotekari.Connection = konekcija;
                    komandaSviBibliotekari.CommandText = "select sifraRadnika from bibliotekar;";
                    MySqlDataReader citac = komandaSviBibliotekari.ExecuteReader();
                    if (citac.Read())
                    {
                        citac.Close(); citac = komandaSviBibliotekari.ExecuteReader();
                        while (citac.Read())
                        {

                            bibliotekarKombo.Items.Add(citac.GetString(0));
                        }
                    }
                    citac.Close();
                    
                }
            }
        }//kraj metode bogacenje bibliotekar
        public static void BogacenjeKomboBoxaBrojIndeksa(ComboBox brojIndeksa)
        {
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaSviIndeksi = new MySqlCommand())
                {
                    brojIndeksa.Items.Clear();
                    komandaSviIndeksi.Connection = konekcija;
                    komandaSviIndeksi.CommandText = "select brojIndeksa from student;";
                    MySqlDataReader citac = komandaSviIndeksi.ExecuteReader();
                    if (citac.Read())
                    {
                        citac.Close(); citac = komandaSviIndeksi.ExecuteReader();
                        while (citac.Read())
                        {
                            brojIndeksa.Items.Add(citac.GetString(0));
                        }
                    }
                    citac.Close();
                }
            }
        }//kraj metode bogacenje broj indeksa
        public static void BogacenjeKomboBoxaKnjiga(ComboBox knjige)
        {
            knjige.Items.Clear();
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaSveKnjiga = new MySqlCommand())
                {
                    komandaSveKnjiga.Connection = konekcija;
                    komandaSveKnjiga.CommandText = "select nazivKnjige from knjiga;";
                    MySqlDataReader citac = komandaSveKnjiga.ExecuteReader();
                    while (citac.Read())
                    {
                        knjige.Items.Add(citac.GetString(0));
                    }
                    citac.Close();
                }
            }
        }//kraj metode bogacenje kombo knjiga
        

        public static void ModifikacijaTextaBrIndeksa(ComboBox brojIndeksa)
        {
            List<string> ListaBrojevaIndexa = new List<string>();
            brojIndeksa.Items.Clear();
            brojIndeksa.SelectionStart = brojIndeksa.Text.Length;
            brojIndeksa.SelectionLength = 0;
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaSviIndeksi = new MySqlCommand())
                {
                    komandaSviIndeksi.Connection = konekcija;
                    komandaSviIndeksi.CommandText = "select brojIndeksa from student;";
                    MySqlDataReader citac = komandaSviIndeksi.ExecuteReader();
                    if (citac.Read())
                    {
                        citac.Close(); citac = komandaSviIndeksi.ExecuteReader();
                        while (citac.Read())
                        {
                            ListaBrojevaIndexa.Add(citac.GetString(0));
                        }
                    }
                    citac.Close();
                }
                string komboText = brojIndeksa.Text;
                int brojPoklapanja = 0;
                for (int i = 0; i < ListaBrojevaIndexa.Count; i++)
                {
                    for (int j = 0; j < komboText.Length && j < ListaBrojevaIndexa[i].Length; j++)
                    {
                        if (komboText[j] == ListaBrojevaIndexa[i][j])
                        {
                            brojPoklapanja++;
                        }
                    }
                    if(brojPoklapanja == komboText.Length)
                    {
                        brojIndeksa.Items.Add(ListaBrojevaIndexa[i]);
                    }
                    brojPoklapanja = 0;
                }
            }
        }//kraj metode modifikacija broj indexa

        public static void ModifikacijaKomboKnjiga(ComboBox knjige)
        {
            List<string> ListaKnjigaNaslovi = new List<string>();
            knjige.Items.Clear();
            knjige.SelectionStart = knjige.Text.Length;
            knjige.SelectionLength = 0;
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaSveKnjiga = new MySqlCommand())
                {
                    komandaSveKnjiga.Connection = konekcija;
                    komandaSveKnjiga.CommandText = "select nazivKnjige from knjiga;";
                    MySqlDataReader citac = komandaSveKnjiga.ExecuteReader();
                    while (citac.Read())
                    {
                        ListaKnjigaNaslovi.Add(citac.GetString(0));
                    }
                    citac.Close();
                }
                int brojPoklapanja = 0;
                string komboText = knjige.Text;
                for (int i = 0; i < ListaKnjigaNaslovi.Count; i++)
                {
                    for (int j = 0; j < komboText.Length && j < ListaKnjigaNaslovi[i].Length; j++)
                    {
                        if (komboText[j] == ListaKnjigaNaslovi[i][j])
                        {
                            brojPoklapanja++;
                        }
                    }
                    if (brojPoklapanja == komboText.Length)
                    {
                        knjige.Items.Add(ListaKnjigaNaslovi[i]);
                    }
                    brojPoklapanja = 0;
                }
            }
        }//kraj metode modifikacija knjiga

        public static void BogacenjePrimerakaKnjiga(ComboBox knjiga, ComboBox primerak)
        {
            primerak.Items.Clear();
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=;database=biblioteka;"))
            {
                konekcija.Open();
                int brojacKljuceva = 0;
                string kljuc="";
                using (MySqlCommand komandaKljucKnjige = new MySqlCommand())
                {
                    komandaKljucKnjige.Connection = konekcija;
                    komandaKljucKnjige.CommandText = "select knjiga_id from knjiga where nazivKnjige = @nazivKnjige;";
                    komandaKljucKnjige.Parameters.AddWithValue("@nazivKnjige", knjiga.Text);
                    MySqlDataReader citac = komandaKljucKnjige.ExecuteReader();
                    brojacKljuceva =0;
                    while (citac.Read())
                    {
                        brojacKljuceva++;
                        kljuc = citac.GetString(0);
                    }
                    citac.Close();
                }
                if (brojacKljuceva > 0)
                {
                    using (MySqlCommand komandaSviPrimerciKnjiga = new MySqlCommand())
                    {
                        komandaSviPrimerciKnjiga.Connection = konekcija;
                        komandaSviPrimerciKnjiga.CommandText = "select indetifikator from knjige where knjiga_knjiga_id = @soljniKljuc && " +
                            " slobodnaZauzeta='slobodna' ;";
                        komandaSviPrimerciKnjiga.Parameters.AddWithValue("@soljniKljuc", kljuc);
                        MySqlDataReader citac = komandaSviPrimerciKnjiga.ExecuteReader();
                        while (citac.Read())
                        {
                            primerak.Items.Add(citac.GetString(0));
                        }

                    }
                }
            }
        }//kraj metode bogacenje primeraka knjiga

        public static void UpdateTextaPrimerci(ComboBox knjiga, ComboBox primerak)
        {
            List<string> ListaPrimeraka = new List<string>();
            primerak.Items.Clear();
            primerak.SelectionStart = primerak.Text.Length;
            primerak.SelectionLength = 0;
            
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=;database=biblioteka;"))
            {
                konekcija.Open();
                int brojacKljuceva = 0;
                string kljuc = "";
                using (MySqlCommand komandaKljucKnjige = new MySqlCommand())
                {
                    komandaKljucKnjige.Connection = konekcija;
                    komandaKljucKnjige.CommandText = "select knjiga_id from knjiga where nazivKnjige = @nazivKnjige;";
                    komandaKljucKnjige.Parameters.AddWithValue("@nazivKnjige", knjiga.Text);
                    MySqlDataReader citac = komandaKljucKnjige.ExecuteReader();
                    brojacKljuceva = 0;
                    while (citac.Read())
                    {
                        brojacKljuceva++;
                        kljuc = citac.GetString(0);
                    }
                    citac.Close();
                }
                if (brojacKljuceva > 0)
                {
                    using (MySqlCommand komandaSviPrimerciKnjiga = new MySqlCommand())
                    {
                        komandaSviPrimerciKnjiga.Connection = konekcija;
                        komandaSviPrimerciKnjiga.CommandText = "select indetifikator from knjige where knjiga_knjiga_id = @soljniKljuc && " +
                            " slobodnaZauzeta='slobodna' ;";
                        komandaSviPrimerciKnjiga.Parameters.AddWithValue("@soljniKljuc", kljuc);
                        MySqlDataReader citac = komandaSviPrimerciKnjiga.ExecuteReader();
                        while (citac.Read())
                        {
                            ListaPrimeraka.Add(citac.GetString(0));
                        }

                    }
                } 
            }
            string komboText = primerak.Text;
            int brojPoklapanja = 0;
            for (int i = 0; i < ListaPrimeraka.Count; i++)
            {
                for (int j = 0; j < ListaPrimeraka[i].Length && j < komboText.Length; j++)
                {
                    if (ListaPrimeraka[i][j] == komboText[j])
                    {
                        brojPoklapanja++;
                    }
                }
                if (brojPoklapanja == komboText.Length)
                {
                    primerak.Items.Add(ListaPrimeraka[i]);
                }
                brojPoklapanja = 0;
            }
        }//kraj metode Update texta kombo boxa primerci
        
        
        public static void PromenaKomboBoxaBrojIndeksa(ComboBox brojIndeksaCon, TextBox imeCon, TextBox prezimeCon, 
            TextBox polCon, TextBox jmbgCon, TextBox bracniStatusCon, TextBox nacinFinansiranjaCon, TextBox godinaCon, TextBox drzavaCon,
             TextBox gradCon, TextBox telefonCon, TextBox poverenje, PictureBox slikaIzdajemCon)
        {
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=;database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand cmdPodaciStudenta = new MySqlCommand())
                {
                    cmdPodaciStudenta.Connection = konekcija;
                    cmdPodaciStudenta.CommandText = "select * from student where brojIndeksa = @brIndeksa;";
                    cmdPodaciStudenta.Parameters.AddWithValue("@brIndeksa", brojIndeksaCon.Text);
                    MySqlDataReader citac = cmdPodaciStudenta.ExecuteReader();
                    if (citac.Read())
                    {
                        imeCon.Text = citac.GetString(2);
                        prezimeCon.Text = citac.GetString(3);
                        polCon.Text = citac.GetString(4);
                        jmbgCon.Text = citac.GetString(5);
                        bracniStatusCon.Text = citac.GetString(6);
                        nacinFinansiranjaCon.Text = citac.GetString(7);
                        godinaCon.Text = citac.GetString(8);
                        drzavaCon.Text = citac.GetString(9);
                        gradCon.Text = citac.GetString(10);
                        telefonCon.Text = citac.GetString(11);
                        poverenje.Text = citac.GetString(12);
                        byte[] biti = null;
                        if (citac["slika"] == DBNull.Value)
                        {
                            slikaIzdajemCon.Image = null;
                        }
                        else
                        {
                            biti = (byte[])citac["slika"];
                            MemoryStream ms = new MemoryStream(biti);
                            Image img = Image.FromStream(ms);
                            slikaIzdajemCon.Image = img;
                            slikaIzdajemCon.SizeMode = PictureBoxSizeMode.StretchImage;
                        }
                    }
                    citac.Close();
                }
            }
        }
    }
}
