﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmPitanjeZaParametarskiUpit : Form
    {
        string porukaNaLabelu;
        string dugmeText;
        string izbor;
        public frmPitanjeZaParametarskiUpit(string porukaNaLabelu, string dugmeText, string izbor)
        {
            InitializeComponent();
            this.porukaNaLabelu = porukaNaLabelu;
            this.dugmeText = dugmeText;
            this.izbor = izbor;
        }

        private void loadPokretanjeFormePitalica(object sender, EventArgs e)
        {
            lblPoruka.Text = porukaNaLabelu;
            btnPrikazi.Text = dugmeText;
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            frmParametarskiIzvestaj pi = new frmParametarskiIzvestaj(this.izbor, txtParametar.Text);
            pi.ShowDialog();
            if (!chcOstaviOtvoreno.Checked)
            {
                this.Close();
            }
        }
    }
}
