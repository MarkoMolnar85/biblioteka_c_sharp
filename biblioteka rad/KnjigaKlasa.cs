﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using MySql.Data.MySqlClient;
namespace biblioteka_rad
{
    class KnjigaKlasa
    {
        public static int brojacZaListuKnjiga;
        string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }
        public string oblast { get; private set; }
        public int brojStrana { get; private set; }
        public string tip { get; private set; }
        public string vez { get; private set; }
        public string imeIzdavaca { get; private set; }

        public string ImeIzdavaca
        {
            get { return imeIzdavaca; }
            set { imeIzdavaca = value; }
        }
        bool praznaPolja;
        public List<KnjigeStavkeKlasa> ListaKnjigaStavke;
        public List<AutoriStavkeKlasa> ListaAutoraStavke;
        public KnjigaKlasa()
        {
            ListaKnjigaStavke = new List<KnjigeStavkeKlasa>();
            ListaAutoraStavke = new List<AutoriStavkeKlasa>();
        }

        public KnjigaKlasa(string nazivKnjige, string oblastKnjige, string brojStrana, string tipKnjige, string vezKnjige, string izdavac, string str)
        {
            this.naziv = nazivKnjige; this.oblast = oblastKnjige; this.brojStrana = int.Parse(brojStrana); this.tip = tipKnjige; this.vez = vezKnjige;
            this.imeIzdavaca = izdavac;
            ListaKnjigaStavke = new List<KnjigeStavkeKlasa>();
            ListaAutoraStavke = new List<AutoriStavkeKlasa>();

        }
        public KnjigaKlasa(ComboBox nazivKnjige, ComboBox oblastKnjige, TextBox brojStrana, ComboBox tipKnjige, ComboBox vezKnjige, TextBox izdavac)
        {
            if (String.IsNullOrEmpty(nazivKnjige.Text) || String.IsNullOrEmpty(oblastKnjige.Text) ||
               String.IsNullOrEmpty(brojStrana.Text) || String.IsNullOrEmpty(tipKnjige.Text) || String.IsNullOrEmpty(vezKnjige.Text))
            {
                praznaPolja = true;
                MessageBox.Show("Polja za knjigu trebaju biti popunjena");
            }
            else
            {
                praznaPolja = false;
                this.naziv = nazivKnjige.Text;
                this.oblast = oblastKnjige.Text;
                this.brojStrana = int.Parse(brojStrana.Text);
                this.tip = tipKnjige.Text;
                this.vez = vezKnjige.Text;
                this.imeIzdavaca = izdavac.Text;
                List<KnjigeStavkeKlasa> ListaKnjigaStavke = new List<KnjigeStavkeKlasa>();
                List<AutoriStavkeKlasa> ListaAutoraStavke = new List<AutoriStavkeKlasa>();
            }
        }
        public bool KnjigaPoljaUnetaOK()
        {
            if (praznaPolja == true)
            { return false; }
            else
            { return true; }
        }
        public void KonekcijaUnosKnjigeNaslov()
        {
            string konekcijaStr = "server=localhost; uid=root; password=; database=biblioteka;";
            using (MySqlConnection konekcija = new MySqlConnection(konekcijaStr))
            {
                konekcija.Open();
                string pronadjiIDizdavacaStr = "select izdavac_id from izdavac where " +
                    " nazivIzdavaca = @nazivIzdavaca;";
                using (MySqlCommand komandatrazimIdIzdavaca = new MySqlCommand(pronadjiIDizdavacaStr, konekcija))
                {
                    komandatrazimIdIzdavaca.Parameters.AddWithValue("@nazivIzdavaca", this.imeIzdavaca);
                    MySqlDataReader citac = komandatrazimIdIzdavaca.ExecuteReader();
                    string izdavacID="";
                    while (citac.Read())
                    {
                        izdavacID = citac.GetString(0);
                    }
                    citac.Close();
                    string unosKnjigeNaslovStr = "insert into knjiga(nazivKnjige, oblast, brojStrana, tip, vezKoji, izdavac_izdavac_id) " +
                        "values (@nazivKnjige, @oblast, @brojStrana, @tip, @vezKoji, @izdavacID);";
                    using (MySqlCommand komandaUnosaKnjigeNaslov = new MySqlCommand(unosKnjigeNaslovStr, konekcija))
                    {
                        komandaUnosaKnjigeNaslov.Parameters.AddWithValue("@nazivKnjige", this.naziv);
                        komandaUnosaKnjigeNaslov.Parameters.AddWithValue("@oblast", this.oblast);
                        komandaUnosaKnjigeNaslov.Parameters.AddWithValue("@brojStrana", this.brojStrana);
                        komandaUnosaKnjigeNaslov.Parameters.AddWithValue("@tip", this.tip);
                        komandaUnosaKnjigeNaslov.Parameters.AddWithValue("@vezKoji", this.vez);
                        komandaUnosaKnjigeNaslov.Parameters.AddWithValue("@izdavacID", izdavacID);
                        komandaUnosaKnjigeNaslov.ExecuteNonQuery();
                    }
                }
            }
        }



        public static void FiksniKomboBoxovi(ComboBox oblastKnjige, ComboBox tipKnjige, ComboBox vezKnjige, ComboBox slobodnaIliNe, 
            ComboBox ostecena)
        {
            oblastKnjige.Items.Clear(); tipKnjige.Items.Clear(); vezKnjige.Items.Clear(); slobodnaIliNe.Items.Clear();
            oblastKnjige.Items.Add("Naucna fantastika"); oblastKnjige.Items.Add("Poezija"); oblastKnjige.Items.Add("Strucna literatura");
            oblastKnjige.Items.Add("Ljubavni romani"); oblastKnjige.Items.Add("Drama"); oblastKnjige.Items.Add("Auto biografija");

            tipKnjige.Items.Add("Klasicna"); tipKnjige.Items.Add("CD");
            vezKnjige.Items.Add("Tvrdi vez"); vezKnjige.Items.Add("Meki vez");
            slobodnaIliNe.Items.Add("Slobodna"); slobodnaIliNe.Items.Add("Zauzeta");
            ostecena.Items.Add("Ostecena"); ostecena.Items.Add("Malo ostecena"); ostecena.Items.Add("Neostecena");
            ostecena.Text = "Neostecena";
            slobodnaIliNe.Text = "Slobodna";
            vezKnjige.Text = "Meki vez";
            tipKnjige.Text = "Klasicna";
            oblastKnjige.Text = "Strucna literatura";
        }
        
        public static DataGridView KnjigaPrebacivanjeRekordaDesno(ComboBox nazivKnjige, ComboBox oblast, TextBox brStrana,  
            ComboBox tip, ComboBox vez, List<KnjigaKlasa>listaKnjigaU, DataGridView gridAutori, DataGridView gridKnjige)
        {
            gridAutori.DataSource = null; gridAutori.Rows.Clear();
            gridKnjige.DataSource = null; gridKnjige.Rows.Clear();
            KnjigaKlasa.brojacZaListuKnjiga++;
            if(KnjigaKlasa.brojacZaListuKnjiga >= listaKnjigaU.Count){KnjigaKlasa.brojacZaListuKnjiga = 0;}
            int index = KnjigaKlasa.brojacZaListuKnjiga;
            nazivKnjige.Text = listaKnjigaU[index].naziv; oblast.Text = listaKnjigaU[index].oblast;
            brStrana.Text = listaKnjigaU[index].brojStrana.ToString(); tip.Text = listaKnjigaU[index].tip; vez.Text = listaKnjigaU[index].vez;
            return gridAutori;

        }//kraj metode rekord desno
        
        public static void KnjigaPrebacivanjeRekordaLevo(ComboBox nazivKnjige, ComboBox oblast, TextBox brStrana,
            ComboBox tip, ComboBox vez, List<KnjigaKlasa> listaKnjigaU, DataGridView gridAutori, DataGridView gridKnjige)
        {
            gridAutori.DataSource = null; gridAutori.Rows.Clear();
            gridKnjige.DataSource = null; gridKnjige.Rows.Clear();
            KnjigaKlasa.brojacZaListuKnjiga--;
            if (KnjigaKlasa.brojacZaListuKnjiga < 0) { KnjigaKlasa.brojacZaListuKnjiga = listaKnjigaU.Count-1; }
            int index = KnjigaKlasa.brojacZaListuKnjiga;
            nazivKnjige.Text = listaKnjigaU[index].naziv; oblast.Text = listaKnjigaU[index].oblast;
            brStrana.Text = listaKnjigaU[index].brojStrana.ToString(); tip.Text = listaKnjigaU[index].tip; vez.Text = listaKnjigaU[index].vez;
            

        }//kraj metode rekord levo

        public static void BogacenjeKomboBoxaNaPromenutaba(TextBox nazivIzdavaca, ComboBox knjigaPadajucaLista, ComboBox autoriPadajucaLista)
        {
            try
            {

                knjigaPadajucaLista.Items.Clear();
                autoriPadajucaLista.Items.Clear();
                using (MySqlConnection konekcija = new MySqlConnection("server=localhost; uid=root; password=;database=biblioteka;"))
                {
                    konekcija.Open();
                    using (MySqlCommand komandaObogatiNaziv = new MySqlCommand())
                    {
                        MySqlCommand komandaNadjiForgenKnjige = new MySqlCommand();
                        komandaNadjiForgenKnjige.Connection = konekcija;
                        komandaNadjiForgenKnjige.CommandText = "select izdavac_id from izdavac where nazivIzdavaca = @nazivIzd;";
                        komandaNadjiForgenKnjige.Parameters.AddWithValue("@nazivIzd", nazivIzdavaca.Text);
                        MySqlDataReader citac = komandaNadjiForgenKnjige.ExecuteReader();
                        int brojPoklapanja = 0;
                        string forgenK = "";
                        while (citac.Read())
                        {
                            forgenK = citac.GetString(0);
                            brojPoklapanja++;
                        }
                        citac.Close();
                        if (brojPoklapanja > 0)
                        {
                            komandaObogatiNaziv.Connection = konekcija;
                            komandaObogatiNaziv.CommandText = "select nazivKnjige from knjiga, izdavac where izdavac_izdavac_id = @spoljniK && izdavac_id = izdavac_izdavac_id;";
                            komandaObogatiNaziv.Parameters.AddWithValue("@spoljniK", forgenK);
                            citac = komandaObogatiNaziv.ExecuteReader();
                            while (citac.Read())
                            {
                                knjigaPadajucaLista.Items.Add(citac.GetString(0));
                            }
                            citac.Close();
                        }
                    }
                    using (MySqlCommand komandaObogatiAutora = new MySqlCommand())
                    {
                        komandaObogatiAutora.Connection = konekcija;
                        komandaObogatiAutora.CommandText = "select imePrezimeAutora from autor;";
                        MySqlDataReader citac = komandaObogatiAutora.ExecuteReader();
                        while (citac.Read())
                        {
                            autoriPadajucaLista.Items.Add(citac.GetString(0));
                        }
                        citac.Close();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }//krajBogacenjaKomboboxa
        public static void PopunjavanjeListeKnjiga(List<KnjigaKlasa>ListaKnjiga, TextBox imeIzdavaca)
        {
            try
            {
                string konekcijaStr = "server=localhost; uid=root; password=; database=biblioteka;";
                MySqlConnection konekcija = new MySqlConnection(konekcijaStr); konekcija.Open();
                MySqlCommand komandaSveKnjige = new MySqlCommand();
                komandaSveKnjige.Connection = konekcija;
                komandaSveKnjige.CommandText = "select * from knjiga;";
                MySqlDataReader citac = komandaSveKnjige.ExecuteReader();
                int brojKnjiga = 0;
                while (citac.Read())
                {
                    brojKnjiga++;
                }
                citac.Close();
                if (brojKnjiga > 0)
                {
                    citac = komandaSveKnjige.ExecuteReader();

                    List<tempKlasa> listaTemp = new List<tempKlasa>();
                    while (citac.Read())
                    {
                        tempKlasa temp1a = new tempKlasa(citac.GetString(1), citac.GetString(2), citac.GetString(3),
                            citac.GetString(4), citac.GetString(5), citac.GetString(6));
                        listaTemp.Add(temp1a);
                    }
                    citac.Close();
                    for (int m = 0; m < listaTemp.Count; m++)
                    {
                        MySqlCommand komandaNadjiIzdavacImeNaOnsovuKljuca = new MySqlCommand(); komandaNadjiIzdavacImeNaOnsovuKljuca.Connection = konekcija;
                        komandaNadjiIzdavacImeNaOnsovuKljuca.CommandText = "select nazivIzdavaca from izdavac where izdavac_id = @izdavacevKljuc;";
                        komandaNadjiIzdavacImeNaOnsovuKljuca.Parameters.AddWithValue("@izdavacevKljuc", listaTemp[m].IdIzdavaca);
                        MySqlDataReader citacSamoZaImeIzdavaca = komandaNadjiIzdavacImeNaOnsovuKljuca.ExecuteReader();
                        string imeIzdavacaDobijenIzTemp = "";
                        while (citacSamoZaImeIzdavaca.Read())
                        {
                            imeIzdavacaDobijenIzTemp = citacSamoZaImeIzdavaca.GetString(0);
                        }
                        citacSamoZaImeIzdavaca.Close();
                        string nazivTemp = listaTemp[m].Naziv; string oblastTemp = listaTemp[m].Oblast; string brojStranaTemp = listaTemp[m].BrojStrana;
                        string tipTemp = listaTemp[m].Tip; string vezTemp = listaTemp[m].Vez; string nazivIzdavacatemp = imeIzdavacaDobijenIzTemp;
                        KnjigaKlasa knjiga12 = new KnjigaKlasa(nazivTemp, oblastTemp, brojStranaTemp, tipTemp, vezTemp, nazivIzdavacatemp, "str");
                        ListaKnjiga.Add(knjiga12);
                        

                    }
                    for (int i = 0; i < ListaKnjiga.Count; i++)
                    {
                        string dajKljucKnjigeStr = "select knjiga_id from knjiga where nazivKnjige = @nazivKnjige;";
                        MySqlCommand komandaDajKljucKjnige = new MySqlCommand(dajKljucKnjigeStr, konekcija);
                        komandaDajKljucKjnige.Parameters.AddWithValue("@nazivKnjige", ListaKnjiga[i].Naziv);

                        citac = komandaDajKljucKjnige.ExecuteReader();
                        int brojKljucevaKnjige = 0; 
                        while (citac.Read())
                        {
                            brojKljucevaKnjige++;
                        }
                        citac.Close();
                        if (brojKljucevaKnjige > 0)
                        {
                            citac = komandaDajKljucKjnige.ExecuteReader();
                            string kljucevi = "";
                            while (citac.Read())
                            {
                                kljucevi = citac.GetString(0);
                            }
                            citac.Close();

                            MySqlCommand komandaPrimarniKljuc = new MySqlCommand(); komandaPrimarniKljuc.Connection = konekcija;
                            komandaPrimarniKljuc.CommandText = "select autori_id from autori where knjiga_knjiga_id =@knjigaKnjigaID;";
                            komandaPrimarniKljuc.Parameters.AddWithValue("@knjigaKnjigaID", kljucevi);
                            List<string> primarniKljucevi = new List<string>();
                            citac = komandaPrimarniKljuc.ExecuteReader();
                            while (citac.Read()) { primarniKljucevi.Add(citac.GetString(0)); } citac.Close();
                            for (int k = 0; k < primarniKljucevi.Count; k++)
                            {
                                MySqlCommand komandaSpoljniOba = new MySqlCommand(); komandaSpoljniOba.Connection = konekcija;
                                komandaSpoljniOba.CommandText = "select knjiga_knjiga_id, autor_autor_id from autori where autori_id = @kljucOvde;";
                                komandaSpoljniOba.Parameters.AddWithValue("@kljucOvde", primarniKljucevi[k]);/////////////////
                                string knjigaknjiga = ""; string autorautor = "";
                                citac = komandaSpoljniOba.ExecuteReader();
                                while (citac.Read()) { knjigaknjiga = citac.GetString(0); autorautor = citac.GetString(1); } citac.Close();

                                MySqlCommand komandaDajMiAutora = new MySqlCommand(); komandaDajMiAutora.Connection = konekcija;
                                komandaDajMiAutora.CommandText = "select imePrezimeAutora from autor where autor_id=@autorIDe;";
                                komandaDajMiAutora.Parameters.AddWithValue("@autorIDe", autorautor);
                                string autorRezultat = "";
                                citac = komandaDajMiAutora.ExecuteReader();
                                while (citac.Read()) { autorRezultat = citac.GetString(0); } citac.Close();

                                MySqlCommand komandaDajMiKnjigu = new MySqlCommand(); komandaDajMiKnjigu.Connection = konekcija;
                                komandaDajMiKnjigu.CommandText = "select nazivKnjige from knjiga where knjiga_id=@knjigaKnjiga;";
                                komandaDajMiKnjigu.Parameters.AddWithValue("@knjigaKnjiga", knjigaknjiga);

                                string knjigaRezultat = "";
                                citac = komandaDajMiKnjigu.ExecuteReader();
                                while (citac.Read()) { knjigaRezultat = citac.GetString(0); } citac.Close();

                                AutoriStavkeKlasa a1 = new AutoriStavkeKlasa(knjigaRezultat, autorRezultat, "string");
                                ListaKnjiga[i].ListaAutoraStavke.Add(a1);

                            }

                        }
                        //kraj dodavanja autori

                        if (brojKljucevaKnjige > 0)
                        {
                            citac = komandaDajKljucKjnige.ExecuteReader();
                            string kljuceviKnjige = "";
                            while (citac.Read())
                            {
                                kljuceviKnjige = citac.GetString(0);
                            }
                            citac.Close();
                            MySqlCommand kljuceviKnjigeStavke = new MySqlCommand(); kljuceviKnjigeStavke.Connection = konekcija;
                            kljuceviKnjigeStavke.CommandText = "select knjige_id from knjige where knjiga_knjiga_id = @knjigaknjiga_id;";
                            kljuceviKnjigeStavke.Parameters.AddWithValue("knjigaknjiga_id", kljuceviKnjige);
                            citac = kljuceviKnjigeStavke.ExecuteReader();
                            int brojPrimarnigKljucevaKnjigeStavke = 0;

                            List<string> primarniKljuceviStavkeKnjige = new List<string>();

                            while (citac.Read())
                            {
                                primarniKljuceviStavkeKnjige.Add(citac.GetString(0));
                                brojPrimarnigKljucevaKnjigeStavke++;
                            }
                            citac.Close();
                            if (brojPrimarnigKljucevaKnjigeStavke > 0)
                            {
                                for (int z = 0; z < primarniKljuceviStavkeKnjige.Count; z++)
                                {
                                    MySqlCommand komandaSveIzStavkeKnjiga = new MySqlCommand(); komandaSveIzStavkeKnjiga.Connection = konekcija;
                                    komandaSveIzStavkeKnjiga.CommandText = "select * from knjige where knjige_id = @primarniKnjige;";
                                    komandaSveIzStavkeKnjiga.Parameters.AddWithValue("@primarniKnjige", primarniKljuceviStavkeKnjige[z]);
                                    citac = komandaSveIzStavkeKnjiga.ExecuteReader();
                                    KnjigeStavkeKlasa knjigeStavkeklasaOBJ = null;
                                    while (citac.Read())
                                    {
                                        string indetifikatorStr = citac.GetString(1); string slobodnaZauzetaStr = citac.GetString(2);
                                        string ostecenaStr = citac.GetString(3); string mestoStr = citac.GetString(4);
                                        string naslovKnjigeStr = citac.GetString(5);
                                        knjigeStavkeklasaOBJ = new KnjigeStavkeKlasa(indetifikatorStr, slobodnaZauzetaStr, ostecenaStr,
                                            mestoStr, naslovKnjigeStr, "string");
                                    }
                                    citac.Close();
                                    ListaKnjiga[i].ListaKnjigaStavke.Add(knjigeStavkeklasaOBJ);
                                }
                            }
                        }


                    }//kraj petlje

                }
                konekcija.Close();
            }
            catch (Exception ex) 
            { 
                MessageBox.Show(ex.Message); 
            }
            
        }//kraj metode unos knjiga

        public static List<KnjigaKlasa> UmanjenjeListeKnjigeZaSamoJednogIzdavaca(List<KnjigaKlasa> listaKnjiga, TextBox nazivIzdavacTab)
        {
            try
            {
                for (int i = 0; i < listaKnjiga.Count; i++)
                {
                    if (listaKnjiga[i].ImeIzdavaca != nazivIzdavacTab.Text)
                    {
                        listaKnjiga[i] = null;
                    }
                }
                List<KnjigaKlasa> trenutnaLista = new List<KnjigaKlasa>();
                for (int i = 0; i < listaKnjiga.Count; i++)
                {
                    if (listaKnjiga[i] != null)
                    {
                        trenutnaLista.Add(listaKnjiga[i]);
                    }
                }
                listaKnjiga = null; listaKnjiga = new List<KnjigaKlasa>();
                for (int i = 0; i < trenutnaLista.Count; i++)
                {
                    listaKnjiga.Add(trenutnaLista[i]);
                }
                
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return listaKnjiga;
        }
        //kraj metode smanjenje liste
        public static void PromenaNaKomboBoxuKnjige(ComboBox komboIzaberiKnjigu, ComboBox oblast, TextBox brStrana, List<KnjigaKlasa>listaKnjiga, 
            ComboBox tip, ComboBox vez, DataGridView gridAutori, DataGridView gridKnjige, KnjigaKlasa knjig, string jelDodat)
        {
            try
            {
                if (jelDodat == "dodat")
                {
                    komboIzaberiKnjigu.Items.Add(knjig.Naziv);
                }
                int brojac = 0;
                int redniBroj = 0;
                foreach (var v in listaKnjiga)
                {
                    if (v.Naziv == komboIzaberiKnjigu.Text)
                    {
                        redniBroj = brojac;
                    }
                    brojac++;
                }
                oblast.Text = listaKnjiga[redniBroj].oblast; brStrana.Text = listaKnjiga[redniBroj].brojStrana.ToString();
                tip.Text = listaKnjiga[redniBroj].tip; vez.Text = listaKnjiga[redniBroj].vez;
                gridAutori.DataSource = null;
                gridAutori.Rows.Clear();
                if (listaKnjiga[redniBroj].ListaAutoraStavke.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Autor"); dt.Columns.Add("Knjiga");
                    for (int i = 0; i < listaKnjiga[redniBroj].ListaAutoraStavke.Count; i++)
                    {
                        dt.Rows.Add(listaKnjiga[redniBroj].ListaAutoraStavke[i].ImeAutora, listaKnjiga[redniBroj].ListaAutoraStavke[i].ImeKnjige);
                    }
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; gridAutori.DataSource = bs;
                    gridAutori.AutoResizeColumns();
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Autor"); dt.Columns.Add("Knjiga");
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; gridAutori.DataSource = bs;
                    gridAutori.Columns[0].Width = gridAutori.Width / 2; gridAutori.Columns[1].Width = gridAutori.Width / 2 -50;
                }


                gridKnjige.DataSource = null;
                gridKnjige.Rows.Clear();
                if (listaKnjiga[redniBroj].ListaKnjigaStavke.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Indetifikator"); dt.Columns.Add("Slobodna?"); dt.Columns.Add("Ostecena"); dt.Columns.Add("Mesto");
                    dt.Columns.Add("Knjiga");
                    for (int i = 0; i < listaKnjiga[redniBroj].ListaKnjigaStavke.Count; i++)
                    {
                        dt.Rows.Add(listaKnjiga[redniBroj].ListaKnjigaStavke[i].Indetifikator, listaKnjiga[redniBroj].ListaKnjigaStavke[i].SlobodnaZauzeta,
                        listaKnjiga[redniBroj].ListaKnjigaStavke[i].OstecenaNeostecena, listaKnjiga[redniBroj].ListaKnjigaStavke[i].MestoKnjige,
                        listaKnjiga[redniBroj].ListaKnjigaStavke[i].ImeKnjigeNaslov);
                    }
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; gridKnjige.DataSource = bs;
                    gridKnjige.AutoResizeColumns();
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Indetifikator"); dt.Columns.Add("Slobodna?"); dt.Columns.Add("Ostecena"); dt.Columns.Add("Mesto");
                    dt.Columns.Add("Knjiga");
                    BindingSource bs = new BindingSource(); bs.DataSource = dt; gridKnjige.DataSource = bs;
                    gridKnjige.Columns[0].Width = gridKnjige.Width / 4 - 11; gridKnjige.Columns[1].Width = gridKnjige.Width / 4 - 11;
                    gridKnjige.Columns[2].Width = gridKnjige.Width / 4 - 11; gridKnjige.Columns[3].Width = gridKnjige.Width / 4 - 11;
                }
            }

            catch (Exception ex) { MessageBox.Show(ex.Message + " promena kombo boxa"); }
            
        }
    }
}
