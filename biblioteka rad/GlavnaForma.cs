﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmGlavnaForma : Form
    {
        public frmGlavnaForma()
        {
            InitializeComponent();
        }

        private void UpisStudentaOtvori(object sender, EventArgs e)
        {
            frmStudent f1 = new frmStudent();
            f1.ShowDialog();
        }

        private void btnUpisBibliotekara_Click(object sender, EventArgs e)
        {
            frmBibliotekarUnos b1 = new frmBibliotekarUnos();
            b1.ShowDialog();
        }

        private void btnUpisAutora_Click(object sender, EventArgs e)
        {
            frmAutor a1 = new frmAutor();
            a1.ShowDialog();
        }

        private void btnIzdavajneVracanje_Click(object sender, EventArgs e)
        {
            frmIzdatoVracenoForma i1 = new frmIzdatoVracenoForma();
            i1.ShowDialog();
        }

        private void btnIzdavacKnjiga_Click(object sender, EventArgs e)
        {
            frmIzdavacKnjiga ik = new frmIzdavacKnjiga();
            ik.ShowDialog();
        }

        private void btnSipleSviStudenti_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni
                = new frmPreglediJednostavniUpiti("svi studenti");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleSviAutori_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("svi upisani autori");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleSveKnjige_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("sve knjige");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleSviPrimerciKnjiga_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("svi primerci knjiga");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleSviSlobodniPrimerciKnjiga_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("svi slobodni primerci knjiga");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleSviZauzetiPrimerciKnjiga_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("svi zauzeti primerci knjiga");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleSviOsteceniPrimerciKnjiga_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("svi osteceni primerci knjiga");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleSpisakIkadaIzdatihKnjiga_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("spisak ikada izdatih knjiga");
            pregledJednostavni.ShowDialog();
        }

        private void btnSimpleProsecnaOcenaSvihKnjiga_Click(object sender, EventArgs e)
        {
            frmPreglediJednostavniUpiti pregledJednostavni = new frmPreglediJednostavniUpiti("prosecna ocena za svaku knjigu");
            pregledJednostavni.ShowDialog();
        }

        private void btnNadjiStudenta_Click(object sender, EventArgs e)
        {
            frmPitanjeZaParametarskiUpit pzpi = new frmPitanjeZaParametarskiUpit("Pronadji studenta na osnovu bilo koje informacije", "Prikazi", "pronadji studenta");
            pzpi.ShowDialog();

        }

        private void btnPronadjiAutora_Click(object sender, EventArgs e)
        {
            frmPitanjeZaParametarskiUpit pzpi = new frmPitanjeZaParametarskiUpit("Pronadji autora na osnovu bilo koje informacije", "Prikazi", "pronadji autora");
            pzpi.ShowDialog();
        }

        private void btnKnjigaPoBiloKomOsnovu_Click(object sender, EventArgs e)
        {
            frmPitanjeZaParametarskiUpit pzpi = 
                new frmPitanjeZaParametarskiUpit("Pronadji knjigu na osnovu bilo koje informacije", 
                    "Prikazi", "pronadji naslov");
            pzpi.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmPitanjeZaParametarskiUpit pzu = new frmPitanjeZaParametarskiUpit("Uneti broj indeksa studenta", "Prikazi", "spisak");
            pzu.ShowDialog(); 
        }
    }
}
