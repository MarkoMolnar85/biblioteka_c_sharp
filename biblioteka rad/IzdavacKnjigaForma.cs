﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace biblioteka_rad
{
    public partial class frmIzdavacKnjiga : Form
    {
        List<IzdavacKlasa> ListaIzdavaca;
        List<KnjigaKlasa> ListaKnjigaNaslovi;
        public frmIzdavacKnjiga()
        {
            InitializeComponent();
            IzdavacKlasa.FiksniKomboBoxOcena(cmbOcena);
        }

        private void btnUnesiNovogIzdavaca_Click(object sender, EventArgs e)
        {
            try
            {
                IzdavacKlasa i1 = new IzdavacKlasa(cmbNaziv, txtDrzava, txtGrad, txtPostanski, txtBrojTel, txtEmail, cmbOcena);
                if (i1.PopunjenaPoljaOK())
                {
                    if (i1.ProveraOceneOK(cmbOcena))
                    {
                        i1.KonekcijaUnosIzdavaca(ListaIzdavaca);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void LoadPokretanjeForme(object sender, EventArgs e)
        {
            try
            {
                ListaIzdavaca = new List<IzdavacKlasa>();
                IzdavacKlasa.PopunjavanjeListe(ListaIzdavaca);
                for (int i = 0; i < ListaIzdavaca.Count; i++)
                {
                    cmbNaziv.Items.Add(ListaIzdavaca[i].Naziv);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void izdavacRekordLevo(object sender, EventArgs e)
        {
            try
            {
                IzdavacKlasa.Rekordlevo(cmbNaziv, txtDrzava, txtGrad, txtPostanski, txtBrojTel, txtEmail, cmbOcena, ListaIzdavaca);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void IzdavacRekordDesno(object sender, EventArgs e)
        {
            try
            {
                IzdavacKlasa.RekorDesno(cmbNaziv, txtDrzava, txtGrad, txtPostanski, txtBrojTel, txtEmail, cmbOcena, ListaIzdavaca);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnNoviZapis_Click(object sender, EventArgs e) //novi zapis za izdavaca
        {
            try
            {
                IzdavacKlasa.CiscenjeZaUpisNovog(cmbNaziv, txtDrzava, txtGrad, txtPostanski, txtBrojTel, txtEmail, cmbOcena);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void PormenaKomboBoxaIzdavac(object sender, EventArgs e)
        {
            try
            {
                IzdavacKlasa.PopunjavanjeNaPromenu(cmbNaziv, txtDrzava, txtGrad, txtPostanski, txtBrojTel, txtEmail, cmbOcena, ListaIzdavaca);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void UpdateTextaKomboBoxa(object sender, EventArgs e)
        {
            try
            {
                IzdavacKlasa.PromenaTextaNaKomboBoxu(cmbNaziv, ListaIzdavaca);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        //knjiga metode su ispod
        private void PromenaTaba(object sender, EventArgs e)
        {
            txtIzdavacKnjigaTab.Enabled = false;
            try
            {
                if (tabKontrola.SelectedTab.ToString() == tabKontrola.Controls[1].ToString())
                {
                    if (!String.IsNullOrEmpty(cmbNaziv.Text))
                    {
                        KnjigaKlasa.brojacZaListuKnjiga = 0;
                        txtIzdavacKnjigaTab.Visible = cmbNazivKnjige.Visible = cmbOblastKnjige.Visible = txtBrojStranaKnjige.Visible = true;
                        cmbTipKnjige.Visible = cmbVezKnjige.Visible = gridAutori.Visible = gridKnjige.Visible = true;
                        cmbUnosAutoraKoautora.Visible = btnUnesiAutore.Visible = btnKnjigaRekordLevo.Visible = btnKnjigaRekordDesno.Visible = true;
                        btnUnos.Visible = btnNoviRekordUpisKnjiga.Visible = txtIndetifikatorPosebneKnjige.Visible = true;
                        cmbSlobodnaZauzeta.Visible = cmbOstecenaNeostecena.Visible = txtMestoKnjige.Visible = btnUnesiKnjigeStavke.Visible = true;

                        cmbNazivKnjige.Text = "";
                        cmbOblastKnjige.Text = "Strucna literatura"; txtBrojStranaKnjige.Clear(); cmbTipKnjige.Text = "Klasicna";
                        cmbVezKnjige.Text = "Meki vez"; cmbUnosAutoraKoautora.Text = ""; txtIndetifikatorPosebneKnjige.Clear();
                        cmbSlobodnaZauzeta.Text = "Slobodna"; cmbOstecenaNeostecena.Text = "Neostecena"; txtMestoKnjige.Clear();

                        
                        txtIzdavacKnjigaTab.Text = cmbNaziv.Text;
                        KnjigaKlasa.FiksniKomboBoxovi(cmbOblastKnjige, cmbTipKnjige, cmbVezKnjige, cmbSlobodnaZauzeta, cmbOstecenaNeostecena);
                        ListaKnjigaNaslovi = new List<KnjigaKlasa>();
                        
                        KnjigaKlasa.PopunjavanjeListeKnjiga(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);

                        ListaKnjigaNaslovi = KnjigaKlasa.UmanjenjeListeKnjigeZaSamoJednogIzdavaca(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);
                        KnjigaKlasa.BogacenjeKomboBoxaNaPromenutaba(txtIzdavacKnjigaTab, cmbNazivKnjige, cmbUnosAutoraKoautora);
                        KreiranjePraznogGrida();
                        
                    }
                    else
                    {
                        KnjigaKlasa.brojacZaListuKnjiga = 0;
                        txtIzdavacKnjigaTab.Visible = cmbNazivKnjige.Visible = cmbOblastKnjige.Visible = txtBrojStranaKnjige.Visible = false;
                        cmbTipKnjige.Visible = cmbVezKnjige.Visible = gridAutori.Visible = gridKnjige.Visible = false;
                        cmbUnosAutoraKoautora.Visible = btnUnesiAutore.Visible = btnKnjigaRekordLevo.Visible = btnKnjigaRekordDesno.Visible = false;
                        btnUnos.Visible = btnNoviRekordUpisKnjiga.Visible = txtIndetifikatorPosebneKnjige.Visible = false;
                        cmbSlobodnaZauzeta.Visible = cmbOstecenaNeostecena.Visible = txtMestoKnjige.Visible = btnUnesiKnjigeStavke.Visible = false;

                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }//kraj promene taba

        private void btnUnesiAutore_Click(object sender, EventArgs e)
        {
            try
            {
                AutoriStavkeKlasa a1 = new AutoriStavkeKlasa(cmbNazivKnjige, cmbUnosAutoraKoautora);
                if (a1.AutorStavkaPoljaUnetaOK())
                {
                    //
                    KnjigaKlasa.brojacZaListuKnjiga = 0;
                    txtIzdavacKnjigaTab.Text = cmbNaziv.Text;
                    ListaKnjigaNaslovi = new List<KnjigaKlasa>();

                    KnjigaKlasa.PopunjavanjeListeKnjiga(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);

                    ListaKnjigaNaslovi = KnjigaKlasa.UmanjenjeListeKnjigeZaSamoJednogIzdavaca(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);
                    KnjigaKlasa.BogacenjeKomboBoxaNaPromenutaba(txtIzdavacKnjigaTab, cmbNazivKnjige, cmbUnosAutoraKoautora);
                    int redniZaListuAutoraUknjizi = 0; //nisam siguran
                    for (int i = 0; i < ListaKnjigaNaslovi.Count; i++)
                    {
                        if (ListaKnjigaNaslovi[i].Naziv == cmbNazivKnjige.Text)
                        {
                            redniZaListuAutoraUknjizi = i;
                        }
                    }
                    ListaKnjigaNaslovi[redniZaListuAutoraUknjizi].ListaAutoraStavke.Add(a1);
                    
                    a1.KonekcijaUnosAutoraStavke();
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Pisac"); dt.Columns.Add("Knjiga");
                    for (int i = 0; i < 1; i++)
                    {
                        for (int j = 0; j < ListaKnjigaNaslovi[redniZaListuAutoraUknjizi].ListaAutoraStavke.Count; j++)
                        {
                            dt.Rows.Add(ListaKnjigaNaslovi[redniZaListuAutoraUknjizi].ListaAutoraStavke[j].ImeAutora, 
                                ListaKnjigaNaslovi[redniZaListuAutoraUknjizi].ListaAutoraStavke[j].ImeKnjige);
                        }
                    }
                    BindingSource bs = new BindingSource();
                    bs.DataSource = dt;
                    gridAutori.DataSource = bs;
                    DataGridViewColumn kolona0 = gridAutori.Columns[0]; kolona0.Width = 150;
                    DataGridViewColumn kolona1 = gridAutori.Columns[1]; kolona1.Width = 145;
                    if (gridAutori.Rows.Count > 5)
                    {
                        gridAutori.Width = 320;
                    }
                    
                    
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            
        }

        private void UnosKnjigeNovoIme(object sender, EventArgs e)
        {
            try
            {          
                KnjigaKlasa k1 = new KnjigaKlasa(cmbNazivKnjige, cmbOblastKnjige, txtBrojStranaKnjige, cmbTipKnjige, cmbVezKnjige, txtIzdavacKnjigaTab);
                if (k1.KnjigaPoljaUnetaOK())
                {
                    KnjigaKlasa.brojacZaListuKnjiga = 0;
                    txtIzdavacKnjigaTab.Text = cmbNaziv.Text;
                    ListaKnjigaNaslovi = new List<KnjigaKlasa>();

                    KnjigaKlasa.PopunjavanjeListeKnjiga(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);

                    ListaKnjigaNaslovi = KnjigaKlasa.UmanjenjeListeKnjigeZaSamoJednogIzdavaca(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);
                    KnjigaKlasa.BogacenjeKomboBoxaNaPromenutaba(txtIzdavacKnjigaTab, cmbNazivKnjige, cmbUnosAutoraKoautora);
                    KreiranjePraznogGrida();
                    
                    ListaKnjigaNaslovi.Add(k1);
                    k1.KonekcijaUnosKnjigeNaslov();
     
                }     
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void UpisViseKnjiga(object sender, EventArgs e)
        {
            try
            {
                KnjigeStavkeKlasa knjigeStavke = new KnjigeStavkeKlasa(txtIndetifikatorPosebneKnjige, cmbSlobodnaZauzeta, cmbOstecenaNeostecena,
                    txtMestoKnjige, cmbNazivKnjige);
                if (knjigeStavke.KnjigeStavkaUnetaPoljaOK())
                {
                    //
                    KnjigaKlasa.brojacZaListuKnjiga = 0;
                    txtIzdavacKnjigaTab.Text = cmbNaziv.Text;
                    ListaKnjigaNaslovi = new List<KnjigaKlasa>();
                    KnjigaKlasa.PopunjavanjeListeKnjiga(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);

                    ListaKnjigaNaslovi = KnjigaKlasa.UmanjenjeListeKnjigeZaSamoJednogIzdavaca(ListaKnjigaNaslovi, txtIzdavacKnjigaTab);
                    KnjigaKlasa.BogacenjeKomboBoxaNaPromenutaba(txtIzdavacKnjigaTab, cmbNazivKnjige, cmbUnosAutoraKoautora);
                    int rbLista = 0;
                    for (int i = 0; i < ListaKnjigaNaslovi.Count; i++)
                    {
                        if (ListaKnjigaNaslovi[i].Naziv == knjigeStavke.ImeKnjigeNaslov)
                        {
                            rbLista = i;
                        }
                    }

                    ListaKnjigaNaslovi[rbLista].ListaKnjigaStavke.Add(knjigeStavke);
                    knjigeStavke.KonekcijaUpis();

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Indetifikator"); dt.Columns.Add("Slobodna?"); 
                    dt.Columns.Add("Ostecena?"); dt.Columns.Add("Mesto"); 
                    for (int i = 0; i < 1; i++)
                    {
                        for (int j = 0; j < ListaKnjigaNaslovi[rbLista].ListaKnjigaStavke.Count; j++)
                        {
                            dt.Rows.Add(ListaKnjigaNaslovi[rbLista].ListaKnjigaStavke[j].Indetifikator,
                                ListaKnjigaNaslovi[rbLista].ListaKnjigaStavke[j].SlobodnaZauzeta,
                                ListaKnjigaNaslovi[rbLista].ListaKnjigaStavke[j].OstecenaNeostecena,
                                ListaKnjigaNaslovi[rbLista].ListaKnjigaStavke[j].MestoKnjige);
                        } 
                    }
                    BindingSource bs = new BindingSource(); bs.DataSource = dt;
                    gridKnjige.DataSource = bs;
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnKnjigaRekordLevo_Click(object sender, EventArgs e)  //prebacuje rekord levo za knjigu
        {
            try
            {
                if (ListaKnjigaNaslovi.Count > 0)
                KnjigaKlasa.KnjigaPrebacivanjeRekordaLevo(cmbNazivKnjige, cmbOblastKnjige, txtBrojStranaKnjige, cmbTipKnjige,
                    cmbVezKnjige, ListaKnjigaNaslovi, gridAutori, gridKnjige);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnKnjigaRekordDesno_Click(object sender, EventArgs e)   //prebacuje knjigu rekord desno za knjigu
        {
            try
            {
                if (ListaKnjigaNaslovi.Count > 0)
                {
                    gridAutori = KnjigaKlasa.KnjigaPrebacivanjeRekordaDesno(cmbNazivKnjige, cmbOblastKnjige, txtBrojStranaKnjige, cmbTipKnjige, 
                        cmbVezKnjige, ListaKnjigaNaslovi, gridAutori, gridKnjige);

                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnNoviRekordUpisKnjiga_Click(object sender, EventArgs e)
        {
            cmbNazivKnjige.Text = ""; cmbOblastKnjige.Text = ""; txtBrojStranaKnjige.Text = ""; cmbTipKnjige.Text = ""; cmbVezKnjige.Text = "";
            KreiranjePraznogGrida();
        }
        public void KreiranjePraznogGrida()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Autor");
            dt.Columns.Add("Knjiga");
            

            gridAutori.DataSource = dt;

            gridAutori.Columns[0].Width = gridAutori.Width/ 2;
            gridAutori.Columns[1].Width = gridAutori.Width / 2 - 50;

            DataTable dt2 = new DataTable();
            dt2.Columns.Add("Indetifikator");
            dt2.Columns.Add("Slobodna?"); dt2.Columns.Add("Ostecena?"); dt2.Columns.Add("Mesto");

            gridKnjige.DataSource = dt2;
            int sirinaKolone = (int)(gridKnjige.Width / 4)-11;
            gridKnjige.Columns[0].Width = sirinaKolone; gridKnjige.Columns[1].Width = sirinaKolone;
            gridKnjige.Columns[2].Width = sirinaKolone; gridKnjige.Columns[3].Width = sirinaKolone;
        }

        private void SelectedIndexChangePromenaKomboKnjiga(object sender, EventArgs e)
        {
            try
            {
                KnjigaKlasa knji = null;
                KnjigaKlasa.PromenaNaKomboBoxuKnjige(cmbNazivKnjige, cmbOblastKnjige, txtBrojStranaKnjige,
                    ListaKnjigaNaslovi, cmbTipKnjige, cmbVezKnjige, gridAutori, gridKnjige, knji, "nije dodat");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        


    }
}
