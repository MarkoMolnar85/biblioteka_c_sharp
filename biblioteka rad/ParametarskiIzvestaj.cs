﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;
namespace biblioteka_rad
{
    class ParametarskiIzvestaj : UpitiKlasaAbstrakt
    {
        private string parametarUpita;
        public ParametarskiIzvestaj(DataGridView grid, Form forma, string izbor, string parametarUpita)
        {
            this.grid = grid; this.forma = forma;
            this.traziSe = izbor;
            this.parametarUpita = parametarUpita;
        }
        public void IzvrsenjeUpita()
        {
            switch (this.traziSe)
            {
                case "pronadji studenta": PronadjiStudenta();
                    break;
                case "pronadji autora": AutorPoBiloKomOsnovu();
                    break;
                case "pronadji naslov": PronalazenjeNaslovaKnjigePoBiloKomOsnovu();
                    break;
                case "spisak": SpisakNevracenihKnjiga();
                    break;
                default:
                    break;
            }
        }

        private void TraziPoklapanje(List<StudentKlasa>listaUmanjena, List<StudentKlasa>listaCela, int index, string atribut)
        {
            string strZaPoredjenje = atribut;
            string parametar = this.parametarUpita;
            int brojPoklapanja = 0;
            for (int i = 0; i < strZaPoredjenje.Length && i < parametar.Length; i++ )
            {
                if (strZaPoredjenje[i] == parametar[i])
                {
                    brojPoklapanja++;
                }
            }
            if(brojPoklapanja == parametar.Length)
            {
                listaUmanjena.Add(listaCela[index]);
            }
        }

        private void PronadjiStudenta()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Broj indeksa");
            dt.Columns.Add("Ime");
            dt.Columns.Add("Prezime");
            dt.Columns.Add("Pol");
            dt.Columns.Add("JMBG");
            dt.Columns.Add("Bracni status");
            dt.Columns.Add("Nacin finansiranja");
            dt.Columns.Add("Godina rodjenja");
            dt.Columns.Add("Drzava");
            dt.Columns.Add("Grad");
            dt.Columns.Add("Telefon");
            dt.Columns.Add("Poverenje");

            List<StudentKlasa> listaStudenata = new List<StudentKlasa>();
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                string komandaStudentiStr = "select * from student";
                using (MySqlCommand komandaStudenti = new MySqlCommand(komandaStudentiStr, konekcija))
                {
                    MySqlDataReader citac = komandaStudenti.ExecuteReader();
                    while(citac.Read())
                    {
                        StudentKlasa stud = new StudentKlasa("string", citac.GetString(1), citac.GetString(2),
                            citac.GetString(3), citac.GetString(5), citac.GetString(4), citac.GetString(6),
                            citac.GetString(7), citac.GetString(8), citac.GetString(10), citac.GetString(9),
                            citac.GetString(11), citac.GetString(12));
                        listaStudenata.Add(stud);
                    }
                    citac.Close();
                    List<StudentKlasa> listaUmanjena = new List<StudentKlasa>();
                    for (int i = 0; i < listaStudenata.Count; i++)
                    {
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].BrIndeksa);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Ime);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Prezime);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Jmbg);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].BracniStatus);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Finansiranje);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Godina);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Drzava);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Telefon);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Poverenje);
                        TraziPoklapanje(listaUmanjena, listaStudenata, i, listaStudenata[i].Grad);
                    }
                    if (listaUmanjena.Count > 0)
                    {
                        HashSet<string> hashStudenti = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
                        for (int i = 0; i < listaUmanjena.Count; i++)
                        {
                            hashStudenti.Add(listaUmanjena[i].BrIndeksa);
                        }
                        int br = 0;
                        foreach (string s in hashStudenti)
                        {
                            using (MySqlCommand komandaStudentiZaIspis = new MySqlCommand())
                            {
                                komandaStudentiZaIspis.Connection = konekcija;
                                komandaStudentiZaIspis.CommandText = "select * from student where brojIndeksa = @brIndeksa;";
                                komandaStudentiZaIspis.Parameters.AddWithValue("@brIndeksa", s);
                                citac = komandaStudentiZaIspis.ExecuteReader();

                                while (citac.Read())
                                {
                                    dt.Rows.Add((br + 1), citac.GetString(1), citac.GetString(2), citac.GetString(3), citac.GetString(4),
                                        citac.GetString(5), citac.GetString(6), citac.GetString(7), citac.GetString(8), citac.GetString(9),
                                        citac.GetString(10), citac.GetString(11), citac.GetString(12));
                                    br++;
                                }
                                citac.Close();

                            }
                        }
                        BindingSource bs = new BindingSource();
                        bs.DataSource = dt; this.grid.DataSource = bs;
                        VisinaForme(br); SirinaForme(dt.Rows.Count);


                    }
                    else
                    {
                        BindingSource bs = new BindingSource();
                        bs.DataSource = dt; this.grid.DataSource = bs;
                        VisinaForme(0); SirinaForme(dt.Rows.Count);
                        this.forma.Close(); MessageBox.Show("Nema rezultata pretrage");
                    }
                }
            }
        }//kraj metode pronadji studenta

        private void PronalazenjeAutora(List<AutorKlasa> ListaVelika, List<AutorKlasa> ListaMala, int index, string atribut)
        {
            int brojac = 0;
            for (int i = 0; i < atribut.Length && i < this.parametarUpita.Length; i++)
            {
                if (atribut[i] == parametarUpita[i])
                {
                    brojac++;
                }
            }
            if (brojac == parametarUpita.Length)
            {
                ListaMala.Add(ListaVelika[index]);
            }
        }
        private void AutorPoBiloKomOsnovu()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Ime prezime");
            dt.Columns.Add("Godina");
            dt.Columns.Add("Drzava");
            dt.Columns.Add("Grad");
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaAutoriSve = new MySqlCommand())
                {
                    komandaAutoriSve.Connection = konekcija;
                    komandaAutoriSve.CommandText = "select * from autor;";
                    MySqlDataReader citac = komandaAutoriSve.ExecuteReader();
                    
                    List<AutorKlasa> listaAutora = new List<AutorKlasa>();
                    
                    while (citac.Read())
                    {
                        AutorKlasa autor = new AutorKlasa(citac.GetString(1),citac.GetString(2),citac.GetString(3),citac.GetString(4));
                        listaAutora.Add(autor);
                    }
                    citac.Close();
                    List<AutorKlasa> UmanjenaLista = new List<AutorKlasa>();
                    for(int i =0; i < listaAutora.Count; i++)
                    {
                        PronalazenjeAutora(listaAutora, UmanjenaLista, i, listaAutora[i].imePrezime);
                        PronalazenjeAutora(listaAutora, UmanjenaLista, i, listaAutora[i].godina);
                        PronalazenjeAutora(listaAutora, UmanjenaLista, i, listaAutora[i].drzava);
                        PronalazenjeAutora(listaAutora, UmanjenaLista, i, listaAutora[i].grad);
                    }
                    HashSet<string>hasAutori = new HashSet<string>();
                    for (int i = 0; i < UmanjenaLista.Count; i++)
                    {
                        hasAutori.Add(UmanjenaLista[i].imePrezime);
                    }
                    listaAutora = new List<AutorKlasa>();
                    foreach(var v in hasAutori)
                    {
                        using (MySqlCommand komandaAutoriOdabrani = new MySqlCommand())
                        {
                            komandaAutoriOdabrani.Connection = konekcija;
                            komandaAutoriOdabrani.CommandText = "select * from autor where imePrezimeAutora  =@imeAutora";
                            komandaAutoriOdabrani.Parameters.AddWithValue("@imeAutora", v);
                            citac = komandaAutoriOdabrani.ExecuteReader();
                            while (citac.Read())
                            {
                                listaAutora.Add(new AutorKlasa(citac.GetString(1), citac.GetString(2), 
                                    citac.GetString(3), citac.GetString(4)));
                            }
                            citac.Close();
                        }
                    }
                    int br = 0;
                    if (listaAutora.Count > 0)
                    {
                        for (int i = 0; i < listaAutora.Count; i++)
                        {
                            dt.Rows.Add((br + 1), listaAutora[i].imePrezime, listaAutora[i].godina, listaAutora[i].drzava, listaAutora[i].grad);
                            br++;
                        }
                        BindingSource bs = new BindingSource(); bs.DataSource = dt; this.grid.DataSource = bs;
                        VisinaForme(br); SirinaForme(dt.Rows.Count);
                    }
                    else
                    {
                        BindingSource bs = new BindingSource(); bs.DataSource = dt; this.grid.DataSource = bs;
                        VisinaForme(br); SirinaForme(dt.Rows.Count);
                        this.forma.Close(); MessageBox.Show("Nema rezultata pretrazivanja");
                    }

                }
            }
        }//kraj metode pretraga autora
        
        private void PronalazenjeKnjige(List<KnjigaKlasa> ListaVelika, List<KnjigaKlasa> ListaMala, int index, string atribut)
        {
            int brojac = 0;
            for (int i = 0; i < atribut.Length && i < this.parametarUpita.Length; i++)
            {
                if (atribut[i] == parametarUpita[i])
                {
                    brojac++;
                }
            }
            if (brojac == parametarUpita.Length)
            {
                ListaMala.Add(ListaVelika[index]);
            }
        }
        private void PronalazenjeNaslovaKnjigePoBiloKomOsnovu()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RB");
            dt.Columns.Add("Naziv knjige");
            dt.Columns.Add("Oblast");
            dt.Columns.Add("Broj strana");
            dt.Columns.Add("Tip");
            dt.Columns.Add("Vez");
            dt.Columns.Add("Izdavac");

            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaSveIzKnjiga = new MySqlCommand())
                {
                    komandaSveIzKnjiga.Connection = konekcija;
                    komandaSveIzKnjiga.CommandText = "select nazivKnjige, oblast, brojStrana, tip, vezKoji, nazivIzdavaca from knjiga, " +
                        " izdavac where izdavac_id = izdavac_izdavac_id";
                    
                    List<KnjigaKlasa> listanaslovaKnjiga = new List<KnjigaKlasa>();
                    MySqlDataReader citac = komandaSveIzKnjiga.ExecuteReader();
                    
                    while (citac.Read())
                    {
                        KnjigaKlasa knj = new KnjigaKlasa(citac.GetString(0), citac.GetString(1), citac.GetString(2), citac.GetString(3),
                            citac.GetString(4), citac.GetString(5), "string");
                        listanaslovaKnjiga.Add(knj);
                    }
                    
                    citac.Close();
                    List<KnjigaKlasa> umanjenaLista = new List<KnjigaKlasa>();
                    for (int i = 0; i < listanaslovaKnjiga.Count; i++)
                    {
                        PronalazenjeKnjige(listanaslovaKnjiga, umanjenaLista, i, listanaslovaKnjiga[i].Naziv);
                        PronalazenjeKnjige(listanaslovaKnjiga, umanjenaLista, i, listanaslovaKnjiga[i].oblast);
                        PronalazenjeKnjige(listanaslovaKnjiga, umanjenaLista, i, listanaslovaKnjiga[i].brojStrana.ToString());
                        PronalazenjeKnjige(listanaslovaKnjiga, umanjenaLista, i, listanaslovaKnjiga[i].tip);
                        PronalazenjeKnjige(listanaslovaKnjiga, umanjenaLista, i, listanaslovaKnjiga[i].vez);
                        PronalazenjeKnjige(listanaslovaKnjiga, umanjenaLista, i, listanaslovaKnjiga[i].ImeIzdavaca);
                    }
                    HashSet<KnjigaKlasa> hashKnjige = new HashSet<KnjigaKlasa>();
                    for (int i = 0; i < umanjenaLista.Count; i++)
                    {
                        hashKnjige.Add(umanjenaLista[i]);
                    }
                    int br = 0;
                    if (umanjenaLista.Count > 0)
                    {
                        foreach (var v in hashKnjige)
                        {
                            dt.Rows.Add((br + 1), v.Naziv, v.oblast, v.brojStrana, v.tip, v.vez, v.ImeIzdavaca);
                            br++;
                        }
                        BindingSource bs = new BindingSource(); bs.DataSource = dt; this.grid.DataSource = bs;
                        VisinaForme(br); SirinaForme(dt.Rows.Count);
                    }
                    else
                    {
                        BindingSource bs = new BindingSource(); bs.DataSource = dt; this.grid.DataSource = bs;
                        VisinaForme(br); SirinaForme(dt.Rows.Count);
                        this.forma.Close();
                        MessageBox.Show("Nema rezultata pretrage. Postoji osetljivost na mala i velika slova");
                    }

                }
            }
        }//kraj metode pronalazenje knjige po bilo kom osnovu

        public void SpisakNevracenihKnjiga()
        {
            this.grid.DataSource = null;
            DataTable dt = new DataTable();
            /*dt.Columns.Add("Naziv primerka");
            dt.Columns.Add("Slobodna/Zauzeta");
            dt.Columns.Add("Ostecena/Neostecena");
            dt.Columns.Add("Mesto knjige");
            dt.Columns.Add("Datuma uzeto");
            dt.Columns.Add("Na koliko dana");
            dt.Columns.Add("Predvidjeno vracanje");*/
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                string strStudenti = "select * from student where brojIndeksa = @brIndeksa;";
                using (MySqlCommand cmdStudenti = new MySqlCommand(strStudenti, konekcija))
                {
                    cmdStudenti.Parameters.AddWithValue("@brIndeksa", this.parametarUpita);
                    MySqlDataReader citac = cmdStudenti.ExecuteReader();
                    if (citac.Read())
                    {
                        string strPrimarniKljucStudent = citac.GetString(0);
                        citac.Close();
                        using (MySqlCommand cmdNadjiKnjigeUizdato = new MySqlCommand())
                        {
                            cmdNadjiKnjigeUizdato.Connection = konekcija;
                            cmdNadjiKnjigeUizdato.CommandText = "select indetifikator, slobodnaZauzeta, knjige.ostecenaNeostecena, " +
                                " mestoKnjige, datumaUzeto, kolikoDana, izdatoVracenocol " +
                                " from knjige inner join izdatovraceno on knjige_id = knjige_knjige_id " +
                                " where student_student_id = @studentKljuc && datumaVraceno is NULL;";
                            cmdNadjiKnjigeUizdato.Parameters.AddWithValue("@studentKljuc", strPrimarniKljucStudent);
                            MySqlDataAdapter adapter = new MySqlDataAdapter(cmdNadjiKnjigeUizdato);
                            adapter.Fill(dt);
                            BindingSource bs = new BindingSource(); bs.DataSource = dt; this.grid.DataSource = bs;
                            this.grid.Columns[0].HeaderText = "Naziv primerka";
                            this.grid.Columns[1].HeaderText = "Slobodna/Zauzeta";
                            this.grid.Columns[2].HeaderText = "Ostecena/Neostecena";
                            this.grid.Columns[3].HeaderText = "Mesto knjige";
                            this.grid.Columns[4].HeaderText = "Datuma uzeta";
                            this.grid.Columns[5].HeaderText = "Na koliko dana";
                            this.grid.Columns[6].HeaderText = "Predvidjeno vracanje";
                            VisinaForme(dt.Rows.Count); SirinaForme(dt.Rows.Count);
                        }
                    }
                    else if (String.IsNullOrEmpty(this.parametarUpita))
                    {
                        this.forma.Close();
                        MessageBox.Show("Da bi dobili listu nevracenih knjiga odredjenog studenta, potrebno je uneti njegov broj indeksa");
                    }
                    else
                    {
                        this.forma.Close();
                        MessageBox.Show("Ne postoji student sa ovim brojem indeksa");
                    }
                }
            }

        }
    }
}
