﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace biblioteka_rad
{
    class tempKlasa
    {
        public string Naziv { get; set; }
        public string Oblast { get; set; }
        public string BrojStrana { get; set; }
        public string Tip { get; set; }
        public string Vez { get; set; }
        public string IdIzdavaca { get; set; }
        public tempKlasa(string naziv, string oblast, string brojStrana, string tip, string vez, string idIzdavaca)
        {
            Naziv = naziv; Oblast = oblast; BrojStrana = brojStrana; Tip = tip; Vez = vez; IdIzdavaca = idIzdavaca;
        }
    }
}
