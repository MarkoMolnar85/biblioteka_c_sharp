﻿namespace biblioteka_rad
{
    partial class frmGlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpisStudenata = new System.Windows.Forms.Button();
            this.btnUpisBibliotekara = new System.Windows.Forms.Button();
            this.btnUpisAutora = new System.Windows.Forms.Button();
            this.btnIzdavajneVracanje = new System.Windows.Forms.Button();
            this.btnIzdavacKnjiga = new System.Windows.Forms.Button();
            this.btnSipleSviStudenti = new System.Windows.Forms.Button();
            this.btnSimpleSviAutori = new System.Windows.Forms.Button();
            this.btnSimpleSveKnjige = new System.Windows.Forms.Button();
            this.btnSimpleSviPrimerciKnjiga = new System.Windows.Forms.Button();
            this.btnSimpleSviSlobodniPrimerciKnjiga = new System.Windows.Forms.Button();
            this.btnSimpleSviZauzetiPrimerciKnjiga = new System.Windows.Forms.Button();
            this.btnSimpleSviOsteceniPrimerciKnjiga = new System.Windows.Forms.Button();
            this.btnSimpleSpisakIkadaIzdatihKnjiga = new System.Windows.Forms.Button();
            this.btnSimpleProsecnaOcenaSvihKnjiga = new System.Windows.Forms.Button();
            this.btnNadjiStudenta = new System.Windows.Forms.Button();
            this.btnPronadjiAutora = new System.Windows.Forms.Button();
            this.btnKnjigaPoBiloKomOsnovu = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnUpisStudenata
            // 
            this.btnUpisStudenata.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUpisStudenata.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpisStudenata.Location = new System.Drawing.Point(12, 60);
            this.btnUpisStudenata.Name = "btnUpisStudenata";
            this.btnUpisStudenata.Size = new System.Drawing.Size(133, 56);
            this.btnUpisStudenata.TabIndex = 0;
            this.btnUpisStudenata.Text = "Upis studenta";
            this.btnUpisStudenata.UseVisualStyleBackColor = false;
            this.btnUpisStudenata.Click += new System.EventHandler(this.UpisStudentaOtvori);
            // 
            // btnUpisBibliotekara
            // 
            this.btnUpisBibliotekara.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUpisBibliotekara.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpisBibliotekara.Location = new System.Drawing.Point(151, 60);
            this.btnUpisBibliotekara.Name = "btnUpisBibliotekara";
            this.btnUpisBibliotekara.Size = new System.Drawing.Size(133, 56);
            this.btnUpisBibliotekara.TabIndex = 1;
            this.btnUpisBibliotekara.Text = "Upis bibliotekara";
            this.btnUpisBibliotekara.UseVisualStyleBackColor = false;
            this.btnUpisBibliotekara.Click += new System.EventHandler(this.btnUpisBibliotekara_Click);
            // 
            // btnUpisAutora
            // 
            this.btnUpisAutora.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUpisAutora.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpisAutora.Location = new System.Drawing.Point(418, 60);
            this.btnUpisAutora.Name = "btnUpisAutora";
            this.btnUpisAutora.Size = new System.Drawing.Size(133, 56);
            this.btnUpisAutora.TabIndex = 3;
            this.btnUpisAutora.Text = "Upis Autora";
            this.btnUpisAutora.UseVisualStyleBackColor = false;
            this.btnUpisAutora.Click += new System.EventHandler(this.btnUpisAutora_Click);
            // 
            // btnIzdavajneVracanje
            // 
            this.btnIzdavajneVracanje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnIzdavajneVracanje.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzdavajneVracanje.Location = new System.Drawing.Point(557, 60);
            this.btnIzdavajneVracanje.Name = "btnIzdavajneVracanje";
            this.btnIzdavajneVracanje.Size = new System.Drawing.Size(133, 56);
            this.btnIzdavajneVracanje.TabIndex = 4;
            this.btnIzdavajneVracanje.Text = "Izdaj preuzmi knjigu";
            this.btnIzdavajneVracanje.UseVisualStyleBackColor = false;
            this.btnIzdavajneVracanje.Click += new System.EventHandler(this.btnIzdavajneVracanje_Click);
            // 
            // btnIzdavacKnjiga
            // 
            this.btnIzdavacKnjiga.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnIzdavacKnjiga.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzdavacKnjiga.Location = new System.Drawing.Point(290, 60);
            this.btnIzdavacKnjiga.Name = "btnIzdavacKnjiga";
            this.btnIzdavacKnjiga.Size = new System.Drawing.Size(122, 56);
            this.btnIzdavacKnjiga.TabIndex = 2;
            this.btnIzdavacKnjiga.Text = "Nove knjige";
            this.btnIzdavacKnjiga.UseVisualStyleBackColor = false;
            this.btnIzdavacKnjiga.Click += new System.EventHandler(this.btnIzdavacKnjiga_Click);
            // 
            // btnSipleSviStudenti
            // 
            this.btnSipleSviStudenti.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSipleSviStudenti.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSipleSviStudenti.Location = new System.Drawing.Point(12, 178);
            this.btnSipleSviStudenti.Name = "btnSipleSviStudenti";
            this.btnSipleSviStudenti.Size = new System.Drawing.Size(133, 70);
            this.btnSipleSviStudenti.TabIndex = 5;
            this.btnSipleSviStudenti.Text = "Svi studenti";
            this.btnSipleSviStudenti.UseVisualStyleBackColor = false;
            this.btnSipleSviStudenti.Click += new System.EventHandler(this.btnSipleSviStudenti_Click);
            // 
            // btnSimpleSviAutori
            // 
            this.btnSimpleSviAutori.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleSviAutori.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSviAutori.Location = new System.Drawing.Point(151, 178);
            this.btnSimpleSviAutori.Name = "btnSimpleSviAutori";
            this.btnSimpleSviAutori.Size = new System.Drawing.Size(133, 70);
            this.btnSimpleSviAutori.TabIndex = 6;
            this.btnSimpleSviAutori.Text = "Svi autori";
            this.btnSimpleSviAutori.UseVisualStyleBackColor = false;
            this.btnSimpleSviAutori.Click += new System.EventHandler(this.btnSimpleSviAutori_Click);
            // 
            // btnSimpleSveKnjige
            // 
            this.btnSimpleSveKnjige.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleSveKnjige.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSveKnjige.Location = new System.Drawing.Point(290, 178);
            this.btnSimpleSveKnjige.Name = "btnSimpleSveKnjige";
            this.btnSimpleSveKnjige.Size = new System.Drawing.Size(122, 70);
            this.btnSimpleSveKnjige.TabIndex = 7;
            this.btnSimpleSveKnjige.Text = "Sve knjige";
            this.btnSimpleSveKnjige.UseVisualStyleBackColor = false;
            this.btnSimpleSveKnjige.Click += new System.EventHandler(this.btnSimpleSveKnjige_Click);
            // 
            // btnSimpleSviPrimerciKnjiga
            // 
            this.btnSimpleSviPrimerciKnjiga.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleSviPrimerciKnjiga.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSviPrimerciKnjiga.Location = new System.Drawing.Point(418, 178);
            this.btnSimpleSviPrimerciKnjiga.Name = "btnSimpleSviPrimerciKnjiga";
            this.btnSimpleSviPrimerciKnjiga.Size = new System.Drawing.Size(133, 70);
            this.btnSimpleSviPrimerciKnjiga.TabIndex = 8;
            this.btnSimpleSviPrimerciKnjiga.Text = "Svi primerci knjiga";
            this.btnSimpleSviPrimerciKnjiga.UseVisualStyleBackColor = false;
            this.btnSimpleSviPrimerciKnjiga.Click += new System.EventHandler(this.btnSimpleSviPrimerciKnjiga_Click);
            // 
            // btnSimpleSviSlobodniPrimerciKnjiga
            // 
            this.btnSimpleSviSlobodniPrimerciKnjiga.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleSviSlobodniPrimerciKnjiga.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSviSlobodniPrimerciKnjiga.Location = new System.Drawing.Point(557, 178);
            this.btnSimpleSviSlobodniPrimerciKnjiga.Name = "btnSimpleSviSlobodniPrimerciKnjiga";
            this.btnSimpleSviSlobodniPrimerciKnjiga.Size = new System.Drawing.Size(133, 70);
            this.btnSimpleSviSlobodniPrimerciKnjiga.TabIndex = 9;
            this.btnSimpleSviSlobodniPrimerciKnjiga.Text = "Svi slobodni primerci knjiga";
            this.btnSimpleSviSlobodniPrimerciKnjiga.UseVisualStyleBackColor = false;
            this.btnSimpleSviSlobodniPrimerciKnjiga.Click += new System.EventHandler(this.btnSimpleSviSlobodniPrimerciKnjiga_Click);
            // 
            // btnSimpleSviZauzetiPrimerciKnjiga
            // 
            this.btnSimpleSviZauzetiPrimerciKnjiga.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleSviZauzetiPrimerciKnjiga.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSviZauzetiPrimerciKnjiga.Location = new System.Drawing.Point(23, 254);
            this.btnSimpleSviZauzetiPrimerciKnjiga.Name = "btnSimpleSviZauzetiPrimerciKnjiga";
            this.btnSimpleSviZauzetiPrimerciKnjiga.Size = new System.Drawing.Size(160, 64);
            this.btnSimpleSviZauzetiPrimerciKnjiga.TabIndex = 10;
            this.btnSimpleSviZauzetiPrimerciKnjiga.Text = "Svi zauzeti primeci knjiga";
            this.btnSimpleSviZauzetiPrimerciKnjiga.UseVisualStyleBackColor = false;
            this.btnSimpleSviZauzetiPrimerciKnjiga.Click += new System.EventHandler(this.btnSimpleSviZauzetiPrimerciKnjiga_Click);
            // 
            // btnSimpleSviOsteceniPrimerciKnjiga
            // 
            this.btnSimpleSviOsteceniPrimerciKnjiga.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleSviOsteceniPrimerciKnjiga.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSviOsteceniPrimerciKnjiga.Location = new System.Drawing.Point(189, 254);
            this.btnSimpleSviOsteceniPrimerciKnjiga.Name = "btnSimpleSviOsteceniPrimerciKnjiga";
            this.btnSimpleSviOsteceniPrimerciKnjiga.Size = new System.Drawing.Size(160, 64);
            this.btnSimpleSviOsteceniPrimerciKnjiga.TabIndex = 11;
            this.btnSimpleSviOsteceniPrimerciKnjiga.Text = "Svi osteceni primerci knjiga";
            this.btnSimpleSviOsteceniPrimerciKnjiga.UseVisualStyleBackColor = false;
            this.btnSimpleSviOsteceniPrimerciKnjiga.Click += new System.EventHandler(this.btnSimpleSviOsteceniPrimerciKnjiga_Click);
            // 
            // btnSimpleSpisakIkadaIzdatihKnjiga
            // 
            this.btnSimpleSpisakIkadaIzdatihKnjiga.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleSpisakIkadaIzdatihKnjiga.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSpisakIkadaIzdatihKnjiga.Location = new System.Drawing.Point(364, 254);
            this.btnSimpleSpisakIkadaIzdatihKnjiga.Name = "btnSimpleSpisakIkadaIzdatihKnjiga";
            this.btnSimpleSpisakIkadaIzdatihKnjiga.Size = new System.Drawing.Size(150, 64);
            this.btnSimpleSpisakIkadaIzdatihKnjiga.TabIndex = 12;
            this.btnSimpleSpisakIkadaIzdatihKnjiga.Text = "Spisak ikada izdatih knjiga";
            this.btnSimpleSpisakIkadaIzdatihKnjiga.UseVisualStyleBackColor = false;
            this.btnSimpleSpisakIkadaIzdatihKnjiga.Click += new System.EventHandler(this.btnSimpleSpisakIkadaIzdatihKnjiga_Click);
            // 
            // btnSimpleProsecnaOcenaSvihKnjiga
            // 
            this.btnSimpleProsecnaOcenaSvihKnjiga.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSimpleProsecnaOcenaSvihKnjiga.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleProsecnaOcenaSvihKnjiga.Location = new System.Drawing.Point(520, 254);
            this.btnSimpleProsecnaOcenaSvihKnjiga.Name = "btnSimpleProsecnaOcenaSvihKnjiga";
            this.btnSimpleProsecnaOcenaSvihKnjiga.Size = new System.Drawing.Size(160, 64);
            this.btnSimpleProsecnaOcenaSvihKnjiga.TabIndex = 13;
            this.btnSimpleProsecnaOcenaSvihKnjiga.Text = "Prosecna ocena svih knjiga";
            this.btnSimpleProsecnaOcenaSvihKnjiga.UseVisualStyleBackColor = false;
            this.btnSimpleProsecnaOcenaSvihKnjiga.Click += new System.EventHandler(this.btnSimpleProsecnaOcenaSvihKnjiga_Click);
            // 
            // btnNadjiStudenta
            // 
            this.btnNadjiStudenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnNadjiStudenta.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNadjiStudenta.Location = new System.Drawing.Point(88, 366);
            this.btnNadjiStudenta.Name = "btnNadjiStudenta";
            this.btnNadjiStudenta.Size = new System.Drawing.Size(133, 55);
            this.btnNadjiStudenta.TabIndex = 14;
            this.btnNadjiStudenta.Text = "Pronadji studenta";
            this.btnNadjiStudenta.UseVisualStyleBackColor = false;
            this.btnNadjiStudenta.Click += new System.EventHandler(this.btnNadjiStudenta_Click);
            // 
            // btnPronadjiAutora
            // 
            this.btnPronadjiAutora.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnPronadjiAutora.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPronadjiAutora.Location = new System.Drawing.Point(227, 366);
            this.btnPronadjiAutora.Name = "btnPronadjiAutora";
            this.btnPronadjiAutora.Size = new System.Drawing.Size(122, 55);
            this.btnPronadjiAutora.TabIndex = 15;
            this.btnPronadjiAutora.Text = "Pronadji autora";
            this.btnPronadjiAutora.UseVisualStyleBackColor = false;
            this.btnPronadjiAutora.Click += new System.EventHandler(this.btnPronadjiAutora_Click);
            // 
            // btnKnjigaPoBiloKomOsnovu
            // 
            this.btnKnjigaPoBiloKomOsnovu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnKnjigaPoBiloKomOsnovu.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKnjigaPoBiloKomOsnovu.Location = new System.Drawing.Point(355, 366);
            this.btnKnjigaPoBiloKomOsnovu.Name = "btnKnjigaPoBiloKomOsnovu";
            this.btnKnjigaPoBiloKomOsnovu.Size = new System.Drawing.Size(133, 55);
            this.btnKnjigaPoBiloKomOsnovu.TabIndex = 16;
            this.btnKnjigaPoBiloKomOsnovu.Text = "Pronadji knjigu";
            this.btnKnjigaPoBiloKomOsnovu.UseVisualStyleBackColor = false;
            this.btnKnjigaPoBiloKomOsnovu.Click += new System.EventHandler(this.btnKnjigaPoBiloKomOsnovu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(317, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 30);
            this.label1.TabIndex = 9;
            this.label1.Text = "Upis";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(297, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 30);
            this.label2.TabIndex = 9;
            this.label2.Text = "Pregled";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(295, 333);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 30);
            this.label3.TabIndex = 9;
            this.label3.Text = "Pretraga";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(494, 366);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 55);
            this.button1.TabIndex = 17;
            this.button1.Text = "Student nije vratio";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmGlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = global::biblioteka_rad.Properties.Resources.glavnaForma;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(703, 487);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnKnjigaPoBiloKomOsnovu);
            this.Controls.Add(this.btnPronadjiAutora);
            this.Controls.Add(this.btnNadjiStudenta);
            this.Controls.Add(this.btnSimpleSviSlobodniPrimerciKnjiga);
            this.Controls.Add(this.btnSimpleProsecnaOcenaSvihKnjiga);
            this.Controls.Add(this.btnSimpleSpisakIkadaIzdatihKnjiga);
            this.Controls.Add(this.btnSimpleSviPrimerciKnjiga);
            this.Controls.Add(this.btnSimpleSviOsteceniPrimerciKnjiga);
            this.Controls.Add(this.btnSimpleSveKnjige);
            this.Controls.Add(this.btnSimpleSviZauzetiPrimerciKnjiga);
            this.Controls.Add(this.btnSimpleSviAutori);
            this.Controls.Add(this.btnSipleSviStudenti);
            this.Controls.Add(this.btnIzdavacKnjiga);
            this.Controls.Add(this.btnIzdavajneVracanje);
            this.Controls.Add(this.btnUpisAutora);
            this.Controls.Add(this.btnUpisBibliotekara);
            this.Controls.Add(this.btnUpisStudenata);
            this.Name = "frmGlavnaForma";
            this.ShowIcon = false;
            this.Text = "Glavna forma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpisStudenata;
        private System.Windows.Forms.Button btnUpisBibliotekara;
        private System.Windows.Forms.Button btnUpisAutora;
        private System.Windows.Forms.Button btnIzdavajneVracanje;
        private System.Windows.Forms.Button btnIzdavacKnjiga;
        private System.Windows.Forms.Button btnSipleSviStudenti;
        private System.Windows.Forms.Button btnSimpleSviAutori;
        private System.Windows.Forms.Button btnSimpleSveKnjige;
        private System.Windows.Forms.Button btnSimpleSviPrimerciKnjiga;
        private System.Windows.Forms.Button btnSimpleSviSlobodniPrimerciKnjiga;
        private System.Windows.Forms.Button btnSimpleSviZauzetiPrimerciKnjiga;
        private System.Windows.Forms.Button btnSimpleSviOsteceniPrimerciKnjiga;
        private System.Windows.Forms.Button btnSimpleSpisakIkadaIzdatihKnjiga;
        private System.Windows.Forms.Button btnSimpleProsecnaOcenaSvihKnjiga;
        private System.Windows.Forms.Button btnNadjiStudenta;
        private System.Windows.Forms.Button btnPronadjiAutora;
        private System.Windows.Forms.Button btnKnjigaPoBiloKomOsnovu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
    }
}