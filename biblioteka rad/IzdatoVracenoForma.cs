﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace biblioteka_rad
{
    public partial class frmIzdatoVracenoForma : Form
    {
        public frmIzdatoVracenoForma()
        {
            InitializeComponent();
        }

        private void KliknutTabIzdajemVracam(object sender, EventArgs e)
        {
            try
            {
                if (KontrolaTabIzdatoVrateci.SelectedIndex == 0)
                {
                    IzdatoKlasa.BogacenjeKomboBoxaBibliotekar(cmbBibliotekarIzdajem);
                    IzdatoKlasa.BogacenjeKomboBoxaBrojIndeksa(cmbBrojIndeksaIzdajem);
                    IzdatoKlasa.BogacenjeKomboBoxaKnjiga(cmbKnjigaIzdajem);
                }
                else if (KontrolaTabIzdatoVrateci.SelectedIndex == 1)
                {
                    VracenoKlasa.FiksniKomboBoxoviVraceno(cmbOstecenaVracam, cmbPoverenjeVracam, cmbOcenaVracam);
                    VracenoKlasa.BogacenjeKomboBoxaBrojIndexaVraceno(cmbBrojIndeksaVracam);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PokretanjeFormePocetak(object sender, EventArgs e)
        {
            try
            {
                IzdatoKlasa.BogacenjeKomboBoxaBibliotekar(cmbBibliotekarIzdajem);
                IzdatoKlasa.BogacenjeKomboBoxaBrojIndeksa(cmbBrojIndeksaIzdajem);
                IzdatoKlasa.BogacenjeKomboBoxaKnjiga(cmbKnjigaIzdajem);
                txtImeIzdajem.Enabled = txtPrezimeIzdajem.Enabled = txtpolIzdajem.Enabled = txtJmbgizdajem.Enabled = txtBracniStatusIzdajem.Enabled = false;
                txtnacinFinansiranjaIzdajem.Enabled = txtGodinaRodjenjaIzdajem.Enabled = txtDrzavaIzdajem.Enabled = false;
                txtGradIzdajem.Enabled = txtBrojTelefonaIdajem.Enabled = txtPoverenjeIzdajem.Enabled = false;
                txtImeVraceno.Enabled = txtPrezimeVraceno.Enabled = txtPolVraceon.Enabled = txtJmbgVraceno.Enabled = txtBracniStatusVraceno.Enabled = false;
                txtnacinFinansiranjaVraceno.Enabled = txtGodinaVraceno.Enabled = txtDrzavaVraceno.Enabled = false;
                txtGradVraceno.Enabled = txtBrojTelefonaVraceno.Enabled = txtPoverenjeVraceno.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SelectedIndexChangePromenaKomboKnjiga(object sender, EventArgs e)
        {
            try
            {
                IzdatoKlasa.BogacenjePrimerakaKnjiga(cmbKnjigaIzdajem, cmbPrimerakIzdajem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ModifikacijaTexaKomboBoxaBrIndeksaIzdajem(object sender, EventArgs e)
        {
            try
            {
                IzdatoKlasa.ModifikacijaTextaBrIndeksa(cmbBrojIndeksaIzdajem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateTextaKnjiga(object sender, EventArgs e)
        {
            IzdatoKlasa.ModifikacijaKomboKnjiga(cmbKnjigaIzdajem);
        }

        private void UpdateModifikacijaPrimerakaKnjiga(object sender, EventArgs e)
        {
            IzdatoKlasa.UpdateTextaPrimerci(cmbKnjigaIzdajem, cmbPrimerakIzdajem);
        }

        private void IzdajKnjiguEVENT(object sender, EventArgs e)
        {
            try
            {
                IzdatoKlasa izdato = new IzdatoKlasa(cmbBibliotekarIzdajem, cmbBrojIndeksaIzdajem, cmbKnjigaIzdajem, cmbPrimerakIzdajem,
                    txtDatumIzdavanjaIzdajem, txtKolikoDanaIzdajem);
                if (izdato.PopunjenaPoljaizdajemOK() && izdato.daniOk)
                {
                    txtPredvidjenoVracanjeIzdajem.Text = IzdatoKlasa.LepoNapisiDatumZaTextBoxSamoAkoSeVadiIzGotovogDatuma(izdato.PredvidjeniPovratak);
                    izdato.KonekcijaUpisIzdavanja();
                    cmbPrimerakIzdajem.Items.Remove(cmbPrimerakIzdajem.Text); cmbPrimerakIzdajem.Text = "";
                    
                }
                IzdatoKlasa.BogacenjePrimerakaKnjiga(cmbKnjigaIzdajem, cmbPrimerakIzdajem);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }
        //sve ispod je za vracanje tab

        private void btnNoviUnosIzdavanjeKnjige(object sender, EventArgs e)
        {
            cmbBibliotekarIzdajem.Text = ""; cmbBrojIndeksaIzdajem.Text = ""; cmbKnjigaIzdajem.Text = "";
            cmbPrimerakIzdajem.Text = ""; txtDatumIzdavanjaIzdajem.Clear(); txtKolikoDanaIzdajem.Clear(); 
            txtPredvidjenoVracanjeIzdajem.Clear();
        }

        private void SelectedIndexChangePromenaBrIndexavraceno(object sender, EventArgs e)
        {
            try
            {
                VracenoKlasa.BogacenjeKomboBoxaPrimerak(cmbBrojIndeksaVracam, cmbPrimerakVracam);
                VracenoKlasa.PromenaKomboBoxaBrojIndeksa(cmbBrojIndeksaVracam, txtImeVraceno, txtPrezimeVraceno, txtPolVraceon, txtJmbgVraceno,
                    txtBracniStatusVraceno, txtnacinFinansiranjaVraceno, txtGodinaVraceno, txtDrzavaVraceno, txtGradVraceno, txtBrojTelefonaVraceno,
                    txtPoverenjeVraceno, imgSlikaVraceno);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnVracanjeKnjigeEVENT(object sender, EventArgs e)
        {
            try
            {
                VracenoKlasa v1 = new VracenoKlasa(cmbBrojIndeksaVracam, cmbPrimerakVracam,
                    txtDatumaVracenoVracam, cmbPoverenjeVracam, cmbOstecenaVracam, cmbOcenaVracam);
                if (v1.UnetaPoljaOK)
                {
                    v1.KonekcijaUnosVraceno();
                    cmbPrimerakVracam.Items.Remove(cmbPrimerakVracam.Text); cmbPrimerakVracam.Text = "";
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnNoviUnosVracenoEVENT(object sender, EventArgs e)
        {
            try
            {
                cmbBrojIndeksaVracam.Text = ""; cmbPrimerakVracam.Text = ""; txtDatumaVracenoVracam.Clear();
                cmbPoverenjeVracam.Text = ""; cmbOstecenaVracam.Text = ""; cmbOcenaVracam.Text = "";
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void SelectedIndexChangeIzdajemIndex(object sender, EventArgs e)
        {
            try
            {
                IzdatoKlasa.PromenaKomboBoxaBrojIndeksa(cmbBrojIndeksaIzdajem, txtImeIzdajem, txtPrezimeIzdajem, txtpolIzdajem, txtJmbgizdajem, txtBracniStatusIzdajem,
                    txtnacinFinansiranjaIzdajem, txtGodinaRodjenjaIzdajem, txtDrzavaIzdajem, txtGradIzdajem, txtBrojTelefonaIdajem, txtPoverenjeIzdajem, imgSlikaStudentaIzdajem);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

    }
}
