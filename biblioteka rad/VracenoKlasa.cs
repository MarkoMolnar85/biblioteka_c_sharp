﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.IO;
namespace biblioteka_rad
{
    class VracenoKlasa
    {
        string brojIndeksa;
        string indetifikator;
        DateTime datumaVraceno;
        string poverenje;
        string ostecenje;
        int ocenaKnjige;
        public bool UnetaPoljaOK { get; private set; }
        
        public DateTime RazbiDatum(string datumaIzdato)
        {
            DateTime vreme = DateTime.Now;

            string[] danMesecGodina = new string[3];
            int j = 0;
            string datumaIzdatoStr = datumaIzdato;
            for (int i = 0; i < datumaIzdatoStr.Length && j < danMesecGodina.Length; i++)
            {
                if (datumaIzdatoStr[i] != ' ')
                {
                    if (datumaIzdatoStr[i] != '/')
                    {
                        danMesecGodina[j] += datumaIzdatoStr[i].ToString();
                    }
                }
                if (datumaIzdatoStr[i] == '/')
                {
                    j++;
                }
            }
            danMesecGodina[2] += '/'; danMesecGodina[1] += '/';
            string spojen = danMesecGodina[2] + danMesecGodina[1] + danMesecGodina[0];
            vreme = new DateTime(); vreme = Convert.ToDateTime(spojen);

            return vreme;
        }

        public VracenoKlasa(ComboBox brojIndeksa, ComboBox primerak, TextBox datumavraceno, ComboBox poverenje, ComboBox ostecena, ComboBox ocena)
        {
            if (String.IsNullOrEmpty(brojIndeksa.Text) || String.IsNullOrEmpty(primerak.Text) || String.IsNullOrEmpty(datumavraceno.Text) ||
                String.IsNullOrEmpty(poverenje.Text) || String.IsNullOrEmpty(ostecena.Text) || String.IsNullOrEmpty(ocena.Text))
            {
                MessageBox.Show("Potrebno je uneti sva polja");
                UnetaPoljaOK = false;
            }
            else
            {
                bool ocenaOK = false;
                ocenaOK = int.TryParse(ocena.Text, out this.ocenaKnjige);
                if (ocenaOK || this.ocenaKnjige !=1 || this.ocenaKnjige !=2 ||this.ocenaKnjige !=3 ||this.ocenaKnjige !=4 ||this.ocenaKnjige !=5)
                {
                    this.brojIndeksa = brojIndeksa.Text; this.indetifikator = primerak.Text; this.datumaVraceno = RazbiDatum(datumavraceno.Text);
                    this.poverenje = poverenje.Text; this.ostecenje = ostecena.Text;
                    UnetaPoljaOK = true;
                }
                else
                {
                    UnetaPoljaOK = false;
                    MessageBox.Show("Ocena nije upisana kako treba");
                }
            }
        }

        public void KonekcijaUnosVraceno()
        {
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid = root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                
                string kljucStudenta1Str = "select student_id from student where brojIndeksa=@br;";
                string kljucKnjige1Str = "select knjige_id from biblioteka.knjige where indetifikator = @ind;";
                using (MySqlCommand komandaKljucStudenta1 = new MySqlCommand(kljucStudenta1Str, konekcija))
                {
                    komandaKljucStudenta1.Parameters.AddWithValue("@br", this.brojIndeksa);
                    MySqlDataReader citac = komandaKljucStudenta1.ExecuteReader();
                    int brojCitanja = 0;
                    string potrebanKljucStudenta = "";
                    while (citac.Read())
                    {
                        brojCitanja++;
                        potrebanKljucStudenta = citac.GetString(0);
                    }
                    citac.Close();
                    if(brojCitanja > 0)
                        using (MySqlCommand komandakljucKnjige1 = new MySqlCommand(kljucKnjige1Str, konekcija))
                        {
                            komandakljucKnjige1.Parameters.AddWithValue("@ind", this.indetifikator);
                            brojCitanja = 0;
                            string potrebanKljucKnjige = "";
                            citac = komandakljucKnjige1.ExecuteReader();
                            while (citac.Read())
                            {
                                brojCitanja++;
                                potrebanKljucKnjige = citac.GetString(0);
                            }
                            citac.Close();
                            string komandaPrimarniKljucIzdatovracenoStr1 = "select izdatoVraceno_id from izdatovraceno, biblioteka.knjige, student " +
                                /*" where student_student_id = student_id && knjige_knjige_id = knjige_id &&*/ "where  knjige.slobodnaZauzeta = 'Zauzeta' && " +
                                " student_student_id = @kljucStudent11 && knjige_knjige_id=@kljucknjige11 ;"; //todo
                            if(brojCitanja > 0)
                            using (MySqlCommand komandaPrimarniKljucIzdatovraceno1 = new MySqlCommand(komandaPrimarniKljucIzdatovracenoStr1, konekcija))
                            {
                                komandaPrimarniKljucIzdatovraceno1.Parameters.AddWithValue("@kljucStudent11", potrebanKljucStudenta);
                                komandaPrimarniKljucIzdatovraceno1.Parameters.AddWithValue("@kljucknjige11", potrebanKljucKnjige);
                                citac = komandaPrimarniKljucIzdatovraceno1.ExecuteReader();
                                brojCitanja = 0;
                                string kljucZaSveOstalo = "";
                                while (citac.Read())
                                {
                                    brojCitanja++;
                                    kljucZaSveOstalo = citac.GetString(0);
                                }
                                citac.Close();
                                if (brojCitanja > 0)
                                {
                                    string komandaUpdateSveOstaloStr = "update izdatovraceno set datumaVraceno = @datumm, ocenaKnjige = @ocenaa, " +
                                    " izdatovraceno.ostecenaNeostecena = @ostecenjee where izdatoVraceno_id = @kljucc;";
                                    using (MySqlCommand komandaUpdateSveOstalo = new MySqlCommand(komandaUpdateSveOstaloStr, konekcija))
                                    {
                                        komandaUpdateSveOstalo.Parameters.AddWithValue("@datumm", this.datumaVraceno);
                                        komandaUpdateSveOstalo.Parameters.AddWithValue("@ocenaa", this.ocenaKnjige);
                                        komandaUpdateSveOstalo.Parameters.AddWithValue("@ostecenjee", this.ostecenje);
                                        komandaUpdateSveOstalo.Parameters.AddWithValue("@kljucc", kljucZaSveOstalo);
                                        komandaUpdateSveOstalo.ExecuteNonQuery();
                                    }
                                    //kraj update izdatovraceno
                                    //
                                    string komandaUpdateStudentPoverenjeStr = "update student set nivoPoverenjaStudenta =@poverenje "+
                                        " where brojIndeksa = @brIndeksa;";
                                    using (MySqlCommand komandaUpdatePoverenjeStudenta = new MySqlCommand(komandaUpdateStudentPoverenjeStr, konekcija))
                                    {
                                        komandaUpdatePoverenjeStudenta.Parameters.AddWithValue("@brIndeksa", this.brojIndeksa);
                                        komandaUpdatePoverenjeStudenta.Parameters.AddWithValue("@poverenje", this.poverenje);
                                        komandaUpdatePoverenjeStudenta.ExecuteNonQuery();
                                    }
                                    string komandaUpdateOstecenjeZauzetostStr = "update knjige set knjige.ostecenaNeostecena = @ostecenje, slobodnaZauzeta = 'Slobodna' "+
                                        " where indetifikator = @indet;";
                                    using (MySqlCommand komandaUpdateOstecenjeKnjige = new MySqlCommand(komandaUpdateOstecenjeZauzetostStr, konekcija))
                                    {
                                        komandaUpdateOstecenjeKnjige.Parameters.AddWithValue("@ostecenje", this.ostecenje);
                                        komandaUpdateOstecenjeKnjige.Parameters.AddWithValue("@indet", this.indetifikator);
                                        komandaUpdateOstecenjeKnjige.ExecuteNonQuery();
                                    }
                                    //
                                }
                            }
                        }
                }//kraj usinga

            }
        }//kraj metode konekcija unos

        public static void FiksniKomboBoxoviVraceno(ComboBox ostecenje, ComboBox poverenje, ComboBox ocena)
        {
            ostecenje.Items.Clear(); poverenje.Items.Clear(); ocena.Items.Clear();
            ostecenje.Items.Add("Neostecena"); ostecenje.Items.Add("Malo ostecena"); ostecenje.Items.Add("Ostecena");
            poverenje.Items.Add("Pouzdan"); poverenje.Items.Add("Srednje"); poverenje.Items.Add("Nepouzdan");
            ocena.Items.Add("1"); ocena.Items.Add("2"); ocena.Items.Add("3"); ocena.Items.Add("4"); ocena.Items.Add("5"); 
        }

        public static void BogacenjeKomboBoxaBrojIndexaVraceno(ComboBox brojIndeksa)
        {
            brojIndeksa.Items.Clear();
            HashSet<string> hashBrojeviIndeksa = new HashSet<string>();
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid = root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komadnaSpoljniKljuceviBrIndexa = new MySqlCommand())
                {
                    komadnaSpoljniKljuceviBrIndexa.Connection = konekcija;
                    komadnaSpoljniKljuceviBrIndexa.CommandText = "select student_student_id from izdatovraceno;";
                    MySqlDataReader citac = komadnaSpoljniKljuceviBrIndexa.ExecuteReader();
                    List<string> SviSpoljniKljuceviBrIndexa = new List<string>();
                    if (citac.Read())
                    {
                        citac.Close(); citac = komadnaSpoljniKljuceviBrIndexa.ExecuteReader();
                        while (citac.Read())
                        {
                            SviSpoljniKljuceviBrIndexa.Add(citac.GetString(0));
                        }
                        citac.Close();
                        
                        for (int i = 0; i < SviSpoljniKljuceviBrIndexa.Count; i++)
                        {
                            using (MySqlCommand komandaSviBrIndexa = new MySqlCommand())
                            {
                                komandaSviBrIndexa.Connection = konekcija;
                                komandaSviBrIndexa.CommandText = "select brojIndeksa from student where student_id = @kljucStudent;";
                                komandaSviBrIndexa.Parameters.AddWithValue("@kljucStudent", SviSpoljniKljuceviBrIndexa[i]);

                                string brojIndexaRezStr = "";
                                citac = komandaSviBrIndexa.ExecuteReader();
                                if (citac.Read())
                                {
                                    citac.Close(); citac = komandaSviBrIndexa.ExecuteReader();
                                    while (citac.Read())
                                    {
                                        brojIndexaRezStr = citac.GetString(0);
                                    }
                                    hashBrojeviIndeksa.Add(brojIndexaRezStr);
                                    citac.Close();
                                }
                            }
                        }
                        foreach (var v in hashBrojeviIndeksa)
                        {
                            brojIndeksa.Items.Add(v);
                        }
                    }//kraj ifa ako ima kljuceva za indekse
                }
            }
        }//kraj metode bogacenje kombo boxa za indexe vraceno

        public static void BogacenjeKomboBoxaPrimerak(ComboBox studentIndex, ComboBox primerak)
        {
            primerak.Items.Clear();
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid = root; password=; database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand komandaNadjiKljucStudent = new MySqlCommand())
                {
                    komandaNadjiKljucStudent.Connection = konekcija;
                    komandaNadjiKljucStudent.CommandText = "select student_id from student where brojIndeksa = @brojIndexa;";
                    komandaNadjiKljucStudent.Parameters.AddWithValue("@brojIndexa", studentIndex.Text);

                    MySqlDataReader citac = komandaNadjiKljucStudent.ExecuteReader();
                    if (citac.Read())
                    {
                        citac.Close(); citac = komandaNadjiKljucStudent.ExecuteReader();
                        string kljucStr = "";
                        while (citac.Read())
                        {
                            kljucStr = citac.GetString(0);
                        }
                        citac.Close();
                        using (MySqlCommand komandaNadjiSpoljneKljucevePrimerakaKnjige = new MySqlCommand())
                        {
                            komandaNadjiSpoljneKljucevePrimerakaKnjige.Connection = konekcija;
                            komandaNadjiSpoljneKljucevePrimerakaKnjige.CommandText = "select knjige_knjige_id from izdatovraceno " +
                                " where student_student_id = @kljucStudenta && datumaVraceno is NULL ;";
                            komandaNadjiSpoljneKljucevePrimerakaKnjige.Parameters.AddWithValue("@kljucStudenta", kljucStr);
                            List<string> listaSpoljniKljuceviKnjige = new List<string>();
                            citac = komandaNadjiSpoljneKljucevePrimerakaKnjige.ExecuteReader();
                            if (citac.Read())
                            {
                                citac.Close();
                                citac = komandaNadjiSpoljneKljucevePrimerakaKnjige.ExecuteReader();
                                
                                while (citac.Read())
                                {
                                    listaSpoljniKljuceviKnjige.Add(citac.GetString(0));
                                }
                                citac.Close();
                                HashSet<string> hashKnjige = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
                                for(int i =0; i < listaSpoljniKljuceviKnjige.Count; i++)
                                {
                                    using (MySqlCommand komandaKnjigeKojeSuZauzete = new MySqlCommand())
                                    {
                                        komandaKnjigeKojeSuZauzete.Connection = konekcija;
                                        komandaKnjigeKojeSuZauzete.CommandText = "select indetifikator from knjige where knjige_id=@kljucKnjige " +
                                            " && slobodnaZauzeta = 'Zauzeta' ;";
                                        komandaKnjigeKojeSuZauzete.Parameters.AddWithValue("@kljucKnjige", listaSpoljniKljuceviKnjige[i]);
                                        citac = komandaKnjigeKojeSuZauzete.ExecuteReader();
                                        while (citac.Read())
                                        {
                                            string knjigeStr = citac.GetString(0);
                                            hashKnjige.Add(knjigeStr);
                                        }
                                        citac.Close();
                                        
                                    }
                                }//kraj petlje
                                foreach (var v in hashKnjige)
                                {
                                    primerak.Items.Add(v);
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Student sa ovim brojem indeksa ne postoji ili nije uzimao knjigu");
                    }
                }
            }
        }//kraj metode bogacenje primerak vraceno

        public static void PromenaKomboBoxaBrojIndeksa(ComboBox brojIndeksaCon, TextBox imeCon, TextBox prezimeCon,
            TextBox polCon, TextBox jmbgCon, TextBox bracniStatusCon, TextBox nacinFinansiranjaCon, TextBox godinaCon, TextBox drzavaCon,
             TextBox gradCon, TextBox telefonCon, TextBox poverenje, PictureBox slikaIzdajemCon)
        {
            using (MySqlConnection konekcija = new MySqlConnection("server = localhost; uid=root; password=;database=biblioteka;"))
            {
                konekcija.Open();
                using (MySqlCommand cmdPodaciStudenta = new MySqlCommand())
                {
                    cmdPodaciStudenta.Connection = konekcija;
                    cmdPodaciStudenta.CommandText = "select * from student where brojIndeksa = @brIndeksa;";
                    cmdPodaciStudenta.Parameters.AddWithValue("@brIndeksa", brojIndeksaCon.Text);
                    MySqlDataReader citac = cmdPodaciStudenta.ExecuteReader();
                    if (citac.Read())
                    {
                        imeCon.Text = citac.GetString(2);
                        prezimeCon.Text = citac.GetString(3);
                        polCon.Text = citac.GetString(4);
                        jmbgCon.Text = citac.GetString(5);
                        bracniStatusCon.Text = citac.GetString(6);
                        nacinFinansiranjaCon.Text = citac.GetString(7);
                        godinaCon.Text = citac.GetString(8);
                        drzavaCon.Text = citac.GetString(9);
                        gradCon.Text = citac.GetString(10);
                        telefonCon.Text = citac.GetString(11);
                        poverenje.Text = citac.GetString(12);
                        byte[] biti = null;
                        if (citac["slika"] == DBNull.Value)
                        {
                            slikaIzdajemCon.Image = null;
                        }
                        else
                        {
                            biti = (byte[])citac["slika"];
                            MemoryStream ms = new MemoryStream(biti);
                            Image img = Image.FromStream(ms);
                            slikaIzdajemCon.Image = img;
                            slikaIzdajemCon.SizeMode = PictureBoxSizeMode.StretchImage;
                        }
                    }
                    citac.Close();
                }
            }
        }
    }
}
