﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
namespace biblioteka_rad
{
    class AutorKlasa
    {
        public string imePrezime { get; private set; }
        public string godina { get; private set; }
        public string drzava { get; private set; }
        public string grad { get; private set; }

        public AutorKlasa(string ime, string god, string drz, string gra)
        {
            imePrezime = ime; godina = god; drzava = drz; grad = gra;
        }
        public bool ProveraGodinaOK()
        {
            bool ok = false;
            if (godina.Length != 4)
            {
                MessageBox.Show("Potrebno je uneti godinu rodjenja u formatu 1985, 4 cifre koje predstavljaju godinu");
                return false;
            }
            for (int i = 0; i < godina.Length; i++)
            {
                int cifra = 0;
                ok = int.TryParse(godina[i].ToString(), out cifra);
                if (!ok)
                {
                    return false;
                }
            }
            return true;
        }
        bool popunjenoSve;
        public bool PopuNjenaPolja()
        {
            return popunjenoSve;
        }
        public AutorKlasa(TextBox ime, TextBox godina, TextBox drzava, TextBox grad)
        {
            if (String.IsNullOrEmpty(ime.Text) || String.IsNullOrEmpty(godina.Text) || String.IsNullOrEmpty(drzava.Text) || String.IsNullOrEmpty(grad.Text))
            {
                MessageBox.Show("Potrebno je uneti sva polja");
                popunjenoSve = false;
            }
            else
            {
                imePrezime = ime.Text; this.godina = godina.Text; this.drzava = drzava.Text; this.grad = grad.Text;
                popunjenoSve = true;
            }
        }
        public void KonekcijaUnosAutora()
        {
            using (MySqlConnection konekcija = new MySqlConnection("server=localhost;uid=root;password=;database=biblioteka;"))
            {
                konekcija.Open();
                UnosPodataka(konekcija);
            }
        }
        private void UnosPodataka(MySqlConnection konekcija)
        {
            string unosBibliotekara = "insert into autor (imePrezimeAutora, godinaRodjenja, drzava, grad) " +
                "values (@ImePrezime, @godina, @drzava, @grad);";
            using (MySqlCommand komanda = new MySqlCommand(unosBibliotekara, konekcija))
            {
                komanda.Parameters.AddWithValue("@ImePrezime", this.imePrezime);
                komanda.Parameters.AddWithValue("@godina", this.godina);
                komanda.Parameters.AddWithValue("@drzava", this.drzava);
                komanda.Parameters.AddWithValue("@grad", this.grad);

                komanda.ExecuteNonQuery();
            }
        }
    }
}
