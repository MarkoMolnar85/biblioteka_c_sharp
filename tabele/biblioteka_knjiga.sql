CREATE DATABASE  IF NOT EXISTS `biblioteka` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `biblioteka`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: biblioteka
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `knjiga`
--

DROP TABLE IF EXISTS `knjiga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knjiga` (
  `knjiga_id` int(11) NOT NULL AUTO_INCREMENT,
  `nazivKnjige` varchar(45) NOT NULL,
  `oblast` varchar(45) DEFAULT NULL,
  `brojStrana` int(11) DEFAULT NULL,
  `tip` varchar(45) DEFAULT NULL,
  `vezKoji` varchar(45) DEFAULT NULL,
  `izdavac_izdavac_id` int(11) NOT NULL,
  PRIMARY KEY (`knjiga_id`),
  UNIQUE KEY `nazivKnjige_UNIQUE` (`nazivKnjige`),
  KEY `fk_knjiga_izdavac_idx` (`izdavac_izdavac_id`),
  CONSTRAINT `fk_knjiga_izdavac` FOREIGN KEY (`izdavac_izdavac_id`) REFERENCES `izdavac` (`izdavac_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `knjiga`
--

LOCK TABLES `knjiga` WRITE;
/*!40000 ALTER TABLE `knjiga` DISABLE KEYS */;
INSERT INTO `knjiga` VALUES (330,'Na bregu kuca mala','Drama',220,'Klasicna','Tvrdi vez',46),(331,'Ogledi o ljubavi','Ljubavni romani',310,'Klasicna','Tvrdi vez',46),(332,'Dusan Slini','Strucna literatura',450,'Klasicna','Tvrdi vez',47),(333,'Plivac','Strucna literatura',210,'Klasicna','Meki vez',47),(334,'Ocaj od nane','Drama',165,'Klasicna','Meki vez',48),(335,'Preko puta istina','Auto biografija',251,'Klasicna','Tvrdi vez',48),(336,'Aura Beograd','Strucna literatura',187,'Klasicna','Meki vez',45),(337,'Beograd kroz kljucaonicu','Auto biografija',350,'Klasicna','Meki vez',45),(338,'C# za 21 dan','Strucna literatura',800,'CD','Meki vez',50),(339,'C++ za 21 dan','Strucna literatura',750,'CD','Meki vez',50),(340,'Reseni zadaci jezik Java','Strucna literatura',650,'Klasicna','Meki vez',50),(341,'Reseni zadaci jezik C++','Strucna literatura',650,'Klasicna','Meki vez',50),(342,'Computer modeling','Strucna literatura',500,'Klasicna','Meki vez',50);
/*!40000 ALTER TABLE `knjiga` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-12 20:43:46
