CREATE DATABASE  IF NOT EXISTS `biblioteka` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `biblioteka`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: biblioteka
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `knjige`
--

DROP TABLE IF EXISTS `knjige`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knjige` (
  `knjige_id` int(11) NOT NULL AUTO_INCREMENT,
  `indetifikator` varchar(45) NOT NULL,
  `slobodnaZauzeta` varchar(45) DEFAULT NULL,
  `ostecenaNeostecena` varchar(45) DEFAULT NULL,
  `mestoKnjige` varchar(45) DEFAULT NULL,
  `knjiga_knjiga_id` int(11) NOT NULL,
  PRIMARY KEY (`knjige_id`),
  UNIQUE KEY `indetifikator_UNIQUE` (`indetifikator`),
  KEY `fk_knjige_knjiga1_idx` (`knjiga_knjiga_id`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `knjige`
--

LOCK TABLES `knjige` WRITE;
/*!40000 ALTER TABLE `knjige` DISABLE KEYS */;
INSERT INTO `knjige` VALUES (155,'Na bregu kuca mala 1','Slobodna','Neostecena','A1',330),(156,'Na bregu kuca mala 2','Slobodna','Neostecena','A1',330),(157,'Na bregu kuca mala 3','Slobodna','Neostecena','A1',330),(158,'Na bregu kuca mala 4','Slobodna','Neostecena','A1',330),(159,'Na bregu kuca mala 5','Slobodna','Neostecena','A1',330),(160,'Na bregu kuca mala 6','Slobodna','Neostecena','A1',330),(161,'Na bregu kuca mala 7','Slobodna','Neostecena','A1',330),(162,'Ogledi o ljubavi 1','Slobodna','Neostecena','B1',331),(163,'Ogledi o ljubavi 2','Slobodna','Neostecena','B1',331),(164,'Ogledi o ljubavi 3','Zauzeta','Neostecena','B1',331),(165,'Ogledi o ljubavi 4','Slobodna','Neostecena','B1',331),(166,'Ogledi o ljubavi 5','Slobodna','Neostecena','B1',331),(167,'Ogledi o ljubavi 6','Slobodna','Ostecena','B1',331),(168,'Dusan Slini 1','Slobodna','Neostecena','C5',332),(169,'Dusan Slini 2','Slobodna','Neostecena','C5',332),(170,'Dusan Slini 3','Slobodna','Neostecena','C5',332),(171,'Dusan Slini 4','Slobodna','Neostecena','C5',332),(172,'Dusan Slini 5','Slobodna','Neostecena','C5',332),(173,'Dusan Slini 6','Slobodna','Neostecena','C5',332),(174,'Dusan Slini 7','Slobodna','Neostecena','C5',332),(175,'Plivac 1','Slobodna','Neostecena','C3',333),(176,'Plivac 2','Slobodna','Neostecena','C3',333),(177,'Plivac 3','Slobodna','Neostecena','C3',333),(178,'Plivac 4','Slobodna','Neostecena','C3',333),(179,'Ocaj od nane 1','Slobodna','Neostecena','T8',334),(180,'Ocaj od nane 2','Slobodna','Neostecena','T8',334),(181,'Ocaj od nane 3','Zauzeta','Neostecena','T8',334),(182,'Ocaj od nane 4','Slobodna','Neostecena','T8',334),(183,'Preko puta istina 1','Zauzeta','Neostecena','J4',335),(184,'Preko puta istina 2','Slobodna','Neostecena','J4',335),(185,'Preko puta istina 3','Slobodna','Neostecena','J4',335),(186,'Preko puta istina 4','Slobodna','Neostecena','J4',335),(187,'Aura Beograd 1','Zauzeta','Ostecena','E2',336),(188,'Aura Beograd 2','Zauzeta','Neostecena','E2',336),(189,'Aura Beograd 3','Slobodna','Neostecena','E2',336),(190,'Aura Beograd 4','Slobodna','Neostecena','E2',336),(191,'Beograd kroz kljucaonicu','Slobodna','Neostecena','P48',337),(192,'Beograd kroz kljucaonicu 2','Slobodna','Neostecena','P48',337),(193,'Beograd kroz kljucaonicu 3','Slobodna','Neostecena','P48',337),(194,'Beograd kroz kljucaonicu 4','Slobodna','Neostecena','P48',337),(195,'Beograd kroz kljucaonicu 5','Zauzeta','Neostecena','P48',337),(196,'Beograd kroz kljucaonicu 6','Slobodna','Neostecena','P48',337),(197,'C# za 21 dan 1','Slobodna','Malo ostecena','H88',338),(198,'C# za 21 dan 2','Slobodna','Neostecena','H88',338),(199,'C# za 21 dan 3','Slobodna','Neostecena','H88',338),(200,'C# za 21 dan 4','Slobodna','Neostecena','H88',338),(201,'C++ za 21 dan 1','Slobodna','Neostecena','H89',339),(202,'C++ za 21 dan 2','Slobodna','Neostecena','H89',339),(203,'C++ za 21 dan 3','Slobodna','Neostecena','H89',339),(204,'Reseni zadaci jezik Java 1','Slobodna','Neostecena','J74',340),(205,'Reseni zadaci jezik Java 2','Slobodna','Neostecena','J74',340),(206,'Reseni zadaci jezik Java 3','Slobodna','Neostecena','J74',340),(207,'Reseni zadaci jezik Java 4','Slobodna','Neostecena','J74',340),(208,'Reseni zadaci jezik C++ 1','Slobodna','Neostecena','J74',341),(209,'Reseni zadaci jezik C++ 2','Slobodna','Neostecena','J74',341),(210,'Reseni zadaci jezik C++ 3','Slobodna','Neostecena','J74',341),(211,'Computer modeling 1','Zauzeta','Neostecena','O85',342),(212,'Computer modeling 2','Slobodna','Neostecena','O85',342),(213,'Computer modeling 3','Slobodna','Neostecena','O85',342),(214,'Computer modeling 4','Slobodna','Neostecena','O85',342);
/*!40000 ALTER TABLE `knjige` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-12 20:43:46
