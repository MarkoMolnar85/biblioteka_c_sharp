CREATE DATABASE  IF NOT EXISTS `biblioteka` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `biblioteka`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: biblioteka
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autor`
--

DROP TABLE IF EXISTS `autor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autor` (
  `autor_id` int(11) NOT NULL AUTO_INCREMENT,
  `imePrezimeAutora` varchar(60) NOT NULL,
  `godinaRodjenja` varchar(45) DEFAULT NULL,
  `drzava` varchar(45) DEFAULT NULL,
  `grad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`autor_id`),
  UNIQUE KEY `imePrezimeAutora_UNIQUE` (`imePrezimeAutora`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autor`
--

LOCK TABLES `autor` WRITE;
/*!40000 ALTER TABLE `autor` DISABLE KEYS */;
INSERT INTO `autor` VALUES (5,'Gradimir R Stojkovic','1975','Srbija','Pancevo'),(6,'Zorica Z Kuburovic','1978','Srbija','Beograd'),(7,'Luka T Miceta','1968','Srbija','Pancevo'),(8,'Andrija T Terzic','1970','Srbija','Uzice'),(9,'Svetislav g Basava','1965','Srbija','Valjevo'),(10,'Milivoje R Tomasevic','1966','Srbija','Beograd'),(11,'Nenad N Stefanovic','1971','Srbija','Novi Sad'),(12,'Dzon A Sarp','1956','USA','New York'),(13,'Jesse K Liberty','1958','USA','Los Angeles'),(14,'Laslo A Kraus','1968','USA','Vashington'),(15,'Milivojevic Milovan','1967','Srbija','Uzice'),(16,'Stopic Srecko','1956','Srbija','Uzice'),(17,'Stojanovic Boban','1956','Srbija','Uzice'),(18,'Dragoljub Drndarevic','1959','Srbija','Uzice');
/*!40000 ALTER TABLE `autor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-12 20:43:45
