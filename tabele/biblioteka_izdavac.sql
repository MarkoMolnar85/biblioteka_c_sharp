CREATE DATABASE  IF NOT EXISTS `biblioteka` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `biblioteka`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: biblioteka
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `izdavac`
--

DROP TABLE IF EXISTS `izdavac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `izdavac` (
  `izdavac_id` int(11) NOT NULL AUTO_INCREMENT,
  `nazivIzdavaca` varchar(45) NOT NULL,
  `drzavaIzdavaca` varchar(45) DEFAULT NULL,
  `gradIzdavaca` varchar(45) DEFAULT NULL,
  `postanskiBrojIzdavaca` varchar(45) DEFAULT NULL,
  `brojTelefonaIzdavaca` varchar(45) DEFAULT NULL,
  `emailIzdavaca` varchar(45) DEFAULT NULL,
  `ocenaIzdavaca` int(11) DEFAULT NULL,
  PRIMARY KEY (`izdavac_id`),
  UNIQUE KEY `nazivIzdavaca_UNIQUE` (`nazivIzdavaca`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `izdavac`
--

LOCK TABLES `izdavac` WRITE;
/*!40000 ALTER TABLE `izdavac` DISABLE KEYS */;
INSERT INTO `izdavac` VALUES (45,'Akademija Beograd','Srbija','Beograd','11000','011555885','akademija@gmail.com',3),(46,'Admiral books','Srbija','Beograd','11000','011111225','admiral@gmail.com',4),(47,'Aed studio','Srbija','Beograd','11000','011111225','aed@gmail.com',4),(48,'Agencija Obrenovic','Srbija','Beograd','11000','011545887','Agencija@gmail.com',4),(50,'Mikro knjiga','Srbija','Beograd','11000','0118787855','mirko@hotmail.com',5);
/*!40000 ALTER TABLE `izdavac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-12 20:43:46
