CREATE DATABASE  IF NOT EXISTS `biblioteka` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `biblioteka`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: biblioteka
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `izdatovraceno`
--

DROP TABLE IF EXISTS `izdatovraceno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `izdatovraceno` (
  `izdatoVraceno_id` int(11) NOT NULL AUTO_INCREMENT,
  `datumaUzeto` date DEFAULT NULL,
  `kolikoDana` int(11) DEFAULT NULL,
  `datumaVraceno` date DEFAULT NULL,
  `izdatoVracenocol` date DEFAULT NULL,
  `ocenaKnjige` int(11) DEFAULT NULL,
  `ostecenaNeostecena` varchar(45) DEFAULT NULL,
  `knjige_knjige_id` int(11) NOT NULL,
  `bibliotekar_bibliotekar_id` int(11) NOT NULL,
  `student_student_id` int(11) NOT NULL,
  PRIMARY KEY (`izdatoVraceno_id`),
  KEY `fk_izdatoVraceno_knjige1_idx` (`knjige_knjige_id`),
  KEY `fk_izdatoVraceno_bibliotekar1_idx` (`bibliotekar_bibliotekar_id`),
  KEY `fk_izdatoVraceno_student1_idx` (`student_student_id`),
  CONSTRAINT `fk_izdatoVraceno_bibliotekar1` FOREIGN KEY (`bibliotekar_bibliotekar_id`) REFERENCES `bibliotekar` (`bibliotekar_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_izdatoVraceno_knjige1` FOREIGN KEY (`knjige_knjige_id`) REFERENCES `knjige` (`knjige_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_izdatoVraceno_student1` FOREIGN KEY (`student_student_id`) REFERENCES `student` (`student_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `izdatovraceno`
--

LOCK TABLES `izdatovraceno` WRITE;
/*!40000 ALTER TABLE `izdatovraceno` DISABLE KEYS */;
INSERT INTO `izdatovraceno` VALUES (88,'2016-06-06',10,'2016-06-15','2016-06-16',4,'Ostecena',187,13,71),(89,'2016-06-06',10,'2016-06-15','2016-06-16',1,'Neostecena',199,13,71),(90,'2016-06-06',10,'2016-06-15','2016-06-16',4,'Neostecena',200,13,53),(91,'2016-06-06',10,'2016-06-15','2016-06-16',4,'Neostecena',179,13,53),(92,'2016-06-06',10,'2016-07-15','2016-06-16',5,'Neostecena',208,13,57),(93,'2016-06-06',10,'2016-07-15','2016-06-16',5,'Neostecena',205,13,57),(94,'2016-07-06',10,'2016-06-15','2016-07-16',4,'Neostecena',168,13,67),(95,'2016-07-06',10,NULL,'2016-07-16',NULL,NULL,181,13,67),(96,'2016-07-06',10,NULL,'2016-07-16',NULL,NULL,195,13,64),(97,'2016-08-06',10,NULL,'2016-08-16',NULL,NULL,164,13,64),(98,'2016-09-06',7,'2016-06-15','2016-09-13',4,'Ostecena',167,13,65),(99,'2016-10-06',7,'2016-06-15','2016-10-13',5,'Malo ostecena',197,13,61),(100,'2016-07-22',5,NULL,'2016-07-27',NULL,NULL,211,13,50),(101,'2016-05-21',5,'2016-05-24','2016-05-26',4,'Neostecena',198,13,72),(102,'2016-12-08',7,NULL,'2016-12-15',NULL,NULL,187,13,49),(103,'2016-12-08',7,NULL,'2016-12-15',NULL,NULL,188,13,49),(104,'2016-08-08',3,'2016-11-08','2016-08-11',5,'Neostecena',190,13,49),(105,'2016-08-08',4,NULL,'2016-08-12',NULL,NULL,183,13,49);
/*!40000 ALTER TABLE `izdatovraceno` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-12 20:43:46
